﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MiddleMP.master" AutoEventWireup="true" CodeFile="EditPersonalInfoProf.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .auto-style3 {
            width: 196px;
        }
        .auto-style4 {
            width: 239px;
        }
        .auto-style5 {
            width: 382px;
        }
        .auto-style6 {
            width: 382px;
            height: 30px;
        }
        .auto-style7 {
            width: 196px;
            height: 30px;
        }
        .auto-style8 {
            width: 239px;
            height: 30px;
        }
        .auto-style9 {
            height: 30px;
        }
        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="menu2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="middleSide" Runat="Server">
    <table style="width: 100%;">
        <tr>
            <td class="auto-style5">&nbsp;</td>
            <td class="auto-style3">
                <asp:Label ID="Label1" runat="server" Text="Name"></asp:Label>
            </td>
            <td class="auto-style4">
                <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style5">&nbsp;</td>
            <td class="auto-style3">
                <asp:Label ID="Label2" runat="server" Text="Surname"></asp:Label>
            </td>
            <td class="auto-style4">
                <asp:TextBox ID="txtSurname" runat="server"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style5">&nbsp;</td>
            <td class="auto-style3">
                <asp:Label ID="Label4" runat="server" Text="New Password"></asp:Label>
            </td>
            <td class="auto-style4">
                <asp:TextBox ID="txtNewPassword" runat="server"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style5">&nbsp;</td>
            <td class="auto-style3">
                <asp:Label ID="Label5" runat="server" Text="Confirm Password"></asp:Label>
            </td>
            <td class="auto-style4">
                <asp:TextBox ID="txtConfirmPassword" runat="server"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style5">&nbsp;</td>
            <td class="auto-style3">
                <asp:Label ID="Label6" runat="server" Text="Email Address"></asp:Label>
            </td>
            <td class="auto-style4">
                <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style5">&nbsp;</td>
            <td class="auto-style3">
                <asp:Label ID="Label7" runat="server" Text="Phone Number"></asp:Label>
            </td>
            <td class="auto-style4">
                <asp:TextBox ID="txtPhone" runat="server"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style5">&nbsp;</td>
            <td class="auto-style3">
                <asp:Label ID="Label8" runat="server" Text="Title"></asp:Label>
            </td>
            <td class="auto-style4">
                <asp:TextBox ID="txtTitle" runat="server"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style5">&nbsp;</td>
            <td class="auto-style3">
                <asp:Label ID="Label9" runat="server" Text="Department"></asp:Label>
            </td>
            <td class="auto-style4">
                <asp:DropDownList ID="DropDownList1" runat="server">
                </asp:DropDownList>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style5">&nbsp;</td>
            <td class="auto-style3">
                <asp:Label ID="Label10" runat="server" Text="Working Hour"></asp:Label>
            </td>
            <td class="auto-style4">
                <asp:TextBox ID="working" runat="server"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style5">&nbsp;</td>
            <td class="auto-style3">
                <asp:Label ID="Label11" runat="server" Text="personal information"></asp:Label>
            </td>
            <td class="auto-style4">
                <asp:TextBox ID="txtpersonalinfo" runat="server"></asp:TextBox>
            </td>
            <td>
                <asp:FileUpload ID="FileUpload2" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="auto-style6"></td>
            <td class="auto-style7">&nbsp;</td>
            <td class="auto-style8">
                &nbsp;</td>
            <td class="auto-style9">
                <asp:Button ID="btnUploadPicture" runat="server" Text="Upload Picture" Width="130px" OnClick="btnUploadPicture_Click" />
                <asp:Label ID="mssge0" runat="server"></asp:Label>
            </td>

            <td class="auto-style9">&nbsp;</td>

        </tr>
        <tr>
            <td class="auto-style5">&nbsp;</td>
            <td class="auto-style3">&nbsp;</td>
            <td class="auto-style4">
                <asp:Button ID="btnUpdateInformation" runat="server" Text="Update Information" OnClick="btnUpdateInformation_Click1" />
            </td>
            <td>
                <asp:Label ID="mssge" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Content>


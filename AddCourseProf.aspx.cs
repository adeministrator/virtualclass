﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using virtualclass.modles;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session[Constants.UserName] == null || Session[Constants.UserName].GetType() != typeof(Professor))
        {
            Response.Redirect("/login.aspx");
        }

        if (!IsPostBack)
        {
            
            List<Department> depart = Department.getDepartments();
            foreach (Department s in depart)
            {
                dlstDepartment.Items.Add(s.departmentName);
            }
        }
     
    }
   
    protected void btnAddCourse_Click1(object sender, EventArgs e)
    {
        Professor prof = Session[Constants.UserName] as Professor;
        Department dep = Department.getDepartment(dlstDepartment.SelectedItem.ToString());
        Course co = new Course();
        string tname;
        string cname = txtCourseName.Text;
        string code = txtCourseCode.Text;
        string term = txtTerm.Text;
        string desc = txtDescription.Text;
       
            tname = txtTAUserName.Text;

        string pname = prof.userName;
        co.AddCourses(cname, code, term, desc, tname, pname);
        dep.AddCourse(code);
        msge.Text = "class added sucessfully";
    }
}
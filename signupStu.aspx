﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MiddleMP.master" AutoEventWireup="true" CodeFile="signupStu.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
    .auto-style1 {
        width: 100%;
        height: 277px;
    }
    .auto-style2 {
        width: 322px;
    }
    .auto-style3 {
        width: 235px;
    }
    .auto-style7 {
        width: 322px;
        height: 26px;
    }
    .auto-style8 {
        width: 235px;
        height: 26px;
    }
    .auto-style9 {
        height: 26px;
    }
    .auto-style10 {
        width: 222px;
    }
    .auto-style11 {
        width: 222px;
        height: 26px;
    }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="menu2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="middleSide" Runat="Server">
    <asp:ScriptManager ID="ScriptManager3" runat="server">
            </asp:ScriptManager>

     <asp:UpdatePanel ID="UpdatePanel1" runat="server">
   <ContentTemplate>

     <table class="auto-style1">
    <tr>
        <td class="auto-style2">
            
        </td>
        <td class="auto-style3">
            <asp:Label ID="Label12" runat="server" Text="Name"></asp:Label>
        </td>
        <td class="auto-style10">
            <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtName" ErrorMessage="plz enter name"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td class="auto-style2">&nbsp;</td>
        <td class="auto-style3">
            <asp:Label ID="Label13" runat="server" Text="Surname"></asp:Label>
        </td>
        <td class="auto-style10">
            <asp:TextBox ID="txtSurname" runat="server"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtSurname" ErrorMessage="plz enter a surname"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td class="auto-style7"></td>
        <td class="auto-style8">
            <asp:Label ID="Label14" runat="server" Text="User Name"></asp:Label>
        </td>
        <td class="auto-style11">
            <asp:TextBox ID="txtUserName" runat="server"></asp:TextBox>
        </td>
        <td class="auto-style9">
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtUserName" ErrorMessage="plz enter a username"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td class="auto-style7"></td>
        <td class="auto-style8">Password</td>
        <td class="auto-style11">
            <asp:TextBox ID="txtPassword" runat="server" TextMode="Password"></asp:TextBox>
        </td>
        <td class="auto-style9">
            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtPassword" ErrorMessage="plz enter a password"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td class="auto-style7"></td>
        <td class="auto-style8">
            <asp:Label ID="Label16" runat="server" Text="Confirm Password"></asp:Label>
        </td>
        <td class="auto-style11">
            <asp:TextBox ID="txtConfirmPassword" runat="server" TextMode="Password"></asp:TextBox>
        </td>
        <td class="auto-style9">
            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtConfirmPassword" ErrorMessage="plz enter a password"></asp:RequiredFieldValidator>
            <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtPassword" ControlToValidate="txtConfirmPassword" ErrorMessage="password dosnt match"></asp:CompareValidator>
        </td>
    </tr>
    <tr>
        <td class="auto-style7"></td>
        <td class="auto-style8">
            <asp:Label ID="Label17" runat="server" Text="E-mail Address"></asp:Label>
        </td>
        <td class="auto-style11">
            <asp:TextBox ID="txtEmail" runat="server" TextMode="Email"></asp:TextBox>
        </td>
        <td class="auto-style9">
            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtEmail" ErrorMessage="plz enter email"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmail" ErrorMessage="plz enter a correct email form" ValidationExpression="^((?&gt;[a-zA-Z\d!#$%&amp;'*+\-/=?^_`{|}~]+\x20*|&quot;((?=[\x01-\x7f])[^&quot;\\]|\\[\x01-\x7f])*&quot;\x20*)*(?&lt;angle&gt;&lt;))?((?!\.)(?&gt;\.?[a-zA-Z\d!#$%&amp;'*+\-/=?^_`{|}~]+)+|&quot;((?=[\x01-\x7f])[^&quot;\\]|\\[\x01-\x7f])*&quot;)@(((?!-)[a-zA-Z\d\-]+(?&lt;!-)\.)+[a-zA-Z]{2,}|\[(((?(?&lt;!\[)\.)(25[0-5]|2[0-4]\d|[01]?\d?\d)){4}|[a-zA-Z\d\-]*[a-zA-Z\d]:((?=[\x01-\x7f])[^\\\[\]]|\\[\x01-\x7f])+)\])(?(angle)&gt;)$"></asp:RegularExpressionValidator>
        </td>
    </tr>
    <tr>
        <td class="auto-style7"></td>
        <td class="auto-style8">
            <asp:Label ID="txtPhone" runat="server" Text="Phone Number"></asp:Label>
        </td>
        <td class="auto-style11">
            <asp:TextBox ID="txtPhoneNumber" runat="server" TextMode="Email"></asp:TextBox>
        </td>
        <td class="auto-style9"></td>
    </tr>
    <tr>
        <td class="auto-style7"></td>
        <td class="auto-style8">Graduation Date</td>
        <td class="auto-style11">
            <asp:Calendar ID="Calendar1" runat="server" OnSelectionChanged="Calendar1_SelectionChanged"></asp:Calendar>
        </td>
        <td class="auto-style9">&nbsp;</td>
    </tr>
    <tr>
        <td class="auto-style7"></td>
        <td class="auto-style8">
            <asp:Label ID="Label21" runat="server" Text="Department"></asp:Label>
        </td>
        <td class="auto-style11">
            <asp:DropDownList ID="listbDepartment" runat="server">
            </asp:DropDownList>
        </td>
        <td class="auto-style9">
            &nbsp;</td>
    </tr>
    <tr>
        <td class="auto-style7"></td>
        <td class="auto-style8"></td>
        <td class="auto-style11">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;
            <asp:Button ID="btnSignUpStu" runat="server" Text="Sign Up" OnClick="btnSignUpStu_Click" />
        </td>
        <td class="auto-style9"></td>
    </tr>
    <tr>
        <td class="auto-style2">&nbsp;</td>
        <td class="auto-style3">&nbsp;</td>
        <td class="auto-style10">&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
</table>
       </ContentTemplate>
           </asp:UpdatePanel>
</asp:Content>


USE [VirtualClass]
GO
SET IDENTITY_INSERT [academic].[Users] ON 

GO
INSERT [academic].[Users] ([userID], [pass], [userName], [userType], [name], [surname], [picture], [email], [phone], [personalInfo], [isActive]) VALUES (1, N'adm3', N'ademb', N'PROF', N'Adem', N'Bavaş', N'Professors/images/deafult.jpg', N'adembavas', N'', N'', 1)
GO
INSERT [academic].[Users] ([userID], [pass], [userName], [userType], [name], [surname], [picture], [email], [phone], [personalInfo], [isActive]) VALUES (4, N'hsk246', N'hosokiY', N'PROF', N'Hosoki', N'Yamanako', N'Professors/images/deafult.jpg', N'hosokiy@iku.edu.tr', N'', N'', 1)
GO
INSERT [academic].[Users] ([userID], [pass], [userName], [userType], [name], [surname], [picture], [email], [phone], [personalInfo], [isActive]) VALUES (5, N'cgty2466', N'CagatayC', N'PROF', N'Çağatay', N'Çatal', NULL, N'c.catal@iku.edu.tr', NULL, NULL, 1)
GO
INSERT [academic].[Users] ([userID], [pass], [userName], [userType], [name], [surname], [picture], [email], [phone], [personalInfo], [isActive]) VALUES (6, N'ysf24', N'YusufO', N'PROF', N'Yusuf', N'Önen', NULL, N'y.onen@iku.edu.tr', NULL, N'Head of Civil Engineering', 1)
GO
INSERT [academic].[Users] ([userID], [pass], [userName], [userType], [name], [surname], [picture], [email], [phone], [personalInfo], [isActive]) VALUES (7, N'ertgrl47', N'ErtugrulS', N'PROF', N'Ertuğrul', N'Saatçi', NULL, N'e.saatci@iku.edu.tr', NULL, NULL, 1)
GO
INSERT [academic].[Users] ([userID], [pass], [userName], [userType], [name], [surname], [picture], [email], [phone], [personalInfo], [isActive]) VALUES (9, N'eml3', N'EmelD', N'PROF', N'Emel', N'Duman', NULL, N'e.duman@iku.edu.tr', NULL, N'Graduated from IKU and Lecturing in IKU', 1)
GO
INSERT [academic].[Users] ([userID], [pass], [userName], [userType], [name], [surname], [picture], [email], [phone], [personalInfo], [isActive]) VALUES (10, N'szr24', N'SezerT', N'STUDENT', N'Sezer', N'the Earth', NULL, N'sezert@iku.edu.tr', NULL, N'Highest GPA in IKU', 1)
GO
INSERT [academic].[Users] ([userID], [pass], [userName], [userType], [name], [surname], [picture], [email], [phone], [personalInfo], [isActive]) VALUES (11, N'anl3', N'AnılY', N'STUDENT', N'Anıl', N'Yılmaz', NULL, N'anıle@ieee.com', NULL, N'Spend time for IEEE', 1)
GO
SET IDENTITY_INSERT [academic].[Users] OFF
GO
SET IDENTITY_INSERT [academic].[Students] ON 

GO
INSERT [academic].[Students] ([stuID], [garduationDate], [userID]) VALUES (1, NULL, 10)
GO
INSERT [academic].[Students] ([stuID], [garduationDate], [userID]) VALUES (2, NULL, 11)
GO
SET IDENTITY_INSERT [academic].[Students] OFF
GO
SET IDENTITY_INSERT [academic].[Professors] ON 

GO
INSERT [academic].[Professors] ([profID], [workingHours], [title], [userID]) VALUES (1, 3, N'Assistant Professor', 1)
GO
INSERT [academic].[Professors] ([profID], [workingHours], [title], [userID]) VALUES (2, 3, N'Dean', 4)
GO
INSERT [academic].[Professors] ([profID], [workingHours], [title], [userID]) VALUES (3, 6, N'Assoc. Prof.', 5)
GO
INSERT [academic].[Professors] ([profID], [workingHours], [title], [userID]) VALUES (4, 5, N'Dean', 6)
GO
INSERT [academic].[Professors] ([profID], [workingHours], [title], [userID]) VALUES (5, 8, N'Assit. Prof', 7)
GO
INSERT [academic].[Professors] ([profID], [workingHours], [title], [userID]) VALUES (6, 8, N'Assit. Prof', 9)
GO
SET IDENTITY_INSERT [academic].[Professors] OFF
GO
SET IDENTITY_INSERT [academic].[Departments] ON 

GO
INSERT [academic].[Departments] ([departmentID], [departmentName], [departmentDescription]) VALUES (1, N'Computer Science Eng', N'All lectures about computer and computer science engineering will be given')
GO
INSERT [academic].[Departments] ([departmentID], [departmentName], [departmentDescription]) VALUES (2, N'Civil Engineering', N'All lectures about civil engineering and structures will be given')
GO
INSERT [academic].[Departments] ([departmentID], [departmentName], [departmentDescription]) VALUES (3, N'Electric and Electro', N'All lectures about electric and electronic engineering and designing circles will be given')
GO
INSERT [academic].[Departments] ([departmentID], [departmentName], [departmentDescription]) VALUES (4, N'Mathematic and Physi', N'All lectures about neccesary mathematic and physic lectures circles will be given')
GO
SET IDENTITY_INSERT [academic].[Departments] OFF
GO
INSERT [academic].[Belong_To] ([userID], [departmentID]) VALUES (1, 1)
GO
INSERT [academic].[Belong_To] ([userID], [departmentID]) VALUES (4, 2)
GO
INSERT [academic].[Belong_To] ([userID], [departmentID]) VALUES (4, 3)
GO
INSERT [academic].[Belong_To] ([userID], [departmentID]) VALUES (9, 2)
GO
INSERT [academic].[Belong_To] ([userID], [departmentID]) VALUES (9, 4)
GO
SET IDENTITY_INSERT [academic].[Courses] ON 

GO
INSERT [academic].[Courses] ([courseID], [courseName], [term], [courseDescription], [code], [tID], [profID], [isActive]) VALUES (1, N'Object Oriented Programming', N'Fall', N'Object Oriented methods in C++', N'cse2001', NULL, 3, 1)
GO
INSERT [academic].[Courses] ([courseID], [courseName], [term], [courseDescription], [code], [tID], [profID], [isActive]) VALUES (2, N'Calculus 1', N'Spring', N'Beginning Lecture for Mathematic', N'mp1001', NULL, 6, 1)
GO
INSERT [academic].[Courses] ([courseID], [courseName], [term], [courseDescription], [code], [tID], [profID], [isActive]) VALUES (3, N'Calculus 2', N'Fall', N'Next Step for Calculus', N'mp1002', NULL, 6, 1)
GO
INSERT [academic].[Courses] ([courseID], [courseName], [term], [courseDescription], [code], [tID], [profID], [isActive]) VALUES (4, N'Digital Design', N'Spring', N'Teaching digital circles', N'ee2001', NULL, 5, 1)
GO
INSERT [academic].[Courses] ([courseID], [courseName], [term], [courseDescription], [code], [tID], [profID], [isActive]) VALUES (5, N'Digital Design 2', N'Fall', N'Teaching digital circles', N'ee2002', NULL, 5, 1)
GO
SET IDENTITY_INSERT [academic].[Courses] OFF
GO
SET IDENTITY_INSERT [content].[Resources] ON 

GO
INSERT [content].[Resources] ([resourceID], [courseID], [profId], [Path], [isDeleted]) VALUES (2, 1, 3, N'''c:/Downloads/Sth.pdf''', 0)
GO
SET IDENTITY_INSERT [content].[Resources] OFF
GO
SET IDENTITY_INSERT [content].[Questions] ON 

GO
INSERT [content].[Questions] ([questionID], [mssg], [userID], [isResolved], [ressourceID], [isDeleted]) VALUES (1, N'is it working again ?', 10, 1, NULL, 0)
GO
SET IDENTITY_INSERT [content].[Questions] OFF
GO
SET IDENTITY_INSERT [content].[Comments] ON 

GO
INSERT [content].[Comments] ([commentID], [mssg], [userID], [questionID], [isDeleted]) VALUES (1, N'Yes it is!', 10, 1, 0)
GO
SET IDENTITY_INSERT [content].[Comments] OFF
GO
INSERT [academic].[Take] ([courseID], [stuID]) VALUES (1, 1)
GO
INSERT [academic].[Take] ([courseID], [stuID]) VALUES (1, 2)
GO
INSERT [academic].[Take] ([courseID], [stuID]) VALUES (2, 2)
GO
INSERT [academic].[Prof_Degree] ([profID], [degree], [date], [major]) VALUES (6, N'Doctore', CAST(N'2015-05-10' AS Date), N'Math in Daily Life')
GO
INSERT [academic].[Announcements] ([profID], [courseID], [date], [message], [isDeleted]) VALUES (3, 1, CAST(N'2016-10-08 00:00:00.000' AS DateTime), N'There will be no class today', 0)
GO
INSERT [academic].[Have] ([courseID], [departmentID]) VALUES (1, 1)
GO
INSERT [academic].[Have] ([courseID], [departmentID]) VALUES (2, 4)
GO
INSERT [academic].[Have] ([courseID], [departmentID]) VALUES (3, 4)
GO

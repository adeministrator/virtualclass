﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using virtualclass.modles;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session[Constants.UserName] != null)
        {
             virtualclass.modles.User u = Session[Constants.UserName] as virtualclass.modles.User;
            if (u.userType == virtualclass.modles.User.UserType.Student)
            {
                if (Course.GetStudentCourses((u as Student).studentID).Count == 0)
                {
                    Response.Redirect("/CourseInfo.aspx", true);
                }
            }
            
        }

    }
    protected void btnLogin_Click(object sender, EventArgs e)
    {
        
    }

    protected void btnLogin_Click1(object sender, EventArgs e)
    {
        string userName = txtUserName.Text;
        string password = txtPassword.Text;

        if (virtualclass.modles.User.IsLoginCorrect(userName, password))
        {
            if (virtualclass.modles.User.typeOFUser(userName) == virtualclass.modles.User.UserType.Student)
            {
                Student s = Student.Login(userName, password);
                Session[Constants.UserName] = s;
                if (Course.GetStudentCourses(s.studentID).Count == 0)
                {
                
                    Response.Redirect("/CourseInfo.aspx",true);
                }
                else
                {
                    Response.Redirect("/HStudent.aspx", true);
                }

            }
            else if (virtualclass.modles.User.typeOFUser(userName) == virtualclass.modles.User.UserType.Professor)
            {
                Professor pf = Professor.getProfessor(userName);
                Session[Constants.UserName] = pf;
                if (Course.GetProfCourses(pf.profId).Count == 0)
                {
                    
                    Response.Redirect("/AddCoursePRof.aspx",true);

                }
                else
                {
                    Response.Redirect("/HProf.aspx",true);
                }

            }
        }
    }

    protected void btnGoSignup_Click(object sender, EventArgs e)
    {
        Server.Transfer("Welcome.aspx", true);
    }
}
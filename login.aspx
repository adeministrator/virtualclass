﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MiddleMP.master" AutoEventWireup="true" CodeFile="login.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
    .auto-style1 {
        width: 436px;
    }
    .auto-style2 {
        width: 140px;
    }
    .auto-style3 {
        width: 148px;
    }
        .auto-style5 {
            width: 436px;
            height: 20px;
        }
        .auto-style6 {
            width: 140px;
            color: #8A8A8A;
            height: 20px;
        }
        .auto-style7 {
            width: 148px;
            height: 20px;
        }
        .auto-style8 {
            height: 20px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="menu2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="middleSide" Runat="Server">
    <table style="width: 100%;">
    <tr>
        <td class="auto-style1">&nbsp;</td>
        <td class="auto-style2">&nbsp;</td>
        <td class="auto-style3">&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td class="auto-style1">&nbsp;</td>
        <td class="auto-style2">
            <asp:Label ID="Label1" runat="server" Text="User Name"></asp:Label>
        </td>
        <td class="auto-style3">
            <asp:TextBox ID="txtUserName" runat="server"></asp:TextBox>
        </td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td class="auto-style1">&nbsp;</td>
        <td class="auto-style2">
            <asp:Label ID="Label2" runat="server" Text="Password"></asp:Label>
        </td>
        <td class="auto-style3">
            <asp:TextBox ID="txtPassword" runat="server" TextMode="Password"></asp:TextBox>
        </td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td class="auto-style1">&nbsp;</td>
        <td class="auto-style2">&nbsp;</td>
        <td class="auto-style3">&nbsp;</td>
        <td>
            <asp:Label ID="Label3" runat="server" Text="Don't You Have an Account?"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="auto-style1">&nbsp;</td>
        <td class="auto-style2">&nbsp;</td>
        <td class="auto-style3">
            <asp:Button ID="btnLogin" runat="server" Text="Login" OnClick="btnLogin_Click1" Height="26px" />
        </td>
        <td>
            <asp:Button ID="btnGoSignup" runat="server" OnClick="btnGoSignup_Click" Text="Sign Up" />
        </td>
    </tr>
    <tr>
        <td class="auto-style5"></td>
        <td class="auto-style6"></td>
        <td class="auto-style7">
            <asp:LinkButton ID="lbtnForgetPassword" runat="server">Forgot Password?</asp:LinkButton>
        </td>
        <td class="auto-style8">
        </td>
    </tr>
</table>
</asp:Content>


﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MiddleMP.master" AutoEventWireup="true" CodeFile="Welcome.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .auto-style1 {
            width: 289px;
        }
        .auto-style2 {
            width: 622px;
        }
        .auto-style4 {
            width: 338px;
            height: 140px;
        }
        .auto-style5 {
            width: 622px;
            color: #8A8A8A;
            text-align: center;
        }
        .auto-style6 {
            width: 289px;
            height: 182px;
        }
        .auto-style7 {
            width: 622px;
            color: #8A8A8A;
            height: 182px;
        }
        .auto-style8 {
            height: 182px;
        }
        .auto-style9 {
            width: 289px;
            height: 20px;
        }
        .auto-style10 {
            width: 622px;
            color: #8A8A8A;
            text-align: center;
            height: 20px;
        }
        .auto-style11 {
            height: 20px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="menu2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="middleSide" Runat="Server">
    <table style="width: 100%;">
        <tr>
            <td class="auto-style1">&nbsp;</td>
            <td class="auto-style2">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style1">&nbsp;</td>
            <td class="auto-style5" style="font-family: 'Book Antiqua'; font-size: large; color: #000000">About Us</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style1">&nbsp;</td>
            <td class="auto-style2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; This Virtual Class brings professors and students together from all around the world. Our goal is providing education without deduction in online platform.</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style1">&nbsp;</td>
            <td class="auto-style2">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style9"></td>
            <td class="auto-style10" style="color:black;font-size:large; font-family: Century; font-size: medium; text-decoration: underline">
                <h5><strong>Who we work with ?&nbsp;</strong></h5>
            </td>
            <td class="auto-style11"></td>
        </tr>
        <tr>
            <td class="auto-style6"></td>
            <td class="auto-style7">
                &nbsp;<img alt="" class="auto-style4" src="images/harvard-logo.jpg" /></td>
            <td class="auto-style8">&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style1">&nbsp;</td>
            <td class="auto-style2">&nbsp;</td>
            <td>You have account already?</td>
        </tr>
        <tr>
            <td class="auto-style1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="btnRedirectSignupStu" runat="server" OnClick="btnRedirectSignupStu_Click" Text="Sign Up As a Student" />
            </td>
            <td class="auto-style2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="btnRedirectSignupProf" runat="server" OnClick="btnRedirectSignupProf_Click" Text="Sign Up As a Professor" />
            </td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="btnLogin" runat="server" Text="Log In" OnClick="btnLogin_Click" />
            </td>
        </tr>
        <tr>
            <td class="auto-style1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
            <td class="auto-style2">&nbsp;</td>
            <td>
                <asp:LinkButton ID="lbtnForgetPassword" runat="server">Did you forget your password?</asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td class="auto-style1">&nbsp;</td>
            <td class="auto-style5">Do you need help? Let us know.<br />
                support@virtualclass.com</td>
            <td>&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
                </td>
        </tr>
        <tr>
            <td class="auto-style1">&nbsp;</td>
            <td class="auto-style2">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
</asp:Content>


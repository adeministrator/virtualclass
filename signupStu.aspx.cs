﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using virtualclass.modles;

public partial class _Default : System.Web.UI.Page
{public string Password="";
    public string confpwd = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (!IsPostBack)
        {
           
            foreach (Department dep in Department.getDepartments())
            {
                listbDepartment.Items.Add(dep.departmentName);
               
            }
        }
        Password = txtPassword.Text;
        confpwd = txtConfirmPassword.Text;
        txtPassword.Attributes.Add("value", Password);
        txtConfirmPassword.Attributes.Add("value", confpwd);
    }
    protected void btnSignUpStu_Click(object sender, EventArgs e)
    {
        
        DateTime date = DateTime.Parse("1/1/0001");
        string dat3 = Calendar1.SelectedDate.ToShortDateString();
        DateTime date2=DateTime.Parse(dat3);
        if (date.CompareTo(date2) != 0)
        { 
            var s = Student.AddNewUser(txtUserName.Text, Password, txtName.Text, txtSurname.Text, txtPhoneNumber.Text, "", txtEmail.Text, Calendar1.SelectedDate);

            if (s != null)
            {
                Department.getDepartment(listbDepartment.SelectedItem.Text).AddUser(s);
                Response.Redirect("/login.aspx", true);
            }
        }
        else{
            date=DateTime.Parse("1/1/1900");
            var s = Student.AddNewUser(txtUserName.Text, Password, txtName.Text, txtSurname.Text, txtPhoneNumber.Text, "", txtEmail.Text, date);

            if (s != null)
            {
                Department.getDepartment(listbDepartment.SelectedItem.Text).AddUser(s);
                Response.Redirect("/login.aspx", true);
            }
        }
    }



    protected void Calendar1_SelectionChanged(object sender, EventArgs e)
    {

    }
}
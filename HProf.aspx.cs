﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using virtualclass.modles;

public partial class Default2 : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session[Constants.UserName] == null || Session[Constants.UserName].GetType() != typeof(Professor))
        {
            Response.Redirect("/login.aspx");
        }

        if (!IsPostBack)
        {
            Professor s = Session[Constants.UserName] as Professor;

            List<Course> courseList = Course.GetProfCourses(s.profId);
            foreach (Course zz in courseList)
            {
                dlstCourseList.Items.Add(zz.courseName);

            }

            DataTable dt = new DataTable();
            dt.Clear();
            List<Course> c = Course.GetProfCourses(s.profId);
            dt.Columns.Add("Lectures");
            dt.Columns.Add("Link");
            Course course = c[dlstCourseList.SelectedIndex];
            List<Lecture> l = Lecture.getLectures(course);
            for (int i = 0; i < l.Count; i++)
            {
                DataRow row = dt.NewRow();
                row["Lectures"] = l[i].title;
                row["Link"] = i;
                dt.Rows.Add(row);
            }
            ViewState[Constants.LecturersInCourse] = l;
            DataList1.DataSource = dt;
            Session[Constants.LecturersInCourse] = dt;
            DataList1.DataBind();

        }
        else
        {

        }
        keyURL.Text = (Session[Constants.Youtubeurl] != null) ? Session[Constants.Youtubeurl].ToString() : null;
        //if (ViewState[Constants.CurrentLecture] != null)
        //{

        //    Lecture lt = ViewState[Constants.CurrentLecture] as Lecture;
        //    DataSet se = fill(lt);
        //    se.Relations.Add(new DataRelation("DataList2", se.Tables[0].Columns["questionID"], se.Tables[1].Columns["questionID"]));
        //    se.Relations[0].Nested = true;
        //    DataList2.DataSource = se.Tables[0];
        //    DataList2.DataBind();
        //}
        //    DataTable dt = new DataTable();
        //    dt.Columns.Add("Question");
        //    dt.Columns.Add("ID");
        //    dt.Clear();
        //    Lecture le = ViewState[Constants.CurrentLecture] as Lecture;
        //    List<Question> questions = Question.getQuestionsOfRsource(le);
        //    foreach (Question qt in questions)
        //    {
        //        DataRow row = dt.NewRow();
        //        row["Question"] = qt.mssg;
        //        row["ID"] = qt.questionID;
        //        dt.Rows.Add(row);
        //    }
        //    DataList2.DataSource = dt;
        //    DataList2.DataBind();
        //}

    }
    public DataSet fill(Resource c)
    {
        var query = " SELECT * from content.Questions AS CQ join content.Resources AS CR on CQ.ressourceID=Cr.resourceID join academic.Users  AS AU on AU.userID= CQ.userID  where cq.ressourceID=" + c.id + "    select CC.questionID,CC.mssg,CC.commentID ,AU.userName from content.Questions as CQ join content.Comments AS CC ON CQ.questionID=CC.questionID join academic.Users  AS AU  on  AU.userID= CC.userID  JOIN content.Resources as CR ON CR.resourceID=CQ.ressourceID WHERE CQ.ressourceID = " + c.id;
        using (SqlConnection sq = new SqlConnection(ConfigurationManager.ConnectionStrings["connString"].ToString()))
        {

            var cmd = sq.CreateCommand();
            cmd.CommandText = query;
            SqlDataAdapter ad = new SqlDataAdapter();
            ad.SelectCommand = cmd;
            DataSet se = new DataSet();
            ad.Fill(se);
            return se;
        }

    }
    protected void dlstCourseList_SelectedIndexChanged(object sender, EventArgs e)
    {
        Professor s = Session[Constants.UserName] as Professor;
        DataTable dt = new DataTable();
        dt.Clear();
        List<Course> c = Course.GetProfCourses(s.profId);
        dt.Columns.Add("Lectures");
        dt.Columns.Add("Link");
        Course course = c[dlstCourseList.SelectedIndex];
        List<Lecture> l = Lecture.getLectures(course);
        for (int i = 0; i < l.Count; i++)
        {
            DataRow row = dt.NewRow();
            row["Lectures"] = l[i].title;
            row["Link"] = i;
            dt.Rows.Add(row);
        }
        ViewState[Constants.LecturersInCourse] = l;
        DataList1.DataSource = dt;
        Session[Constants.LecturersInCourse] = dt;
        DataList1.DataBind();
    }
    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        List<Lecture> ls = ViewState[Constants.LecturersInCourse] as List<Lecture>;
        int t = Int16.Parse((sender as LinkButton).CommandArgument.ToString());
        keyURL.Text = ls[t].path;
        ViewState[Constants.CurrentLecture] = ls[t];
        DataSet se = fill(ls[t]);
        se.Relations.Add(new DataRelation("DataList2", se.Tables[0].Columns["questionID"], se.Tables[1].Columns["questionID"]));
        se.Relations[0].Nested = true;
        DataList2.DataSource = se.Tables[0];
        DataList2.DataBind();

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        Lecture l = ViewState[Constants.CurrentLecture] as Lecture;
        Professor s = Session[Constants.UserName] as Professor;
        l.addQuestion(TextBox1.Text, s.userName);
        if (ViewState[Constants.CurrentLecture] != null)
        {

            Lecture lt = ViewState[Constants.CurrentLecture] as Lecture;
            DataSet se = fill(lt);
            se.Relations.Add(new DataRelation("DataList2", se.Tables[0].Columns["questionID"], se.Tables[1].Columns["questionID"]));
            se.Relations[0].Nested = true;
            DataList2.DataSource = se.Tables[0];
            DataList2.DataBind();
        }

    }

    protected void DataList2_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item)
        {

            DataRowView drv = e.Item.DataItem as DataRowView;
            DataList DataListComments = e.Item.FindControl("DataList4") as DataList;
            DataListComments.DataSource = drv.CreateChildView("DataList2");
            DataListComments.DataBind();

        }
    }
    protected void DataList2_ItemDataBound1(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item)
        {

            DataRowView drv = e.Item.DataItem as DataRowView;
            DataList DataListComments = e.Item.FindControl("DataList4") as DataList;
            DataListComments.DataSource = drv.CreateChildView("DataList2");
            DataListComments.DataBind();

        }
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        Professor s= Session[Constants.UserName] as Professor;
        int x = Int16.Parse((sender as Button).CommandArgument.ToString());
        TextBox d = (TextBox)(DataList2.FindControl("TextBox2"));
        Question.getQuestion(x).addComment(d.Text, s.userName);
    }


    protected void DataList2_ItemCommand(object source, DataListCommandEventArgs e)
    {
        if (e.CommandName == "A")
        {
            Professor s = Session[Constants.UserName] as Professor;
            Button z = (Button)e.CommandSource;

            int x = Int16.Parse(z.CommandArgument.ToString());
            TextBox d = ((TextBox)(e.Item.FindControl("TextBox2")));
            Question.getQuestion(x).addComment(d.Text, s.userName);
            if (ViewState[Constants.CurrentLecture] != null)
            {

                Lecture lt = ViewState[Constants.CurrentLecture] as Lecture;
                DataSet se = fill(lt);
                se.Relations.Add(new DataRelation("DataList2", se.Tables[0].Columns["questionID"], se.Tables[1].Columns["questionID"]));
                se.Relations[0].Nested = true;
                DataList2.DataSource = se.Tables[0];
                DataList2.DataBind();
            }
        }
    }
}
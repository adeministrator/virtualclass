﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MiddleMP.master" AutoEventWireup="true" CodeFile="profPersonalInfo.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
    .auto-style3 {
        width: 333px;
    }
    .auto-style4 {
        width: 259px;
    }
    .auto-style5 {
        width: 333px;
        height: 26px;
    }
    .auto-style6 {
        width: 259px;
        height: 26px;
    }
    .auto-style7 {
        height: 26px;
    }
    .auto-style8 {
        height: 26px;
        width: 297px;
    }
    .auto-style9 {
        width: 297px;
    }
        .auto-style10 {
            width: 333px;
            height: 20px;
        }
        .auto-style11 {
            width: 259px;
            height: 20px;
        }
        .auto-style12 {
            width: 297px;
            height: 20px;
        }
        .auto-style13 {
            height: 20px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="menu2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="middleSide" Runat="Server">
    <table style="width: 100%;">
    <tr>
        <td class="auto-style5"></td>
        <td class="auto-style6">
            <asp:Label ID="Label1" runat="server" Text="Name"></asp:Label>
        </td>
        <td class="auto-style8">
            <asp:Label ID="lblName" runat="server"></asp:Label>
        </td>
        <td class="auto-style7"></td>
        <td class="auto-style7"></td>
    </tr>
    <tr>
        <td class="auto-style3">&nbsp;</td>
        <td class="auto-style4">
            <asp:Label ID="Label2" runat="server" Text="Surname"></asp:Label>
        </td>
        <td class="auto-style9">
            <asp:Label ID="lblSurname" runat="server"></asp:Label>
        </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td class="auto-style3">&nbsp;</td>
        <td class="auto-style4">
            <asp:Label ID="Label3" runat="server" Text="User Name"></asp:Label>
        </td>
        <td class="auto-style9">
            <asp:Label ID="lblUserName" runat="server"></asp:Label>
        </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td class="auto-style3">&nbsp;</td>
        <td class="auto-style4">
            <asp:Label ID="Label4" runat="server" Text="E-mail Address"></asp:Label>
        </td>
        <td class="auto-style9">
            <asp:Label ID="lblEmail" runat="server"></asp:Label>
        </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td class="auto-style3">&nbsp;</td>
        <td class="auto-style4">
            <asp:Label ID="Label5" runat="server" Text="Phone Number"></asp:Label>
        </td>
        <td class="auto-style9">
            <asp:Label ID="lblPhone" runat="server"></asp:Label>
        </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td class="auto-style3">&nbsp;</td>
        <td class="auto-style4">
            <asp:Label ID="Label6" runat="server" Text="Title"></asp:Label>
        </td>
        <td class="auto-style9">
            <asp:Label ID="lblTitle" runat="server"></asp:Label>
        </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td class="auto-style10"></td>
        <td class="auto-style11">
            <asp:Label ID="Label7" runat="server" Text="Department"></asp:Label>
        </td>
        <td class="auto-style12">
            <asp:Label ID="lblDepartment" runat="server"></asp:Label>
        </td>
        <td class="auto-style13"></td>
        <td class="auto-style13"></td>
    </tr>
    <tr>
        <td class="auto-style3">&nbsp;</td>
        <td class="auto-style4">
            <asp:Label ID="Label16" runat="server" Text="Personal Information"></asp:Label>
        </td>
        <td class="auto-style9">
            <asp:Label ID="lblPersonalInfo" runat="server"></asp:Label>
        </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td class="auto-style3">&nbsp;</td>
        <td class="auto-style4">
            <asp:Label ID="Label17" runat="server" Text="working hours"></asp:Label>
        </td>
        <td class="auto-style9">
            <asp:Label ID="work" runat="server"></asp:Label>
        </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td class="auto-style3">&nbsp;</td>
        <td class="auto-style4">&nbsp;</td>
        <td class="auto-style9">
            <asp:Button ID="btnGoEditPersonalInfoProf" runat="server" OnClick="btnGoEditPersonalInfoProf_Click" Text=" Edit Personal Info " />
        </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
        <asp:Image ID="Image1" runat="server" ImageUrl='<%#"images//"+Eval("picture") %>' Height="130px" Width="154px" />
</table>
</asp:Content>

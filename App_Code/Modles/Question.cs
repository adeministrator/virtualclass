﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Question
/// </summary>
/// 

namespace virtualclass.modles
{
    [Serializable]
    public class Question
    {
        public int questionID { private set; get; }
        public string mssg { set; get; }
        public string userID { private set; get; }
        public bool isResolved { set; get; }
        public int ressourceID { private set; get; }

        public int isDeleted { private set; get; }

        private static string ConnectionString = ConfigurationManager.ConnectionStrings["connString"].ToString();


        public static List< Question> getQuestionsOfRsource(Resource resource)
        {
            List<Question> questions = new List<Question>();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.Open();
                var cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "QuestionByResource";
                cmd.Parameters.AddWithValue("@ResourceID", resource.id);
                SqlDataAdapter dt = new SqlDataAdapter();
                DataTable da = new DataTable();
                dt.Fill(da);
                foreach (DataRow row in da.Rows)
                {
                    questions.Add(Question.getQuestion((int)row["questionID"]));
                }
            }

            return questions;
        }

        public void addComment(string msg ,string username)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                con.Open();
                var cmd = con.CreateCommand();
                cmd.CommandText = "content.AddNewComment";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Msg",msg);
                cmd.Parameters.AddWithValue("@UserName", username);
                cmd.Parameters.AddWithValue("@QuestionID", questionID);
                var pr=cmd.Parameters.Add("@CommentID", SqlDbType.Int);
                pr.Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
             
            }

        }
        public static Question getQuestion(int id)
        {
            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                conn.Open();
                var cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "content.QuestionByID";
                cmd.Parameters.AddWithValue("@questionID", id);
                SqlDataAdapter ad = new SqlDataAdapter();
                DataTable dt = new DataTable();
                ad.SelectCommand = cmd;
                ad.Fill(dt);
                 return new Question(dt);
            }

        }
        public Question(DataTable tb)
        {
            if (tb.Rows.Count > 0)
            {
                DataRow row = tb.Rows[0];
                questionID = (int)row["questionID"];
                ressourceID = (int)row["ressourceID"];
                mssg = row["mssg"].ToString();
                isResolved = (bool)row["isResolved"];

            }
        }

    }
} 
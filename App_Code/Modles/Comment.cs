﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Comment
/// </summary>
/// 


namespace virtualclass.modles
{
    public class Comment
    {
        public int commentID { private set; get; }
        public string mssg { set; get; }
        public int userID { private set; get; }
        public int questionID { private set; get; }
        public bool isDeleted { set; get; }
        private static string ConnectionString = ConfigurationManager.ConnectionStrings["connString"].ToString();

        public static List<Comment> getComments(Question question) {
            List<Comment> Comments = new List<Comment>();
            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                conn.Open();
                var cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@QuestionId", question.questionID);
                cmd.CommandText = "content.CommentsByQuestion";
                SqlDataAdapter dp = new SqlDataAdapter();
                DataTable dt = new DataTable();
                dp.Fill(dt);
                foreach (DataRow row in dt.Rows)
                {
                    Comments.Add(Comment.getComment((int)row["questionID"]));
                }

            }
            return Comments;
        }
        public static Comment getComment(int id)
        {
           
            using (SqlConnection conn = new SqlConnection())
            {

                conn.Open();
                var cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "content.CommentByID";
                cmd.Parameters.AddWithValue("@CommentID", id);
                SqlDataAdapter dt = new SqlDataAdapter();
                DataTable td = new DataTable();
                dt.Fill(td);
                return new Comment(td);

            }

        }
        public Comment(DataTable dt)
        {

            if (dt.Rows.Count > 0)
            {
                DataRow row = dt.Rows[0];
                isDeleted = (bool)row["isDeleted"];
                questionID = (int)row["questionID"];
                userID = (int)row["userID"];
                mssg = row["mssg"].ToString();
                commentID = (int)row["commentID"];

            }
        }



    }
}
﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using virtualclass.modles;

/// <summary>
/// Summary description for Resource
/// </summary>
/// 

namespace virtualclass.modles{
    [Serializable]
public class Resource
{

    public int id { get; private set; }
    public int courseID { get; private set; }
    public string path { set; get; }
    public int profId { get; private set; }
    bool isDeleted { set; get;}

    private static string ConnectionString=ConfigurationManager.ConnectionStrings["connString"].ToString();

    
    
    public static Resource getResource(int id)
    {
        using (SqlConnection conn = new SqlConnection(ConnectionString))
        {
            conn.Open();
            var cmd = conn.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "content.ResourceByID";
            cmd.Parameters.AddWithValue("@ResourceID", id);
            SqlDataAdapter ad = new SqlDataAdapter();
            ad.SelectCommand = cmd;
            DataTable dt = new DataTable();
            return new Resource(dt);
        }
        
    }
    public static List<Resource> getResourcesOfCourse(Course course){
        List<Resource> result=new List<Resource>();
        using (SqlConnection conn=new SqlConnection(ConnectionString)){
             conn.Open();
             var cmd=conn.CreateCommand();
             cmd.CommandText="academic.ResourcesByCourse";
             cmd.CommandType=CommandType.StoredProcedure;
             cmd.Parameters.AddWithValue("@CourseCode",course.code);
             SqlDataAdapter ad=new SqlDataAdapter();
            ad.SelectCommand=cmd;
            DataTable dt=new DataTable();
            ad.Fill(dt);
            foreach(DataRow row in dt.Rows){
              result.Add(Resource.getResource((int)row["resourceID"]));
            }
            
        }
       

        return result;
    }
    public Resource(DataTable tb)
    {
        if (tb.Rows.Count > 0) {
            DataRow row = tb.Rows[0];
            this.courseID = (int)row["courseID"];
            this.id = (int)row["resourceID"];
            this.isDeleted = (bool)row["isDeleted"];
            this.path = row["Path"].ToString();
            this.profId=(int)row["profId"];
        }


    }
    public Question addQuestion(string msg,string username){
        using (SqlConnection conn = new SqlConnection(ConnectionString))
        {
            conn.Open();
            var cmd = conn.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "[content].[AddNewQuestion]";
            cmd.Parameters.AddWithValue("@Msg", msg);
            cmd.Parameters.AddWithValue("@UserName", username);
            cmd.Parameters.AddWithValue("@IsResolved",0);
            cmd.Parameters.AddWithValue("@ResourceID", id);
            var par=cmd.Parameters.AddWithValue("@QuestionID",SqlDbType.Int);
            cmd.ExecuteNonQuery();

            return Question.getQuestion((int)par.Value);
        }

    }
}
}
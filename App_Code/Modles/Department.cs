﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Department
/// </summary>
/// 
namespace virtualclass.modles
{
public class Department
{
    public int depId { protected set; get; }
    public string departmentName { protected set; get; }
    public string descrption { protected set; get; }

    private static string ConnectionString = ConfigurationManager.ConnectionStrings["connString"].ToString();
    public static Department getDepartment(string name)
    {
        using (SqlConnection conn = new SqlConnection(ConnectionString))
        {
            conn.Open();
            var cmd = conn.CreateCommand();
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "academic.DepartmentByName";
            cmd.Parameters.AddWithValue("@DepartmentName", name);
            SqlDataAdapter ad = new SqlDataAdapter();
            ad.SelectCommand = cmd;
            DataTable dt = new DataTable();
            ad.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                DataRow row = dt.Rows[0];
                Department dep = new Department {
                    descrption = row["departmentDescription"].ToString(),
                    depId=(int)row["departmentID"],
                    departmentName = row["departmentName"].ToString()
                };
                return dep;
            }
        }
        return null;
    }
    public bool AddCourse(string code)
    {
        using (SqlConnection conn = new SqlConnection(ConnectionString))
        {
            conn.Open();
            var cmd = conn.CreateCommand();
            cmd.CommandText = "academic.AddCourseToDepartment";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@DepartmentName", this.departmentName);
            cmd.Parameters.AddWithValue("@CourseCode", code);
            return cmd.ExecuteNonQuery() > 0;

        }
    }
    public static List<Department> getDepartments()
    {
        List<Department> result=new List<Department>();

        using (SqlConnection conn = new SqlConnection(ConnectionString))
        {
             conn.Open();
            var cmd = conn.CreateCommand();
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "academic.GetDepartments";
            SqlDataAdapter ad = new SqlDataAdapter();
            ad.SelectCommand = cmd;
            DataTable dt = new DataTable();
            ad.Fill(dt);
            foreach (DataRow row in dt.Rows)
            {
                result.Add(getDepartment(row["departmentName"].ToString()));
            }
        }
        return result;
    }
    public void AddUser(User user)
    {
        using (SqlConnection conn = new SqlConnection(ConnectionString))
        {
            conn.Open();
            var cmd = conn.CreateCommand();
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "academic.AddUserToDepartment";
            cmd.Parameters.AddWithValue("@UserName", user.userName);
            cmd.Parameters.AddWithValue("@DepartementName", departmentName);
            cmd.ExecuteNonQuery();
            
        }
    }

}
}
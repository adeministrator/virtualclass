﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Lecture
/// </summary>
/// 
namespace virtualclass.modles
{
    [Serializable]
    public class Lecture:Resource
    {
        public int lectureID{get;private set;}
        public string title { get; set; }
        public int resourceID { set; get; }
        public int number { set; get; }

        private static string ConnectionString = ConfigurationManager.ConnectionStrings["connString"].ToString();
        public static Lecture getLecture(int lectureID)
        {
            using (SqlConnection sql = new SqlConnection(ConnectionString))
            {
                sql.Open();
                var cmd = sql.CreateCommand();
                cmd.CommandText = "content.LectureById";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@LectureID", lectureID);
                DataTable dt = new DataTable();
                SqlDataAdapter ad = new SqlDataAdapter();
                ad.SelectCommand = cmd;
                ad.Fill(dt);
                return new Lecture(dt);
            }
        }
        public static List<Lecture> getLectures(Course course)
        {
            List<Lecture> result = new List<Lecture>();
            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                conn.Open();
                var cmd = conn.CreateCommand();
                cmd.CommandText = "content.LecturerByCourse";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CourseCode", course.code);
                
                SqlDataAdapter ad = new SqlDataAdapter();
                ad.SelectCommand = cmd;
                DataTable dt = new DataTable();
                ad.Fill(dt);
                foreach (DataRow row in dt.Rows)
                {
                    result.Add(Lecture.getLecture((int)row["lectureID"]));
                }

            }

            return result;

        }
        public Lecture(DataTable tb):base(tb)
        {
            if (tb.Rows.Count > 0)
            {
                DataRow row = tb.Rows[0];
                lectureID = (int)row["lectureID"];
                title = row["title"].ToString();
                resourceID =(int) row["resourceID"];
                number = (int)row["number"];
            }
        }
    }
}
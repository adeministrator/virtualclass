﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data.Common;

/// <summary>
/// Summary description for User
/// </summary>
namespace virtualclass.modles
{
    [Serializable]
    public abstract class User
    {
        public enum UserType { Student, Professor, Uknown };
        public int userId { get; protected set; }
        public string password {private get; set; }
        public string userName { get; protected set; }

        protected string _userType;
        public UserType userType { get; protected set; }
        public string name { get; set; }
        public string surname { get; set; }
        public string picture { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string personalInfo { set; get; }
        public bool isActive { set; get; }

        private static string ConnectionString = ConfigurationManager.ConnectionStrings["connString"].ToString();

        public static bool IsLoginCorrect(String userName, String password)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                var cmd = connection.CreateCommand();
                connection.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "academic.DoesUserExistAndHisAuthenticationInfoISCorrect";
                cmd.Parameters.AddWithValue("@UserName", userName);
                cmd.Parameters.AddWithValue("@PassWord", password);
                var result = cmd.Parameters.Add("@Result", System.Data.SqlDbType.Bit);
                result.Direction = System.Data.ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                var r = result.Value;
                return (bool)r;
            }
        }

        virtual public bool updateInfo()
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                var cmd = connection.CreateCommand();
                connection.Open();
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "academic.UpdateUserInfo";
                cmd.Parameters.AddWithValue("@UserName", userName);
                cmd.Parameters.AddWithValue("@Password", password);
                cmd.Parameters.AddWithValue("@Picture", picture);
                cmd.Parameters.AddWithValue("@Email", email);
                cmd.Parameters.AddWithValue("@Phone", phone);
                cmd.Parameters.AddWithValue("@PersonalInfo", personalInfo);
                cmd.Parameters.AddWithValue("@Name", name);
                cmd.Parameters.AddWithValue("@Surname", surname);

                var d = cmd.ExecuteNonQuery();
                return d > 0;
            }
        }
        public static UserType typeOFUser(string userName)
        {

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                var cmd = connection.CreateCommand();
                connection.Open();
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "academic.[TypeOFUser]";
                cmd.Parameters.AddWithValue("@UserName", userName);
                var par = cmd.Parameters.Add("@Type", SqlDbType.NVarChar);
                par.Size = 10;
                par.Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                if (par.Value.ToString() == "STUDENT")
                    return UserType.Student;
                else if (par.Value.ToString() == "PROF")
                    return UserType.Professor;

            } 

            return UserType.Uknown;
        }

        protected User(DataTable tb)
        {
            if (tb.Rows.Count > 0)
            {
                DataRow row = tb.Rows[0];
                name = row["name"].ToString();
                userId = (int)row[0];
                userName = row["userName"].ToString();
                if (row["userType"].ToString() == "STUDENT")
                    userType = UserType.Student;
                else if (row["userType"].ToString() == "PROF")
                    userType = UserType.Professor;
                else
                    userType = UserType.Uknown;
                email = row["email"].ToString();
                surname = row["surname"].ToString();
                password = row["pass"].ToString();
                phone = row["phone"].ToString();
                personalInfo = row["personalInfo"].ToString();
                picture = row["picture"].ToString();
                isActive = (bool)row["isActive"];
            }
        }
        public List<Department> getDepartments()
        {
            List<Department> result = new List<Department>();
            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                conn.Open();
                var cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "academic.DepartmentsOfUser";
                cmd.Parameters.AddWithValue("@UserName", userName);
                DataTable tb = new DataTable();
                SqlDataAdapter ad = new SqlDataAdapter();
                ad.SelectCommand = cmd;
                ad.Fill(tb);
                foreach (DataRow row in tb.Rows)
                {
                    result.Add(Department.getDepartment(row["departmentName"].ToString()));
                }
                return result;
            }

        }
        public User()
        {

        }

    }
}
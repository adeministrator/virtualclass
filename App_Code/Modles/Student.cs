﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Student
/// </summary>


namespace virtualclass.modles
{
    [Serializable]
   public class Student : User
    {
        public int studentID { private set; get; }

       public DateTime graduatingDate { set; get; }

        private static string ConnectionString = ConfigurationManager.ConnectionStrings["connString"].ToString();
        public static Student Login(string UserName, string password)
        {
            Student result = null;
            if (User.IsLoginCorrect(UserName, password) == true)
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    connection.Open();
                    var cmd = connection.CreateCommand();
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandText = "academic.StudentByUserName";
                    cmd.Parameters.AddWithValue("@UserName", UserName);
                    SqlDataAdapter adp = new SqlDataAdapter();
                    DataTable tb = new DataTable();
                    adp.SelectCommand = cmd;
                    adp.Fill(tb);
                    result = new Student(tb);
                   

                }
            }
            return result;
        }
        private Student(DataTable tb):base(tb)
        {
            if (tb.Rows.Count > 0)
            {
                DataRow row = tb.Rows[0];
                if (!String.IsNullOrWhiteSpace(row["garduationDate"].ToString()))
                    graduatingDate = DateTime.Parse(row["garduationDate"].ToString());
                studentID = (int)row["stuID"];
             
            }
            
        }
        public Student()
        {

        }
        public static Student AddNewUser(string userName,string password, string name, string surname, string phone, string personalInfo,string email,DateTime gradDate )
        {
            using (SqlConnection sql = new SqlConnection(ConnectionString))
            {
                sql.Open();
                var cmd = sql.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "academic.AddNewStudent";
                cmd.Parameters.AddWithValue("@UserName", userName);
                cmd.Parameters.AddWithValue("@Surname", surname);
                cmd.Parameters.AddWithValue("@Password", password);
                cmd.Parameters.AddWithValue("@Name", name);
                cmd.Parameters.AddWithValue("@Picture", "/Students/images/default.jpg");
                cmd.Parameters.AddWithValue("@phone", phone);
                cmd.Parameters.AddWithValue("@PersonalInf", personalInfo);
                cmd.Parameters.AddWithValue("@GradDate",gradDate.Date );
                cmd.Parameters.AddWithValue("@Email", email);
                var result = cmd.ExecuteNonQuery();
                if (result > 0)
                {
                    Student res = Student.Login(userName, password);
                    return res;

                }
                
            }


            return null;
        }

        public static Student getStudent(string username)
        {
            Student result = null;
            using (SqlConnection sql = new SqlConnection(ConnectionString))
            {
                sql.Open();
                var cmd = sql.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "academic.UserByUserName";
                cmd.Parameters.AddWithValue("@UserName", username);
                DataTable dt=new DataTable();
                SqlDataAdapter ad = new SqlDataAdapter();
                ad.SelectCommand = cmd;
                ad.Fill(dt);
                result = new Student(dt);
            }
            return result;
        }


        public static Student getStudent(int studentId)
        {
            using (SqlConnection sql = new SqlConnection(ConnectionString))
            {
                sql.Open();
                var cmd = sql.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "academic.StudentById";
                cmd.Parameters.AddWithValue("@StdId", studentId);
                DataTable dt = new DataTable();
                
                SqlDataAdapter ad = new SqlDataAdapter();
                ad.SelectCommand = cmd;
                ad.Fill(dt);
                return new Student(dt);
            }
        }
        public override bool updateInfo()
        {
            var t1 = base.updateInfo();
            using (SqlConnection sql = new SqlConnection(ConnectionString))
            {
                sql.Open();
                var cmd = sql.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "academic.UpdateStudentInfo";
                cmd.Parameters.AddWithValue("@Stdid",studentID);
                cmd.Parameters.AddWithValue("@gradDate",graduatingDate.Date);

                return cmd.ExecuteNonQuery() > 0 && t1;
            }
        }
    }
}
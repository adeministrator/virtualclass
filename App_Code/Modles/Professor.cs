﻿
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace virtualclass.modles
{
    [Serializable]
  public  class Professor:User
    {
        public int profId { get; private set; }
        public int workingHours{set;get;}

        public string title{set;get;}
        protected int userid;
        private static string ConnectionString = ConfigurationManager.ConnectionStrings["connString"].ToString();
        public static Professor getProfessor(String userName) 
        {
            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                conn.Open();
                var cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "academic.ProfessorByUserName";
                cmd.Parameters.AddWithValue("@ProfUserName", userName);
                SqlDataAdapter ad = new SqlDataAdapter();
                ad.SelectCommand = cmd;
                DataTable tb = new DataTable();
                ad.Fill(tb);
                return new Professor(tb);
                
            }
            

            
        }
        public static Professor getProfessor(int profD)
        {
            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                conn.Open();
                var cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "academic.ProfessorByProfID";
                cmd.Parameters.AddWithValue("@ProfID", profD);
                SqlDataAdapter ad = new SqlDataAdapter();
                ad.SelectCommand = cmd;
                DataTable tb = new DataTable();
                ad.Fill(tb);
                return new Professor(tb);

            }
           
        }


        public static Professor addNewProfessor(string username,string password,string name,string surname,string picture,string email,string phone,string personalinfo,int workinghours ,string title)
        {
            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                conn.Open();
                var cmd = conn.CreateCommand();
                cmd.CommandText = "[academic].[AddNewProf]";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@UserName",username);
                cmd.Parameters.AddWithValue("@Password",password);
                cmd.Parameters.AddWithValue("@Name",name);
                cmd.Parameters.AddWithValue("@Surname",surname);
                cmd.Parameters.AddWithValue("@Picture",picture);
                cmd.Parameters.AddWithValue("@Email",email);
                cmd.Parameters.AddWithValue("@Phone", phone);
                cmd.Parameters.AddWithValue("@PersonalInf",personalinfo);
                cmd.Parameters.AddWithValue("@WorkingHours",workinghours);
                cmd.Parameters.AddWithValue("@Title", title);
                var x = cmd.ExecuteNonQuery();
                if (x > 0)
                    return getProfessor(username);
            }       
            return null;

        }
        public override bool updateInfo()
        {
            var t1 = base.updateInfo();
            using (SqlConnection sql = new SqlConnection(ConnectionString))
            {
                sql.Open();
                var cmd = sql.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "academic.UpdateProfessorInfo";
                cmd.Parameters.AddWithValue("@ProfID", profId);
                cmd.Parameters.AddWithValue("@WorkingHours", workingHours);
                cmd.Parameters.AddWithValue("@Title", title);

                return cmd.ExecuteNonQuery() > 0 && t1;
            }
        }
        private Professor(DataTable tb)
            : base(tb)
        {
            if (tb.Rows.Count > 0)
            {
                DataRow row = tb.Rows[0];
                profId = (int)row["profID"];
                workingHours = (int)row["workingHours"];
                title = row["title"].ToString();

            }
        }

    }
}

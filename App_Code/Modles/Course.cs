﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
/// <summary>
/// Summary description for course
/// </summary>
namespace virtualclass.modles
{
    public class Course
    {

        string connectionString = System.Configuration.ConfigurationManager.AppSettings["connString"];
        public int coursid { get; private set; }

        public string courseName { get; private set; }

        public string term { get; set; }
        public string couseDiscription { get; set; }
        public string code { get; set; }
        public int tid { get; set; }
        public int profid { get; set; }
        bool isActive { set; get; }

        public Course()
        {

        }
        public static List<Course> GetCourses()
        {
            List<Course> course = new List<Course>();
            string connectionString = System.Configuration.ConfigurationManager.AppSettings["connString"];
            DataTable dt = new DataTable();
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            string qurey = "exec academic.getCourses";
            SqlCommand cmd = new SqlCommand(qurey, conn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);

            adp.Fill(dt);
            foreach (DataRow r in dt.Rows)
            {
                var f = r["tID"].ToString();
                if (!String.IsNullOrWhiteSpace(f))
                {
                    course.Add(new Course
                    {
                        coursid = (int)r["courseID"],
                        courseName = r["courseName"].ToString(),
                        term = r["term"].ToString(),
                        couseDiscription = r["courseDescription"].ToString(),
                        code = r["code"].ToString(),
                        tid = (int)r["tID"],
                        profid = (int)r["profID"],
                        isActive = (bool)r["isActive"],
                    });
                }
                else
                {
                    course.Add(new Course
                    {
                        coursid = (int)r["courseID"],
                        courseName = r["courseName"].ToString(),
                        term = r["term"].ToString(),
                        couseDiscription = r["courseDescription"].ToString(),
                        code = r["code"].ToString(),
                        tid = 0,
                        profid = (int)r["profID"],
                        isActive = (bool)r["isActive"],
                    });
                }

            }

            return course;
        }

        public static List<Course> GetStudentCourses(int id)
        {
            List<Course> course = new List<Course>();
            string connectionString = System.Configuration.ConfigurationManager.AppSettings["connString"];
            DataTable dt = new DataTable();
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            string qurey = "academic.getstudentcourses @id=@ID";
            SqlCommand cmd = new SqlCommand(qurey, conn);
            cmd.Parameters.AddWithValue("@ID", id);// stud.userId
            SqlDataAdapter adp = new SqlDataAdapter(cmd);

            adp.Fill(dt);
            foreach (DataRow r in dt.Rows)
            {
                var f = r["tID"].ToString();
                if (!String.IsNullOrWhiteSpace(f))
                {
                    course.Add(new Course
                    {
                        coursid = (int)r["courseID"],
                        courseName = r["courseName"].ToString(),
                        term = r["term"].ToString(),
                        couseDiscription = r["courseDescription"].ToString(),
                        code = r["code"].ToString(),
                        tid = (int)r["tID"],
                        profid = (int)r["profID"],
                        isActive = (bool)r["isActive"],
                    });
                }
                else
                {
                    course.Add(new Course
                    {
                        coursid = (int)r["courseID"],
                        courseName = r["courseName"].ToString(),
                        term = r["term"].ToString(),
                        couseDiscription = r["courseDescription"].ToString(),
                        code = r["code"].ToString(),
                        tid = 0,
                        profid = (int)r["profID"],
                        isActive = (bool)r["isActive"],
                    });
                }

            }

            return course;
        }

        public static DataTable GetStudentCoursesTa(int id)
        {

            string connectionString = System.Configuration.ConfigurationManager.AppSettings["connString"];
            DataTable dt = new DataTable();
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            string qurey = "academic.getstudentcourses @id=@ID";
            SqlCommand cmd = new SqlCommand(qurey, conn);
            cmd.Parameters.AddWithValue("@ID", id);// stud.userId
            SqlDataAdapter adp = new SqlDataAdapter(cmd);

            adp.Fill(dt);


            return dt;
        }

        public static DataTable GetCoursesTa() //SAME but of type data
        {

            string connectionString = System.Configuration.ConfigurationManager.AppSettings["connString"];
            DataTable dt = new DataTable();
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            string qurey = "exec academic.getCourses";
            SqlCommand cmd = new SqlCommand(qurey, conn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);

            adp.Fill(dt);


            return dt;
        }


        public static List<Course> GetProfCourses(int id)// Professor pro 
        {
            List<Course> course = new List<Course>();
            string connectionString = System.Configuration.ConfigurationManager.AppSettings["connString"];
            DataTable dt = new DataTable();
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            string qurey = "exec  academic.getprofcourses @id=@ID";
            SqlCommand cmd = new SqlCommand(qurey, conn);
            cmd.Parameters.AddWithValue("@ID", id);// pro.profId
            SqlDataAdapter adp = new SqlDataAdapter(cmd);


            adp.Fill(dt);
            foreach (DataRow r in dt.Rows)
            {
                var f = r["tID"].ToString();
                if (!String.IsNullOrWhiteSpace(f))
                {
                    course.Add(new Course
                    {
                        coursid = (int)r["courseID"],
                        courseName = r["courseName"].ToString(),
                        term = r["term"].ToString(),
                        couseDiscription = r["courseDescription"].ToString(),
                        code = r["code"].ToString(),
                        tid = (int)r["tID"],
                        profid = (int)r["profID"],
                        isActive = (bool)r["isActive"],
                    });
                }
                else
                {
                    course.Add(new Course
                    {
                        coursid = (int)r["courseID"],
                        courseName = r["courseName"].ToString(),
                        term = r["term"].ToString(),
                        couseDiscription = r["courseDescription"].ToString(),
                        code = r["code"].ToString(),
                        tid = 0,
                        profid = (int)r["profID"],
                        isActive = (bool)r["isActive"],
                    });
                }

            }
            return course;
        }

        public static DataTable GetProfCoursesTa(int id)// Professor pro 
        {

            string connectionString = System.Configuration.ConfigurationManager.AppSettings["connString"];
            DataTable dt = new DataTable();
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            string qurey = "exec  academic.getprofcourses @id=@ID";
            SqlCommand cmd = new SqlCommand(qurey, conn);
            cmd.Parameters.AddWithValue("@ID", id);// pro.profId
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(dt);

            return dt;
        }
        public static List<Course> GetDepCourses(string name)// department de 
        {
            List<Course> course = new List<Course>();
            string connectionString = System.Configuration.ConfigurationManager.AppSettings["connString"];
            DataTable dt = new DataTable();
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            string qurey = "exec academic.CoursesByDepartment @DepartmentName=@DName";
            SqlCommand cmd = new SqlCommand(qurey, conn);
            cmd.Parameters.AddWithValue("@DName", name);// de.departmentname
            SqlDataAdapter adp = new SqlDataAdapter(cmd);


            adp.Fill(dt);
            foreach (DataRow r in dt.Rows)
            {
                var f = r["tID"].ToString();
                if (!String.IsNullOrWhiteSpace(f))
                {
                    course.Add(new Course
                    {
                        coursid = (int)r["courseID"],
                        courseName = r["courseName"].ToString(),
                        term = r["term"].ToString(),
                        couseDiscription = r["courseDescription"].ToString(),
                        code = r["code"].ToString(),
                        tid = (int)r["tID"],
                        profid = (int)r["profID"],
                        isActive = (bool)r["isActive"],
                    });
                }
                else
                {
                    course.Add(new Course
                    {
                        coursid = (int)r["courseID"],
                        courseName = r["courseName"].ToString(),
                        term = r["term"].ToString(),
                        couseDiscription = r["courseDescription"].ToString(),
                        code = r["code"].ToString(),
                        tid = 0,
                        profid = (int)r["profID"],
                        isActive = (bool)r["isActive"],
                    });
                }

            }
            return course;
        }
        public static DataTable GetDepCoursesTa(string name)// department de 
        {

            string connectionString = System.Configuration.ConfigurationManager.AppSettings["connString"];
            DataTable dt = new DataTable();
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            string qurey = "exec academic.CoursesByDepartment @DepartmentName=@DName";
            SqlCommand cmd = new SqlCommand(qurey, conn);
            cmd.Parameters.AddWithValue("@DName", name);// de.departmentname
            SqlDataAdapter adp = new SqlDataAdapter(cmd);


            adp.Fill(dt);

            return dt;
        }
        public bool AddstudentTOCourses(string name, string courscode)// department de 
        {
            int precount = CountStudentCourses(courscode);
            int postcount = 0;
            bool result = false;
            string connectionString = System.Configuration.ConfigurationManager.AppSettings["connString"];
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            string qurey = "exec academic.AddStudentToCourse @UserName=@name,@CourseCode=@code";
            SqlCommand cmd = new SqlCommand(qurey, conn);
            cmd.Parameters.AddWithValue("@name", name);// de.departmentname
            cmd.Parameters.AddWithValue("@code", courscode);
            cmd.ExecuteNonQuery();
            postcount = CountStudentCourses(courscode);

            if (postcount > precount)
                result = true;

            return result;


        }
        public int CountStudentCourses(string code)
        {
            int count = 0;
            List<Course> course = new List<Course>();
            DataTable dt = new DataTable();
            string connectionString = System.Configuration.ConfigurationManager.AppSettings["connString"];
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            string qurey = "exec  academic.countstudentcourses @code=@cd";
            SqlCommand cmd = new SqlCommand(qurey, conn);
            cmd.Parameters.AddWithValue("@cd", code);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(dt);


            foreach (DataRow r in dt.Rows)
            {
                if (r[0].ToString() != "")
                    count = (int)r[0];
            }
            return count;

        }
        public bool AddCourses(string name, string courscode, string term, string desc, string tname, string pname)// department de 
        {
            int precount = CountCourses();
            int postcount = 0;
            bool result = false;
            string connectionString = System.Configuration.ConfigurationManager.AppSettings["connString"];
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            string qurey = "EXEC [academic].[AddNewCourse]@CourseName = @name,@CourseCode = @code,@Term = @term,"
            + "@CourseDescription = @desic,@TidUserName = @tnam,@ProfUserName = @prname";
            SqlCommand cmd = new SqlCommand(qurey, conn);
            cmd.Parameters.AddWithValue("@name", name);// de.departmentname
            cmd.Parameters.AddWithValue("@code", courscode);
            cmd.Parameters.AddWithValue("@term", term);
            cmd.Parameters.AddWithValue("@desic", desc);
            cmd.Parameters.AddWithValue("@tnam", tname);
            cmd.Parameters.AddWithValue("@prname", pname);
            cmd.ExecuteNonQuery();
            postcount = CountCourses();

            if (postcount > precount)
                result = true;

            return result;


        }
        public int CountCourses()
        {
            int count = 0;
            List<Course> course = new List<Course>();
            DataTable dt = new DataTable();
            string connectionString = System.Configuration.ConfigurationManager.AppSettings["connString"];
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            string qurey = "exec academic.countcourses";
            SqlCommand cmd = new SqlCommand(qurey, conn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(dt);


            foreach (DataRow r in dt.Rows)
            {
                if (r[0].ToString() != "")
                    count = (int)r[0];
            }
            return count;

        }
        public bool AddResource(string path, string courscode, string pname)
        {
            int precount = CountResource();
            int postcount = 0;
            bool result = false;
            string connectionString = System.Configuration.ConfigurationManager.AppSettings["connString"];
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            string qurey = "EXEC [content].[AddNewResources]@CourseCode = @code,@ProfUserName = @pname,@Path = @pth";
            SqlCommand cmd = new SqlCommand(qurey, conn);
            cmd.Parameters.AddWithValue("@pth", path);
            cmd.Parameters.AddWithValue("@code", courscode);
            cmd.Parameters.AddWithValue("@pname", pname);
            cmd.ExecuteNonQuery();
            postcount = CountResource();

            if (postcount > precount)
                result = true;

            return result;


        }
        public int CountResource()
        {
            int count = 0;
            List<Course> course = new List<Course>();
            DataTable dt = new DataTable();
            string connectionString = System.Configuration.ConfigurationManager.AppSettings["connString"];
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            string qurey = "exec academic.countresouce";
            SqlCommand cmd = new SqlCommand(qurey, conn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(dt);


            foreach (DataRow r in dt.Rows)
            {
                if (r[0].ToString() != "")
                    count = (int)r[0];
            }
            return count;

        }
        public bool AddLecture(string courscode, string pname, string utublink, string title, int num)
        {
            int precount = CountLecture(courscode);
            int postcount = 0;
            bool result = false;
            string connectionString = System.Configuration.ConfigurationManager.AppSettings["connString"];
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            string qurey = "EXEC [content].[AddNewLecture] @CourseCode = @code,@ProfUserName = @pname,@YoutubeLink = @link,"
                + "@title =@tit,	@Number =@numb";
            SqlCommand cmd = new SqlCommand(qurey, conn);
            cmd.Parameters.AddWithValue("@link", utublink);
            cmd.Parameters.AddWithValue("@code", courscode);
            cmd.Parameters.AddWithValue("@pname", pname);
            cmd.Parameters.AddWithValue("@numb", num);
            cmd.Parameters.AddWithValue("@tit", title);
            cmd.ExecuteNonQuery();
            postcount = CountLecture(courscode);

            if (postcount > precount)
                result = true;

            return result;


        }
        public int CountLecture(string coursecode)
        {
            int count = 0;
            List<Course> course = new List<Course>();
            DataTable dt = new DataTable();
            string connectionString = System.Configuration.ConfigurationManager.AppSettings["connString"];
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            string qurey = "exec academic.countlecture @coursecode=@code";
            SqlCommand cmd = new SqlCommand(qurey, conn);
            cmd.Parameters.AddWithValue("@code", coursecode);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(dt);


            foreach (DataRow r in dt.Rows)
            {
                if (r[0].ToString() != "")
                    count = (int)r[0];
            }
            return count;

        }
        public bool UpdateCoursesInfo(int courseid, string courscode, string coursname, string term, string desc, string tname, bool isactive)
        {
            bool result = false;
            string connectionString = System.Configuration.ConfigurationManager.AppSettings["connString"];
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            string qurey = "exec academic.Updatcourseinfo2 @id =@coid, @name=@couname,@term =@cterm, @desc =@disc,"
            + "@code =@crcod,@tname = @tiname,@isactiv = @is";
            SqlCommand cmd = new SqlCommand(qurey, conn);
            cmd.Parameters.AddWithValue("@coid", courseid);
            cmd.Parameters.AddWithValue("@couname", coursname);
            cmd.Parameters.AddWithValue("@cterm", term);
            cmd.Parameters.AddWithValue("@disc", desc);
            cmd.Parameters.AddWithValue("@crcod", courscode);
            cmd.Parameters.AddWithValue("@tiname", tname);
            cmd.Parameters.AddWithValue("@is", isactive);
            int i=cmd.ExecuteNonQuery();
            
            if (i > 0)
                result = true;
            return result;

        }
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CompleteMP.master" AutoEventWireup="true" CodeFile="HProf.aspx.cs" Inherits="Default2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .auto-style1 {
            width: 53px;
        }
        .auto-style2 {
            width: 123px;
        }
        .auto-style6 {
            width: 100%;
            height: 137px;
        }
        .auto-style7 {
            width: 53px;
            height: 69px;
        }
        .auto-style8 {
            width: 123px;
            height: 69px;
        }
        .auto-style9 {
            height: 69px;
        }
        .auto-style10 {
            width: 228px;
        }
        .auto-style11 {
            width: 228px;
            height: 69px;
        }
        .auto-style12 {
            width: 129%;
            height: 137px;
        }
        .auto-style13 {
            width: 26px;
        }
        .auto-style14 {
            width: 26px;
            height: 69px;
        }
        .auto-style15 {
            width: 100%;
            height: 324px;
        }
        .auto-style16 {
            height: 215px;
        }
        .auto-style17 {
            width: 380px;
        }
        .auto-style18 {
            height: 215px;
            width: 380px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="menu2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="leftSide" Runat="Server">
    
    &nbsp;<table class="auto-style6">
        <tr>
            <td class="auto-style1">&nbsp;</td>
            <td class="auto-style2">
               
                <asp:DropDownList ID="dlstCourseList" runat="server" 
                    OnSelectedIndexChanged="dlstCourseList_SelectedIndexChanged" AutoPostBack="true">
                </asp:DropDownList>
                    
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style1">&nbsp;</td>
            <td class="auto-style2">
                <asp:Label ID="lblCourseName" runat="server"></asp:Label>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style1">&nbsp;</td>
            <td class="auto-style2">
                 
                <asp:DataList ID="DataList1" runat="server">
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton1" Text='<%#Eval("Lectures") %>' CommandArgument='<%#Eval("Link") %>' runat="server" OnClick="LinkButton1_Click">LinkButton</asp:LinkButton>
                    </ItemTemplate>
                </asp:DataList>
                      
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style7">
               
            </td>
            <td class="auto-style8"></td>
            <td class="auto-style9"></td>
        </tr>
    </table>
  
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="middleSide" Runat="Server">
    <table class="auto-style15">
        <tr>
            <td>&nbsp;</td>
            <td class="auto-style17">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>

        <iframe id="YoutubePlayer" width="500" height="300" src="https://www.youtube.com/embed/<asp:Literal ID="keyURL" runat="server"></asp:Literal>" frameborder="0" allowfullscreen></iframe>
        <tr>
            <td class="auto-style16"></td>
            <td class="auto-style18"></td>
            <td class="auto-style16"></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td class="auto-style17">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="rightSide" Runat="Server">
   
      
    <table class="auto-style12">
        <tr>
            <td class="auto-style13">&nbsp;</td>
            <td class="auto-style10">
                <asp:TextBox ID="TextBox1" runat="server" Height="93px" Width="220px" TextMode="MultiLine" ></asp:TextBox>
                <br />
                <asp:Button ID="Button1" runat="server" Height="31px" OnClick="Button1_Click" Text="Ask a question" Width="172px" />
                <br />
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style13">&nbsp;</td>
            <td class="auto-style10">
                <asp:DataList ID="DataList2" RepeatLayout="Flow" runat="server" OnItemDataBound="DataList2_ItemDataBound1" OnItemCommand="DataList2_ItemCommand" OnSelectedIndexChanged="DataList2_SelectedIndexChanged"  >
                    <ItemTemplate>
                        <asp:Label ID="Label3" runat="server" Text='<%#Eval("mssg") %>'></asp:Label>
                        <asp:Label ID="Label5" runat="server" Text='<%#"@"+Eval("UserName") %>'></asp:Label>
                        <br />
                        <table style="width:100%;">
                            <tr>
                                <td>
                                    <asp:TextBox ID="TextBox2" ommandArgument='<%#Eval("questionID") %>'  runat="server" Height="16px" Width="192px" ></asp:TextBox>
                                </td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Button ID="Button3" runat="server" CommandArgument='<%#Eval("questionID") %>'   Text="Comment" CommandName="A" OnClick="Button3_Click" />
                                </td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <asp:DataList ID="DataList4" runat="server">
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%#Eval("mssg") %>'></asp:Label>
                                            <asp:Label ID="Label4" runat="server" Text='<%#"@"+Eval("UserName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:DataList>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                        <br />
                        <br />
                    </ItemTemplate>
                </asp:DataList>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style14"></td>
            <td class="auto-style11"></td>
            <td class="auto-style9"></td>
        </tr>
    </table>
            
       
</asp:Content>


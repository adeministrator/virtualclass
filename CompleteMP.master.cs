﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using virtualclass.modles;

public partial class CompleteMP : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        Session[Constants.UserName] = null;
        Response.Redirect("/login.aspx", true);
    }
    protected void LinkButton2_Click(object sender, EventArgs e)
    {
        if ( Session[Constants.UserName].GetType() == typeof(Professor))
        {
            Response.Redirect("/EditPersonalInfoProf.aspx",true);


        }
        else
        {
            Response.Redirect("/editPersonalInfoStu.aspx", true);
        }
    }
    protected void LinkButton3_Click(object sender, EventArgs e)
    {
        if (Session[Constants.UserName].GetType() == typeof(Professor))
        {
            Response.Redirect("/profPersonalInfo.aspx", true);


        }
        else
        {
            Response.Redirect("/PersonalInfoStu.aspx", true);
        }

    }
    protected void LinkButton4_Click(object sender, EventArgs e)
    {
        if (Session[Constants.UserName].GetType() == typeof(Professor))
        {
            Response.Redirect("/CourseInfo.aspx", true);


        }
        else
        {
            Response.Redirect("/CourseInfo.aspx", true);
        }

    }
    protected void LinkButton5_Click(object sender, EventArgs e)
    {
        if (Session[Constants.UserName].GetType() == typeof(Professor))
        {
            Response.Redirect("/HProf.aspx", true);


        }
        else
        {
            Response.Redirect("/HStudent.aspx", true);
        }
    }
}

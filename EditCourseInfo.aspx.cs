﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using virtualclass.modles;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session[Constants.UserName] == null || Session[Constants.UserName].GetType() != typeof(Professor))
        {
            Response.Redirect("/login.aspx");
        }
    }

    
    protected void Button1_Click(object sender, EventArgs e)
    {
        Professor prof = Session[Constants.UserName] as Professor;
       List<Course> co = Course.GetProfCourses(prof.profId);
       int x=0;
       int coursid = co[x].coursid;
        string name=co[x].courseName;
        string code=co[x].code;
        string term = co[x].term;
        string desc = co[x].couseDiscription;
        string tname="";
        if (!String.IsNullOrWhiteSpace(txtCourseName.Text))
            name = txtCourseName.Text;
        if (!String.IsNullOrWhiteSpace(txtTerm.Text))
            term = txtTerm.Text;
        if (!String.IsNullOrWhiteSpace(txtCourseCode.Text))
            code= txtCourseCode.Text;
        if (!String.IsNullOrWhiteSpace(txtDescription.Text))
            desc = txtDescription.Text;
        if (!String.IsNullOrWhiteSpace(txtTAid.Text))
            tname = txtTAid.Text;
        co[x].UpdateCoursesInfo(coursid, code, name, term, desc, tname, true);
        msge.Text = "update is sucessfully done";
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        Professor prof = Session[Constants.UserName] as Professor;
        List<Course> co = Course.GetProfCourses(prof.profId);
        int x = 0;
        int coursid = co[x].coursid;
        string name = co[x].courseName;
        string code = co[x].code;
        string term = co[x].term;
        string desc = co[x].couseDiscription;
        string tname = "";
        if (!String.IsNullOrWhiteSpace(txtCourseName.Text))
            name = txtCourseName.Text;
        if (!String.IsNullOrWhiteSpace(txtTerm.Text))
            term = txtTerm.Text;
        if (!String.IsNullOrWhiteSpace(txtCourseCode.Text))
            code = txtCourseCode.Text;
        if (!String.IsNullOrWhiteSpace(txtDescription.Text))
            desc = txtDescription.Text;
        if (!String.IsNullOrWhiteSpace(txtTAid.Text))
            tname = txtTAid.Text;

        co[x].UpdateCoursesInfo(coursid, code, name, term, desc, tname, false);
        msge.Text = "delete is sucessfully done";
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MiddleMP.master" AutoEventWireup="true" CodeFile="AddCourseProf.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .auto-style3 {
            height: 20px;
        }
        .auto-style4 {
            width: 294px;
        }
        .auto-style5 {
            height: 20px;
            width: 294px;
        }
        .auto-style6 {
            width: 191px;
        }
        .auto-style7 {
            height: 20px;
            width: 191px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="menu2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="middleSide" Runat="Server">
    <table style="width:100%;">
        <tr>
            <td class="auto-style4">&nbsp;</td>
            <td class="auto-style6">
                <asp:Label ID="Label1" runat="server" Text="Course Name"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtCourseName" runat="server"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style5">&nbsp;</td>
            <td class="auto-style7">
                <asp:Label ID="Label2" runat="server" Text="Course Code"></asp:Label>
            </td>
            <td class="auto-style3">
                <asp:TextBox ID="txtCourseCode" runat="server"></asp:TextBox>
            </td>
            <td class="auto-style3"></td>
        </tr>
        <tr>
            <td class="auto-style4">&nbsp;</td>
            <td class="auto-style6">
                <asp:Label ID="Label3" runat="server" Text="Term"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtTerm" runat="server"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style4">&nbsp;</td>
            <td class="auto-style6">
                <asp:Label ID="Label6" runat="server" Text="Department"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="dlstDepartment" runat="server">
                </asp:DropDownList>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style4">&nbsp;</td>
            <td class="auto-style6">
                <asp:Label ID="Label4" runat="server" Text="Description"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtDescription" runat="server"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style5"></td>
            <td class="auto-style7">
                <asp:Label ID="Label5" runat="server" Text="TA User Name"></asp:Label>
            </td>
            <td class="auto-style3">
                <asp:TextBox ID="txtTAUserName" runat="server"></asp:TextBox>
            </td>
            <td class="auto-style3"></td>
        </tr>
        <tr>
            <td class="auto-style4">&nbsp;</td>
            <td class="auto-style6">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style4">&nbsp;</td>
            <td class="auto-style6">&nbsp;</td>
            <td>
                <asp:Button ID="btnAddCourse" runat="server" Text="Add Course" OnClick="btnAddCourse_Click1" />
            </td>
            <td>
                <asp:Label ID="msge" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Content>


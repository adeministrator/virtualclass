﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="testcours.aspx.cs" Inherits="testcours" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            height: 17px;
        }
        .auto-style2 {
            height: 23px;
        }
        .auto-style3 {
            height: 17px;
            width: 24px;
        }
        .auto-style4 {
            height: 23px;
            width: 24px;
        }
        .auto-style5 {
            width: 24px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    </div>
        <asp:DataList ID="DataList1" runat="server">
            <ItemTemplate>
                <table style="width:100%;">
                    <tr>
                        <td class="auto-style1">
                            <asp:Label ID="id" runat="server" Text='<%#Eval("courseID") %>'></asp:Label>
                        </td>
                        <td class="auto-style1">
                            <asp:Label ID="name" runat="server" Text='<%#Eval("courseName") %>'></asp:Label>
                        </td>
                        <td class="auto-style1">
                            <asp:Label ID="term" runat="server" Text='<%#Eval("term") %>'></asp:Label>
                        </td>
                        <td class="auto-style1">
                            <asp:Label ID="disc" runat="server" Text='<%#Eval("courseDescription") %>'></asp:Label>
                        </td>
                        <td class="auto-style1">
                            <asp:Label ID="code" runat="server" Text='<%#Eval("code") %>'></asp:Label>
                        </td>
                        <td class="auto-style1">
                            <asp:Label ID="tid" runat="server" Text='<%#Eval("tID") %>'></asp:Label>
                        </td>
                        <td class="auto-style3">
                            <asp:Label ID="profid" runat="server" Text='<%#Eval("profID") %>'></asp:Label>
                        </td>
                        <td class="auto-style1">
                            <asp:Label ID="isactive" runat="server" Text='<%#Eval("isActive") %>'></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style2"></td>
                        <td class="auto-style2"></td>
                        <td class="auto-style2"></td>
                        <td class="auto-style2"></td>
                        <td class="auto-style2"></td>
                        <td class="auto-style2"></td>
                        <td class="auto-style4">&nbsp;</td>
                        <td class="auto-style2"></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td class="auto-style5">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </ItemTemplate>
        </asp:DataList>
    </form>
</body>
</html>

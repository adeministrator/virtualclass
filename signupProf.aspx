﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MiddleMP.master" AutoEventWireup="true" CodeFile="signupProf.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
    .auto-style1 {
        width: 100%;
        height: 163px;
    }
    .auto-style2 {
        width: 214px;
    }
    .auto-style3 {
        width: 146px;
        height: 20px;
    }
    .auto-style4 {
        height: 20px;
    }
    .auto-style7 {
            width: 294px;
        }
    .auto-style8 {
            width: 294px;
            height: 20px;
        }
    .auto-style9 {
            width: 294px;
            height: 26px;
        }
    .auto-style10 {
        width: 214px;
        height: 26px;
    }
    .auto-style12 {
        height: 26px;
    }
        .auto-style13 {
            width: 146px;
            height: 26px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="menu2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="middleSide" Runat="Server">
    <table class="auto-style1">
    <tr>
        <td class="auto-style7">&nbsp;</td>
        <td class="auto-style13">
            <asp:Label ID="Label1" runat="server" Text="Name"></asp:Label>
        </td>
        <td class="auto-style10">
            <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtName" ErrorMessage="plz enter name"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td class="auto-style7">&nbsp;</td>
        <td class="auto-style13">
            <asp:Label ID="Label2" runat="server" Text="Surname"></asp:Label>
        </td>
        <td class="auto-style10">
            <asp:TextBox ID="txtSurname" runat="server"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtSurname" ErrorMessage="plz enter surname"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td class="auto-style7">&nbsp;</td>
        <td class="auto-style13">
            <asp:Label ID="Label3" runat="server" Text="User Name"></asp:Label>
        </td>
        <td class="auto-style10">
            <asp:TextBox ID="txtUserName" runat="server"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtUserName" ErrorMessage="plz enter username"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td class="auto-style9"></td>
        <td class="auto-style13">
            <asp:Label ID="Label4" runat="server" Text="Password"></asp:Label>
        </td>
        <td class="auto-style110">
            <asp:TextBox ID="txtPassword" runat="server" TextMode="Password"></asp:TextBox>
        </td>
        <td class="auto-style12">
            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtPassword" ErrorMessage="plz enter password"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td class="auto-style7">&nbsp;</td>
        <td class="auto-style13">
            <asp:Label ID="Label5" runat="server" Text="Confirm Password"></asp:Label>
        </td>
        <td class="auto-style10">
            <asp:TextBox ID="Textconfirmpassword" runat="server" TextMode="Password"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="Textconfirmpassword" ErrorMessage="plz enter this filed"></asp:RequiredFieldValidator>
            <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtPassword" ControlToValidate="Textconfirmpassword" ErrorMessage="password dosnt match"></asp:CompareValidator>
        </td>
    </tr>
    <tr>
        <td class="auto-style9"></td>
        <td class="auto-style13">
            <asp:Label ID="Label6" runat="server" Text="E-mail Address"></asp:Label>
        </td>
        <td class="auto-style10">
            <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
        </td>
        <td class="auto-style10">
            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtEmail" ErrorMessage="plz enter email"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmail" ErrorMessage="plz enter a correct email form" ValidationExpression="^((?&gt;[a-zA-Z\d!#$%&amp;'*+\-/=?^_`{|}~]+\x20*|&quot;((?=[\x01-\x7f])[^&quot;\\]|\\[\x01-\x7f])*&quot;\x20*)*(?&lt;angle&gt;&lt;))?((?!\.)(?&gt;\.?[a-zA-Z\d!#$%&amp;'*+\-/=?^_`{|}~]+)+|&quot;((?=[\x01-\x7f])[^&quot;\\]|\\[\x01-\x7f])*&quot;)@(((?!-)[a-zA-Z\d\-]+(?&lt;!-)\.)+[a-zA-Z]{2,}|\[(((?(?&lt;!\[)\.)(25[0-5]|2[0-4]\d|[01]?\d?\d)){4}|[a-zA-Z\d\-]*[a-zA-Z\d]:((?=[\x01-\x7f])[^\\\[\]]|\\[\x01-\x7f])+)\])(?(angle)&gt;)$"></asp:RegularExpressionValidator>
        </td>
    </tr>
    <tr>
        <td class="auto-style7">&nbsp;</td>
        <td class="auto-style13">
            <asp:Label ID="Label7" runat="server" Text="Phone Number"></asp:Label>
        </td>
        <td class="auto-style10">
            <asp:TextBox ID="txtPhone" runat="server"></asp:TextBox>
        </td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td class="auto-style9"></td>
        <td class="auto-style13">
            <asp:Label ID="Label8" runat="server" Text="Title"></asp:Label>
        </td>
        <td class="auto-style10">
            <asp:TextBox ID="tit" runat="server"></asp:TextBox>
        </td>
        <td class="auto-style12">
            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="tit" ErrorMessage="plz enter title"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td class="auto-style8">&nbsp;</td>
        <td class="auto-style13">
            <asp:Label ID="Label11" runat="server" Text="working hours"></asp:Label>
        </td>
        <td class="auto-style10">
            <asp:TextBox ID="hou" runat="server"></asp:TextBox>
        </td>
        <td class="auto-style4">
            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="hou" ErrorMessage="plz enter working hours"></asp:RequiredFieldValidator>
            </td>
    </tr>
    <tr>
        <td class="auto-style7">&nbsp;</td>
        <td class="auto-style13">
            <asp:Label ID="Label10" runat="server" Text="Department"></asp:Label>
            g </td>
        <td class="auto-style10">
            <asp:DropDownList ID="lstbDepartment" runat="server">
            </asp:DropDownList>
        </td>
        <td>
            &nbsp;</td>
    </tr>
    <tr>
        <td class="auto-style8">&nbsp;</td>
        <td class="auto-style3">&nbsp;</td>
        <td class="auto-style10">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Button ID="btnSignUpProf" runat="server" Text="Sign Up" OnClick="btnSignUpProf_Click" style="height: 26px" />
        </td>
        <td class="auto-style4"></td>
    </tr>
</table>
</asp:Content>


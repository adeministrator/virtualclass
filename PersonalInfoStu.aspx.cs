﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using virtualclass.modles;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session[Constants.UserName] == null || Session[Constants.UserName].GetType() != typeof(Student))
        {
            Response.Redirect("/login.aspx");
        }
        if (!IsPostBack)
        {
            bindata();
        }

    }
    public void bindata()
    {
        Student student = Session[Constants.UserName] as Student;
        List<Department> dep = student.getDepartments();
        name.Text = student.name;
        suname.Text = student.surname;
        username.Text = student.userName;
        phone.Text = student.phone;
        email.Text = student.email;
        string x = student.graduatingDate.ToString();
        if (x != "1/1/0001 12:00:00 AM")
            date.Text = x;
        else
            date.Text = " ";
        for (int i = 0; i < dep.Count; i++)
        {
            departmment.Text += dep[i].departmentName;
            if (dep.Count - 1 > i)
                departmment.Text += ",";
        }
            pi.Text = student.personalInfo;
            Image1.ImageUrl = student.picture;
        

    }
    protected void btnUpdateInformation_Click(object sender, EventArgs e)
    {
        Response.Redirect("/editPersonalInfoStu.aspx");
    }
}
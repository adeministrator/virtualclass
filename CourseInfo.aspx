﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LeftAndMiddle.master" AutoEventWireup="true" CodeFile="CourseInfo.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
    .auto-style2 {
        width: 100%;
        height: 170px;
    }
    .auto-style3 {
        width: 32px;
    }
    .auto-style4 {
        width: 145px;
    }

        .auto-style5 {
            height: 20px;
        }

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="menu2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="leftSide" Runat="Server">
    <table class="auto-style2">
    <tr>
        <td class="auto-style3">&nbsp;</td>
        <td class="auto-style4">Department List</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td class="auto-style3">&nbsp;</td>
        <td class="auto-style4">
            <asp:DropDownList ID="DropDownList1" runat="server">
            </asp:DropDownList>
        </td>
        <td>
            <asp:Button ID="filter" runat="server" OnClick="filter_Click" Text="filter" />
        </td>
    </tr>
    <tr>
        <td class="auto-style3">&nbsp;</td>
        <td class="auto-style4">
            &nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="middleSide" Runat="Server">
    
    <div id="content" style=" width:800px;height:600px;background-color:white;overflow :scroll; margin:10px;border:1px solid;border-color:gray;">
    <asp:DataList ID="DataList1" runat="server">
        <ItemTemplate>
            <table style="width:100%;">
                <tr>
                    <td class="auto-style5">
                        <asp:Label ID="Label1" runat="server" Text="course name:"></asp:Label>
                    </td>
                    <td class="auto-style5">
                        <asp:Label ID="coursname" runat="server" Text='<%#Eval("courseName") %>'></asp:Label>
                    </td>
                    <td class="auto-style5"></td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label3" runat="server" Text="course code:"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="courscode" runat="server" Text='<%#Eval("code") %>'></asp:Label>
                    </td>
                    <td>
                        <asp:LinkButton CommandArgument='<%#Eval("code") %>' ID="AddCourse_" runat="server" OnClick="LinkButton1_Click" Text='<%#"add to CoursList" %>'></asp:LinkButton>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label2" runat="server" Text="term:"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="term" runat="server" Text='<%#Eval("term") %>'></asp:Label>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label4" runat="server" Text="course description: "></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="coursdesc" runat="server" Text='<%#Eval("courseDescription") %>'></asp:Label>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label5" runat="server" Text="professor name:"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="profname" runat="server" Text='<%#virtualclass.modles.Professor.getProfessor(Int16.Parse(Eval("profID").ToString())).name +" "+virtualclass.modles.Professor.getProfessor(Int16.Parse(Eval("profID").ToString())).surname %>'></asp:Label>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style5">
                        <asp:Label ID="Label6" runat="server" Text="TA name:"></asp:Label>
                    </td>
                    <td class="auto-style5">
                        <asp:Label ID="tname" runat="server" Text="Label"></asp:Label>
                    </td>
                    <td class="auto-style5"></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </ItemTemplate>
    </asp:DataList>
        </div>
</asp:Content>



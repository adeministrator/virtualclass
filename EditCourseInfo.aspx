﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MiddleMP.master" AutoEventWireup="true" CodeFile="EditCourseInfo.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .auto-style3 {
            width: 255px;
        }
        .auto-style4 {
            width: 338px;
        }
        .auto-style5 {
            width: 269px;
        }
        .auto-style6 {
            width: 338px;
            height: 30px;
        }
        .auto-style7 {
            width: 255px;
            height: 30px;
        }
        .auto-style8 {
            width: 269px;
            height: 30px;
        }
        .auto-style9 {
            height: 30px;
        }
        .auto-style10 {
            width: 338px;
            height: 26px;
        }
        .auto-style11 {
            width: 255px;
            height: 26px;
        }
        .auto-style12 {
            width: 269px;
            height: 26px;
        }
        .auto-style13 {
            height: 26px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="menu2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="middleSide" Runat="Server">
    <table style="width: 100%;">
        <tr>
            <td class="auto-style10"></td>
            <td class="auto-style11">
                <asp:Label ID="Label1" runat="server" Text="Course Name"></asp:Label>
            </td>
            <td class="auto-style12">
                <asp:TextBox ID="txtCourseName" runat="server"></asp:TextBox>
            </td>
            <td class="auto-style13"></td>
        </tr>
        <tr>
            <td class="auto-style10"></td>
            <td class="auto-style11">
                <asp:Label ID="Label2" runat="server" Text="Course Code"></asp:Label>
            </td>
            <td class="auto-style12">
                <asp:TextBox ID="txtCourseCode" runat="server"></asp:TextBox>
            </td>
            <td class="auto-style13"></td>
        </tr>
        <tr>
            <td class="auto-style4">&nbsp;</td>
            <td class="auto-style3">
                <asp:Label ID="Label3" runat="server" Text="Term"></asp:Label>
            </td>
            <td class="auto-style5">
                <asp:TextBox ID="txtTerm" runat="server"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style10"></td>
            <td class="auto-style11">
                <asp:Label ID="Label5" runat="server" Text="Description"></asp:Label>
            </td>
            <td class="auto-style12">
                <asp:TextBox ID="txtDescription" runat="server"></asp:TextBox>
            </td>
            <td class="auto-style13"></td>
        </tr>
        <tr>
            <td class="auto-style4">&nbsp;</td>
            <td class="auto-style3">TA User Name</td>
            <td class="auto-style5">
                <asp:TextBox ID="txtTAid" runat="server"></asp:TextBox>
            </td>
            <td>
                <asp:Label ID="msge" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style6"></td>
            <td class="auto-style7">&nbsp;</td>
            <td class="auto-style8">
                <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Update Course Information" />
            </td>
            <td class="auto-style9">
                <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="Delete Course Information" />
            </td>
        </tr>
    </table>
</asp:Content>


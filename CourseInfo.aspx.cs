﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using virtualclass.modles;
using System.Data;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session[Constants.UserName] == null ||Session[Constants.UserName].GetType()!=typeof(Student))
        {
            Response.Redirect("/login.aspx");
        }

        if (!IsPostBack)
        {
            BindDatalist();
            List<Department> depart = Department.getDepartments();
            foreach (Department s in depart){
                DropDownList1.Items.Add(s.departmentName);
            }
        }
     
    }
    protected void lbtnRegisterCourse_Click(object sender, EventArgs e)
    {

    }
    private void BindDatalist()
    {
        DataTable dtcourse = Course.GetCoursesTa();
        ViewState["dtcourse"] = dtcourse;
        DataList1.DataSource = dtcourse;
        DataList1.DataBind();
    }
    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        Course co = new Course();
        
        string d = (sender as LinkButton).CommandArgument;
 
        Student s=Session[Constants.UserName] as Student;
        co.AddstudentTOCourses(s.userName, d);
        Response.Redirect("/HStudent.aspx", true);
        
    }
   
   
    protected void filter_Click(object sender, EventArgs e)
    {
        string depname = DropDownList1.SelectedItem.ToString();
          DataTable dtcourse = Course.GetDepCoursesTa(depname);
         ViewState["dtcourse"] = dtcourse;
              DataList1.DataSource = dtcourse;
           DataList1.DataBind();
    }
}
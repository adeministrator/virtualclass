﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MiddleMP.master" AutoEventWireup="true" CodeFile="editPersonalInfoStu.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .auto-style3 {
            width: 397px;
        }
        .auto-style4 {
            width: 196px;
        }
        .auto-style5 {
            width: 264px;
        }
        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="menu2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="middleSide" Runat="Server">
    <table style="width:100%;">
        <tr>
            <td class="auto-style3">&nbsp;</td>
            <td class="auto-style4">
                <asp:Label ID="Label1" runat="server" Text="Name"></asp:Label>
            </td>
            <td class="auto-style5">
                <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style3">&nbsp;</td>
            <td class="auto-style4">Surname</td>
            <td class="auto-style5">
                <asp:TextBox ID="txtSurname" runat="server"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style3">&nbsp;</td>
            <td class="auto-style4">
                <asp:Label ID="Label4" runat="server" Text="New Password"></asp:Label>
            </td>
            <td class="auto-style5">
                <asp:TextBox ID="txtPassword" runat="server"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style3">&nbsp;</td>
            <td class="auto-style4">
                <asp:Label ID="Label5" runat="server" Text="Confirm New Password"></asp:Label>
            </td>
            <td class="auto-style5">
                <asp:TextBox ID="txtConfirmPassword" runat="server"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style3">&nbsp;</td>
            <td class="auto-style4">
                <asp:Label ID="Label6" runat="server" Text="Email Address"></asp:Label>
            </td>
            <td class="auto-style5">
                <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style3">&nbsp;</td>
            <td class="auto-style4">
                <asp:Label ID="Label7" runat="server" Text="Phone Number"></asp:Label>
            </td>
            <td class="auto-style5">
                <asp:TextBox ID="txtPhoneNumber" runat="server"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style3">&nbsp;</td>
            <td class="auto-style4">
                <asp:Label ID="Label10" runat="server" Text="Personal information"></asp:Label>
            </td>
            <td class="auto-style5">
                <asp:TextBox ID="txtpersonalinfo" runat="server"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style3">&nbsp;</td>
            <td class="auto-style4">
                <asp:Label ID="Label8" runat="server" Text="Graduation Date"></asp:Label>
            </td>
            <td class="auto-style5">
                <asp:Calendar ID="clGraduationDate" runat="server" OnSelectionChanged="clGraduationDate_SelectionChanged"></asp:Calendar>
            </td>
            <td>
                <asp:FileUpload ID="FileUpload2" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="auto-style3">&nbsp;</td>
            <td class="auto-style4">
                <asp:Label ID="Label9" runat="server" Text="Department"></asp:Label>
            </td>
            <td class="auto-style5">
                <asp:DropDownList ID="lstbDepartment" runat="server">
                </asp:DropDownList>
            </td>
            <td>
                <asp:Button ID="btnUploadPicture" runat="server" OnClick="btnUploadPicture_Click" Text="Upload Picture" />
                <asp:Label ID="mssge0" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style3">&nbsp;</td>
            <td class="auto-style4">&nbsp;</td>
            <td class="auto-style5">
                <asp:Button ID="btnUpdateInformation" runat="server" Text="Update Information" OnClick="btnUpdateInformation_Click" />
            </td>
            <td>
                <asp:Label ID="lblmssg" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Content>


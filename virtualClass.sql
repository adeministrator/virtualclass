<<<<<<< HEAD:virtualClass.sql
CREATE DATABASE VirtualClass
go
GO

USE [master]
GO

CREATE LOGIN [Admin] WITH PASSWORD='1234', DEFAULT_DATABASE=[VirtualClass], DEFAULT_LANGUAGE=[us_english], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF
GO
GRANT CREATE ANY DATABASE TO [Admin]
ALTER LOGIN [Admin] DISABLE
GO

ALTER SERVER ROLE [sysadmin] ADD MEMBER [Admin]
GO

ALTER SERVER ROLE [securityadmin] ADD MEMBER [Admin]
GO

ALTER SERVER ROLE [serveradmin] ADD MEMBER [Admin]
GO

ALTER SERVER ROLE [setupadmin] ADD MEMBER [Admin]
GO

ALTER SERVER ROLE [processadmin] ADD MEMBER [Admin]
GO

ALTER SERVER ROLE [diskadmin] ADD MEMBER [Admin]
GO

ALTER SERVER ROLE [dbcreator] ADD MEMBER [Admin]
GO

ALTER SERVER ROLE [bulkadmin] ADD MEMBER [Admin]
GO


USE [VirtualClass]
GO

CREATE USER [Ad] FOR LOGIN [Admin] WITH DEFAULT_SCHEMA=[content]
GO
GO
USE VirtualClass
GO
CREATE SCHEMA academic
GO
CREATE SCHEMA content
Go
CREATE TABLE academic.Departments(
departmentID int NOT NULL PRIMARY KEY IDENTITY(1,1),
departmentName nvarchar(20) UNIQUE NOT NULL,
departmentDescription nvarchar(300)
--dont forget to make departmentName unique
)
GO
CREATE TABLE academic.Users(
userID int PRIMARY KEY IDENTITY(1,1),
pass nvarchar(200) NOT NULL,
userName nvarchar(30) UNIQUE  NOT NULL,
userType nvarchar(10) NOT NULL,
name nvarchar (30)Not NULL,
surname nvarchar(30) NOT NULL,
picture nvarchar(100),
email nvarchar(50) NOT NULL,
phone nvarchar(20),
personalInfo nvarchar(300),
isActive bit NOT NULL
)
GO
CREATE TABLE academic.Students(
stuID int IDENTITY(1,1) PRIMARY KEY,
garduationDate date ,
userID int FOREIGN KEY REFERENCES academic.Users(userId)  NOT NULL
)
GO
CREATE TABLE academic.TeachingAssistants(
tID int IDENTITY(1,1) PRIMARY KEY,
stuID int FOREIGN KEY REFERENCES academic.Students(stuID)  NOT NULL
)

GO
CREATE TABLE academic.Professors(
profID int IDENTITY(1,1) PRIMARY KEY,
workingHours int NOT NULL,
title nvarchar(20) NOT NULL,
userID int FOREIGN KEY REFERENCES academic.Users(userId)  NOT NULL
)
GO

CREATE TABLE academic.Courses(
courseID int IDENTITY(1,1) PRIMARY KEY,
courseName nvarchar(30) not NULL,
term nvarchar (15) not null,
courseDescription nvarchar(200),
code nvarchar (15) UNIQUE NOT NULL,
tID int FOREIGN KEY REFERENCES academic.TeachingAssistants(tID) ON DELETE SET NULL,
profID int FOREIGN KEY REFERENCES academic.Professors(profID) NOT NULL,
isActive bit NOT NULL
)
GO
CREATE TABLE content.Resources(
resourceID int IDENTITY(1,1) PRIMARY KEY,
courseID int FOREIGN KEY REFERENCES academic.Courses(courseID) NOT NULL,
profId int FOREIGN KEY REFERENCES academic.Professors(profID) NOT NULL,
[Path] nvarchar(200) UNIQUE NOT NULL,
isDeleted bit NOT NULL
)
GO
CREATE TABLE content.Lectures(
lectureID int PRIMARY KEY IDENTITY(1,1),
title nvarchar(100) NOT NULL,
resourceID int FOREIGN KEY REFERENCES content.Resources(resourceId) NOT NULL,
number int NOT NULL,
)
GO

CREATE TABLE academic.Belong_To(
userID int FOREIGN KEY REFERENCES academic.Users(userID) NOT NULL ,
departmentID int FOREIGN KEY REFERENCES academic.Departments(departmentID) NOT NULL,
CONSTRAINT PK_UserID_DepartmentID PRIMARY KEY (userId,departmentID)
)
GO
CREATE TABLE academic.Have(
courseID int FOREIGN KEY REFERENCES academic.Courses(courseID) NOT NULL,
departmentID int FOrEIGN KEY REFERENCES academic.Departments(departmentID) NOT NULL,
PRIMARY KEY (courseID,departmentID)
)
GO
CREATE TABLE academic.[Take](
courseID int FOREIGN KEY REFERENCES academic.Courses(courseID) NOT NULL,
stuID int FOREIGN KEY REFERENCES academic.Students(stuID) NOT NULL,
PRIMARY KEY(courseID,stuID)
)
GO
CREATE TABLE academic.Prof_Degree(
profID int FOREIGN KEY REFERENCES academic.Professors(profID) NOT NULL,
degree varchar (30) NOT NULL,
[date] date NOT NULL,
major varchar(30) NOT NULL,
PRIMARY KEY(profID,degree,[date],major)
)
GO
CREATE TABLE content.Questions(
questionID INT PRIMARY KEY IDENTITY(1,1),
mssg nvarchar(400) NOT NULL,
userID int FOREIGN KEY REFERENCES academic.Users(userID) NOT NULL,
isResolved BIT NOT NULL,
ressourceID int FOREIGN KEY REFERENCES content.RESOURCES(resourceId),
isDeleted BIT NOT NULL,
)
Go
CREATE TABLE content.Comments(
commentID INT PRIMARY KEY IDENTITY(1,1),
mssg nvarchar(4000) NOT NULL,
userID int FOREIGN KEY REFERENCES academic.Users(userID) NOT NULL,
questionID int FOREIGN KEY REFERENCES content.Questions(questionID)  NOT NULL ,--on delete caused problem here
isDeleted BIT NOT NULL
)
GO

CREATE TABLE academic.Announcements(
profID int FOREIGN KEY REFERENCES academic.Professors(profId) NOT NULL,
courseID int FOREIGN KEY REFERENCES academic.Courses(courseID) NOT NULL,
[date] datetime NOT NULL,
[message] varchar (4000) NOT NULL,
isDeleted BIT NOT NULL
PRIMARY KEY (profID,CourseID,[date])
)
GO

CREATE PROCEDURE academic.DoesUserExistAndHisAuthenticationInfoISCorrect(@UserName nvarchar(30),@PassWord nvarchar(200),@Result bit OUTPUT)  
AS
BEGIN
IF EXISTS (SELECT userID FROM academic.Users  where userName=@UserName AND pass=@PassWord )
	BEGIN
		SET @Result=1
	END
ELSE	
	BEGIN
		SET @Result=0
	END
END

GO

CREATE PROCEDURE academic.TypeOFUser(@UserName varchar(30),@Type nvarchar(10)OUT)
AS
BEGIN
IF NOT EXISTS (SELECT userType from academic.Users where userName=@UserName)
	SET @Type='UNKNOWN';
ELSE
	SET @Type=(select userType from academic.Users where userName=@UserName)

END

GO

CREATE PROCEDURE academic.UpdateUserInfo(@UserName nvarchar(30),@Password nvarchar(200),@Picture nvarchar(100),@Email nvarchar(50),@Phone nvarchar(20),@PersonalInfo nvarchar (30),@Name nvarchar(50),@Surname nvarchar(50))
As
BEGIN
IF EXISTS (select userName from academic.Users where userName=@UserName)
BEGIN
	IF @Password IS NOT NULL AND LEN(@Password) >0
		BEGIN
			UPDATE academic.Users SET pass=@Password WHERE userName=@UserName
		END
	IF @Email IS NOT NULL AND LEN(@Email) >0
		UPDATE academic.Users SET email=@Email where userName=@UserName
	IF @Picture IS NOT NULL AND LEN(@Picture) >0
		UPDATE academic.Users SET picture=@Picture where userName=@UserName
	END
	
	UPDATE academic.Users SET personalInfo=@PersonalInfo ,phone=@Phone ,name=@Name,surname=@Surname
		where userName=@UserName
END


Go
CREATE PROCEDURE academic.AddNewStudent(@UserName nvarchar(30),@Password nvarchar(200),@Name nvarchar(30),@Surname nvarchar(30),@Picture nvarchar(100),@Email nvarchar(50),@Phone nvarchar(20),@PersonalInf nvarchar(300),@GradDate date)
AS
BEGIN

DECLARE @UID as INT


	BEGIN TRY
		BEGIN TRANSACTION
		INSERT INTO academic.Users VALUES(@Password,@UserName,'STUDENT',@Name,@Surname,@Picture,@Email,@Phone,@PersonalInf,1)
		SET @UID =SCOPE_IDENTITY()
		INSERT INTO academic.Students VALUES(@GradDate,@UID)
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;
	 SELECT 
        @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();

		raiserror(@ErrorMessage,@ErrorSeverity,@ErrorState)
		rollback transaction
	END CATCH
END

Go

CREATE PROCEDURE academic.AddNewProf(@UserName nvarchar(30),@Password nvarchar(200),
										@Name nvarchar(30),@Surname nvarchar(30),@Picture nvarchar(100)
										,@Email nvarchar(50),@Phone nvarchar(20),@PersonalInf nvarchar(300)
										,@WorkingHours int,@Title nvarchar(20))
AS
BEGIN
DECLARE @DpID as int
DECLARE @UID as INT

	BEGIN TRY
		BEGIN TRANSACTION
		INSERT INTO academic.Users VALUES(@Password,@UserName,'PROF',@Name,@Surname,@Picture,@Email,@Phone,@PersonalInf,1)
		SET @UID =SCOPE_IDENTITY()
		INSERT INTO academic.Professors VALUES(@WorkingHours,@Title,@UID)
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;
	 SELECT 
        @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();

		raiserror(@ErrorMessage,@ErrorSeverity,@ErrorState)
		rollback transaction
	END CATCH
END



Go

 CREATE PROCEDURE academic.AddDegree(@ProfUserName nvarchar (30),@Degree nvarchar(30),@Date date,@Major nvarchar(30))
 AS
 BEGIN
	DECLARE @ProfId int
 
	SET @ProfId=(
				select profId
				from academic.Professors as AP join academic.Users as AU on AP.userID=AU.userID
				 WHERE AU.userName=@ProfUserName
				)
	INSERT INTO academic.Prof_Degree VALUES(@ProfId,@Degree,@Date,@Major)
 END
 

 
 GO
 CREATE PROCEDURE academic.AddNewCourse( @CourseName nvarchar(30),@CourseCode nvarchar(15),@Term nvarchar(30),@CourseDescription nvarchar(200),@TidUserName nvarchar(30),@ProfUserName nvarchar(30))
 AS
 BEGIN
 BEGIN TRY
 BEGIN TRANSACTION
	DECLARE @DpID AS INT,@ProfID AS INT,@Tid AS INT ,@StID AS INT

	 SET @ProfID=(
					select profId
					from academic.Professors as AP join academic.Users as AU on AP.userID=AU.userID
					 WHERE AU.userName=@ProfUserName
		)
		IF @TidUserName IS NOT NULL
			BEGIN
				SET @StID=(Select stuID
				from academic.Students as ST join academic.Users AS AU on AU.userID=ST.userID WHERE AU.userName=@TidUserName
				)
				IF @StID IS NOT NULL
				BEGIN 
					INSERT INTO academic.TeachingAssistants VALUES(@StID)
					SET @Tid=SCOPE_IDENTITY()
				END
			END
		INSERT INTO academic.Courses VALUES(@CourseName,@Term,@CourseDescription,@CourseCode,@Tid,@ProfID,1);
		COMMIT TRANSACTION
		END TRY
		BEGIN CATCH
		
			DECLARE @ErrorMessage NVARCHAR(4000);
			DECLARE @ErrorSeverity INT;
			DECLARE @ErrorState INT;
			 SELECT 
				@ErrorMessage = ERROR_MESSAGE(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE();

				raiserror(@ErrorMessage,@ErrorSeverity,@ErrorState)
				rollback transaction
			
		END CATCH
 END



GO
CREATE PROCEDURE content.AddNewResources(@CourseCode nvarchar(15),@ProfUserName nvarchar(30),@Path nvarchar(200))AS
BEGIN
DECLARE @PRofID int;
DECLARE @CourseID int;
SELECT @PRofID=AP.profID,@CourseID=AC.courseID
from academic.Users as AU join academic.Professors AS AP ON AP.userID=AU.userID
JOIN academic.Courses as AC on AC.profID=Ap.profID
	Where AU.userName=@ProfUserName AND AC.code=@CourseCode;
INSERT INTO content.Resources VALUES (@CourseID,@PRofID,@Path,0)
END


Go
CREATE PROCEDURE content.AddNewLecture(@CourseCode nvarchar(15),@ProfUserName nvarchar(30),@YoutubeLink nvarchar(200),@title nvarchar (100),@Number int)AS
BEGIN
DECLARE @PRofID int;
DECLARE @CourseID int;
DECLARE @RID INT;
BEGIN TRY
BEGIN TRANSACTION
	SELECT @PRofID=AP.profID,@CourseID=AC.courseID
	from academic.Users as AU join academic.Professors AS AP ON AP.userID=AU.userID
	JOIN academic.Courses as AC on AC.profID=Ap.profID
		Where AU.userName=@ProfUserName AND AC.code=@CourseCode;
	INSERT INTO content.Resources VALUES (@CourseID,@PRofID,@YoutubeLink,0)
	SET @RID=SCOPE_IDENTITY()
	INSERT INTO content.Lectures VALUES(@title,@RID,@Number)
	commit transaction
	END TRY
	BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000);
			DECLARE @ErrorSeverity INT;
			DECLARE @ErrorState INT;
			 SELECT 
				@ErrorMessage = ERROR_MESSAGE(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE();
	rollback TRANSACTION
	END CATCH
END

GO

CREATE TRIGGER content.OnDeleteQuestionDeleteComment ON content.Questions instead of DELETE
AS
BEGIN
UPDATE content.Comments 
SET isDeleted=1
Where questionID in(select questionID from deleted)
END
UPDATE content.Questions
SET isDeleted=1
where questionID in (select questionID from deleted)
GO

CREATE TRIGGER academic.OnDeleteCourse ON academic.Courses INSTEAD OF DELETE 
AS
BEGIN
UPDATE academic.Courses SET isActive=0 WHERE courseID in (SELECT courseID from deleted)
END
GO
CREATE TRIGGER academic.OnDeleteAnnouncements ON academic.Announcements INSTEAD  OF DELETE 
AS 
BEGIN
UPDATE academic.Announcements  SET isDeleted=1 WHERE EXISTS(SELECT * FROM deleted join academic.Announcements as AA ON AA.profID=deleted.profID AND AA.courseID=deleted.courseID AND AA.[date]=deleted.[date])
END
GO

CREATE TRIGGER content.OnDeleteComment ON content.Comments instead of DELETE 
AS
BEGIN
UPDATE content.Comments 
SET isDeleted=1
Where commentID in(select commentID from deleted)
END
GO

GO
CREATE PROCEDURE content.AddNewQuestion(@Msg nvarchar(400),@UserName nvarchar(30),@IsResolved bit,@ResourceID int)
AS
BEGIN
DECLARE @USerID int;
SET @USerID=(select userID from academic.Users Where userName=@UserName);
INSERT INTO content.Questions VALUES(@Msg,@USerID,0,@ResourceID,0)
END
GO

CREATE PROCEDURE content.SetQuestionResovled(@QuestionID INT,@IsResolved bit)
AS
UPDATE content.Questions SET isResolved=@IsResolved  WHERE content.Questions.questionID=@QuestionID

GO
CREATE PROCEDURE content.AddNewComment(@Msg nvarchar(4000),@UserName nvarchar(30),@QuestionID int)
AS
BEGIN
DECLARE @USerID int;
SET @USerID=(select userID from academic.Users Where userName=@UserName);
INSERT INTO content.Comments VALUES(@Msg,@USerID,@QuestionID,0)
END

GO
CREATE PROCEDURE academic.AddUserToAClass(@UserName nvarchar(30),@CourseCode nvarchar(30))
AS
BEGIN
DECLARE @StudentID int,@CourseID int;
SET @StudentID=(select AST.stuID from academic.Users as AU join academic.Students as AST on AST.userID=AU.userID
 where userName=@UserName)
SET @CourseID =(SELECT courseID from academic.Courses where code=@CourseCode)
INSERT INTO academic.Take VALUES(@CourseID,@StudentID)
END

GO
  
CREATE PROCEDURE academic.AddNewDepartment(@DepartmeantName nvarchar(20),@DepartmentDiscription nvarchar(300))
AS INSERT INTO academic.Departments VALUES (@DepartmeantName,@DepartmentDiscription)
GO

CREATE PROCEDURE academic.AddStudentToCourse(@UserName nvarchar(30),@CourseCode nvarchar(30))
AS
BEGIN
DECLARE @SUID int,@CouserID int
SET @SUID=(select stuID from academic.Students AS AC join  academic.Users AS AU on  AU.userID=AC.userID where AU.userName=@UserName)
SET @CouserID=(select courseID from academic.Courses where Courses.code=@CourseCode)
INSERT INTO academic.[Take] VALUES(@CouserID,@SUID);
END

GO
CREATE FUNCTION academic.StudentsByCourse (@CourseCode nvarchar (30) )
RETURNS @StudentByClass TABLE(
userID  int ,
stuID  int
)
AS
BEGIN 


INSERT INTO @StudentByClass(userID,stuID)
 SELECT AST.userID,AST.stuID
from academic.Courses AS AC join academic.[Take] as AT on AC.courseID=AT.courseID
join academic.Students AS AST on AST.stuID=AT.stuID 
where AC.code=@CourseCode 
RETURN
END

GO
CREATE PROCEDURE academic.MakeNewAnnouncement(@ProfUName nvarchar(30),@CourseCode nvarchar(30),@Date datetime ,@Mesg nvarchar (4000))
AS
BEGIN
DECLARE @ProfId int,@CourseId int;
SET @ProfId=(SELECT profID from academic.Professors join academic.Users on academic.Users.userID= academic.Professors.userID where academic.Users.userName=@ProfUName);
SET @CourseId=(select courseID from academic.Courses where Courses.code=@CourseCode)
INSERT INTO [academic].[Announcements] VALUES(@ProfId,@CourseId,@Date,@Mesg,0)
END

GO
CREATE VIEW academic.StudentsInfo
AS 
SELECT AU.userName,AU.name,AU.surname,Au.email,AD.departmentName,AST.garduationDate
FROM academic.Users AS AU join academic.Students as AST on AST.userID=AU.userID
join academic.Belong_To AS AB on Ab.userID=AU.userID 
join academic.Departments AS AD on Ab.departmentID=AD.departmentID

GO
CREATE VIEW academic.CoursesInfo 
AS
SELECT Ac.code,Ac.courseName,(select AU.name from academic.Professors  AS AP join academic.Users AS AU on AU.userID=AP.userID  where profID=AC.profID) AS [Prof's Name],
(select AU.surname from academic.Professors  AS AP join academic.Users AS AU on AU.userID=AP.userID  where profID=AC.profID) AS [Prof's Surname],
(SELECT AU.name FROM academic.TeachingAssistants AS AT join academic.Students AS ACC ON ACC.stuID=AT.stuID
JOIN academic.Users AS AU ON AU.userID=ACC.userID WHERE AT.tID=AT.tID) AS [TA's Name],
(SELECT AU.surname FROM academic.TeachingAssistants AS AT join academic.Students AS ACC ON ACC.stuID=AT.stuID
JOIN academic.Users AS AU ON AU.userID=ACC.userID WHERE AT.tID=AT.tID) AS [TA's Surname],
(select count(*) from academic.[Take]  AS AT join academic.Students AS AST ON AST.stuID=AT.stuID
join academic.Users AS AUS ON AUS.userID =AST.userID 
WHERE AUS.isActive=1 AND AT.courseID=AC.courseID) AS [Number of current Students],
AC.courseDescription,Ac.term,Ac.isActive
FROM academic.Courses  AS AC
GO
CREATE VIEW academic.DepartmentsInfo AS
SELECT AD.departmentName,
(SELECT COUNT(*) FROM academic.Belong_To AS BT JOIN academic.Users AU ON AU.userID=BT.userID JOIN academic.Professors AS AP ON AP.userID=AU.userID WHERE BT.departmentID=AD.departmentID) AS [Number of Professors],
(SELECT COUNT (*) FROM academic.Belong_To AS BT JOIN academic.Users AU ON AU.userID=BT.userID JOIN
academic.Students AS AST ON AST.userID=AU.userID where BT.departmentID=AD.departmentID) AS[Number of Students in dep],
(SELECT COUNT (*) FROM academic.Courses AS AC JOIN academic.Have AH ON AH.courseID=AC.courseID WHERE AH.departmentID=AD.departmentID) AS [Number Of Courses]
FROM academic.Departments AS AD

GO
SELECT * from academic.DepartmentsInfo
GO
CREATE VIEW academic.ProfInfo
AS 
SELECT PF.title,AU.name,AU.surname,AU.userName,AU.email,PF.workingHours,AD.departmentName,
(SELECT degree FROM academic.Prof_Degree WHERE profID=PF.profID) AS Degree
,(SELECT COUNT (*) from academic.Courses WHERE academic.Courses.profID=PF.profID) AS [Number Of courses]
FROM academic.Professors AS PF join academic.Users AS AU ON AU.userID=PF.userID 
join academic.Belong_To AS AB on AB.userID=AU.userID 
join academic.Departments AS AD ON AD.departmentID=AB.departmentID;
GO
CREATE VIEW academic.ProfHasNoDepartment
AS 
 SELECT PF.title,AU.name,AU.surname,AU.userName,AU.email,PF.workingHours,
(SELECT degree FROM academic.Prof_Degree WHERE profID=PF.profID) AS Degree
,(SELECT COUNT (*) from academic.Courses WHERE academic.Courses.profID=PF.profID) AS [Number Of courses]
FROM academic.Professors AS PF join academic.Users AS AU ON AU.userID=PF.userID WHERE AU.userID NOT IN(select userID from academic.Belong_To )
GO
select * from academic.ProfHasNoDepartment

SELECT * from academic.ProfInfo
GO
CREATE PROCEDURE academic.AddUserToDepartment(@UserName nvarchar(30),@DepartementName nvarchar(20))
AS
BEGIN
DECLARE @UserId int,@DepId int
SELECT @UserId=AU.userID FROM academic.Users AS AU WHERE AU.userName=@UserName
SELECT @DepId=AD.departmentID FROM academic.Departments AS AD where AD.departmentName=@DepartementName 

INSERT INTO academic.Belong_To VALUES(@UserId,@DepId)
END
GO


CREATE PROCEDURE academic.AddCourseToDepartment(@CourseCode nvarchar(15),@DepartmentName nvarchar(20))
AS
BEGIN 
DECLARE @CourseId int,@DepId int
SELECT @CourseId=courseID FROM academic.Courses WHERE code=@CourseCode
SELECT @DepId=AD.departmentID FROM academic.Departments AS AD where AD.departmentName=@DepartmentName
INSERT INTO academic.Have VALUES(@CourseId,@DepId)
END
GO

CREATE PROCEDURE academic.CoursesByDepartment(@DepartmentName nvarchar(20))
AS
BEGIN
SELECT AC.code,AC.courseDescription,Ac.courseID,AC.courseName,AC.isActive,AC.profID,Ac.term,AC.tID FROM academic.Courses AS AC join academic.Have AH ON AH.courseID=AC.courseID JOIN academic.Departments AS AD  ON AD.departmentID=AH.departmentID WHERE @DepartmentName=AD.departmentName 
END
GO
CREATE PROCEDURE academic.StudentByUserName(@UserName nvarchar(30))AS
SELECT * FROM academic.Users as AU JOIN academic.Students AS AST ON AST.userID=AU.userID WHERE AU.userName=@Username
GO
GO
CREATE PROCEDURE academic.DepartmentByName(@DepartmentName nvarchar(30))AS
SELECT * FROM academic.Departments where departmentName=@DepartmentName;
GO
CREATE PROCEDURE academic.GetDepartments
AS
SELECT * from academic.Departments;
GO
CREATE PROCEDURE academic.UpdateStudentInfo(@Stdid int ,@gradDate date)AS
UPDATE academic.Students set garduationDate=@gradDate WHERE stuID=@Stdid;
Go
CREATE PROCEDURE academic.UserByUserName(@UserName varchar(30))AS
SELECT * from academic.Students AS AC join academic.Users AS AU ON AU.userID=AC.userID WHERE AU.userName=@UserName;
GO
CREATE PROCEDURE academic.StudentById(@StdId varchar(30))AS
SELECT * from academic.Students AS AC join academic.Users AS AU ON AU.userID=AC.userID WHERE Ac.stuID=@StdId;
GO
CREATE PROCEDURE academic.ProfessorByUserName(@ProfUserName nvarchar(30))AS
SELECT * from academic.Professors AS AP JOIN academic.Users AS AU ON AU.userID=AP.userID WHERE  AU.userName=@ProfUserName
GO
CREATE PROCEDURE academic.ProfessorByProfID(@ProfID int)AS
SELECT * from academic.Professors AS AP JOIN academic.Users AS AU ON AU.userID=AP.userID WHERE  AP.profID=@ProfID

go

create procedure academic.getstudentcourses(@id as nvarchar(50))
as
select *
from  [academic].[Students]as s inner join [academic].[Take] as t on s.stuID = t.stuID
join [academic].[Courses] as c on c.courseID= t.courseID
where s.stuID=@id 

go
create procedure academic.countstudentcourses(@code as nvarchar(50))
as
select COUNT([stuID]) AS stunum 
from  [academic].[Take] as t inner join [academic].[Courses] as c on t.courseID=c.courseID
where c.[code]=@code
GROUP BY (c.[code])
go
create procedure academic.getdepcourses(@id as nvarchar(50))
as
select *
from [academic].[Courses] as c inner join [academic].[Have] as h on c.courseID = h.courseID
join  [academic].[Departments] as d on d.departmentID= h.departmentID
where d.departmentID=@id
go

create procedure academic.getprofcourses(@id as nvarchar(50))
as
select *
from  [academic].[Professors]as p inner join [academic].[Courses] as c on p.profID = c.profID 
where p.profID=@id
go
create procedure academic.getCourses
as
select *
from [academic].[Courses] 
go
create procedure academic.countcourses
as
select COUNT([courseID]) AS cnum 
from   [academic].[Courses] 
where [isActive]=1
GROUP BY ([isActive])
go
create procedure academic.countresouce
as
select COUNT([resourceID]) AS rnum 
from   [content].[Resources]
where [isDeleted]=0
GROUP BY ([isDeleted])
go
create procedure academic.countlecture(@coursecode as nvarchar(50))
as
select COUNT([lectureID]) AS lecnum 
from   [content].[Resources] as r inner join [content].[Lectures]as l on r.resourceID=l.resourceID
join [academic].[Courses] as c on c.courseID = r.courseID
where c.code=@coursecode
GROUP BY (c.courseID)
=======
CREATE DATABASE VirtualClass
go
GO

USE [master]
GO

CREATE LOGIN [Admin] WITH PASSWORD='1234', DEFAULT_DATABASE=[VirtualClass], DEFAULT_LANGUAGE=[us_english], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF
GO
GRANT CREATE ANY DATABASE TO [Admin]
ALTER LOGIN [Admin] DISABLE
GO

ALTER SERVER ROLE [sysadmin] ADD MEMBER [Admin]
GO

ALTER SERVER ROLE [securityadmin] ADD MEMBER [Admin]
GO

ALTER SERVER ROLE [serveradmin] ADD MEMBER [Admin]
GO

ALTER SERVER ROLE [setupadmin] ADD MEMBER [Admin]
GO

ALTER SERVER ROLE [processadmin] ADD MEMBER [Admin]
GO

ALTER SERVER ROLE [diskadmin] ADD MEMBER [Admin]
GO

ALTER SERVER ROLE [dbcreator] ADD MEMBER [Admin]
GO

ALTER SERVER ROLE [bulkadmin] ADD MEMBER [Admin]
GO


USE [VirtualClass]
GO

CREATE USER [Ad] FOR LOGIN [Admin] WITH DEFAULT_SCHEMA=[content]
GO
GO
USE VirtualClass
GO
CREATE SCHEMA academic
GO
CREATE SCHEMA content
Go
CREATE TABLE academic.Departments(
departmentID int NOT NULL PRIMARY KEY IDENTITY(1,1),
departmentName nvarchar(20) UNIQUE NOT NULL,
departmentDescription nvarchar(300)
--dont forget to make departmentName unique
)
GO
CREATE TABLE academic.Users(
userID int PRIMARY KEY IDENTITY(1,1),
pass nvarchar(200) NOT NULL,
userName nvarchar(30) UNIQUE  NOT NULL,
userType nvarchar(10) NOT NULL,
name nvarchar (30)Not NULL,
surname nvarchar(30) NOT NULL,
picture nvarchar(100),
email nvarchar(50) NOT NULL,
phone nvarchar(20),
personalInfo nvarchar(300),
isActive bit NOT NULL
)
GO
CREATE TABLE academic.Students(
stuID int IDENTITY(1,1) PRIMARY KEY,
garduationDate date ,
userID int FOREIGN KEY REFERENCES academic.Users(userId)  NOT NULL
)
GO
CREATE TABLE academic.TeachingAssistants(
tID int IDENTITY(1,1) PRIMARY KEY,
stuID int FOREIGN KEY REFERENCES academic.Students(stuID)  NOT NULL
)

GO
CREATE TABLE academic.Professors(
profID int IDENTITY(1,1) PRIMARY KEY,
workingHours int NOT NULL,
title nvarchar(20) NOT NULL,
userID int FOREIGN KEY REFERENCES academic.Users(userId)  NOT NULL
)
GO

CREATE TABLE academic.Courses(
courseID int IDENTITY(1,1) PRIMARY KEY,
courseName nvarchar(30) not NULL,
term nvarchar (15) not null,
courseDescription nvarchar(200),
code nvarchar (15) UNIQUE NOT NULL,
tID int FOREIGN KEY REFERENCES academic.TeachingAssistants(tID) ON DELETE SET NULL,
profID int FOREIGN KEY REFERENCES academic.Professors(profID) NOT NULL,
isActive bit NOT NULL
)
GO
CREATE TABLE content.Resources(
resourceID int IDENTITY(1,1) PRIMARY KEY,
courseID int FOREIGN KEY REFERENCES academic.Courses(courseID) NOT NULL,
profId int FOREIGN KEY REFERENCES academic.Professors(profID) NOT NULL,
[Path] nvarchar(200) UNIQUE NOT NULL,
isDeleted bit NOT NULL
)
GO
CREATE TABLE content.Lectures(
lectureID int PRIMARY KEY IDENTITY(1,1),
title nvarchar(100) NOT NULL,
resourceID int FOREIGN KEY REFERENCES content.Resources(resourceId) NOT NULL,
number int NOT NULL,
)
GO

CREATE TABLE academic.Belong_To(
userID int FOREIGN KEY REFERENCES academic.Users(userID) NOT NULL ,
departmentID int FOREIGN KEY REFERENCES academic.Departments(departmentID) NOT NULL,
CONSTRAINT PK_UserID_DepartmentID PRIMARY KEY (userId,departmentID)
)
GO
CREATE TABLE academic.Have(
courseID int FOREIGN KEY REFERENCES academic.Courses(courseID) NOT NULL,
departmentID int FOrEIGN KEY REFERENCES academic.Departments(departmentID) NOT NULL,
PRIMARY KEY (courseID,departmentID)
)
GO
CREATE TABLE academic.[Take](
courseID int FOREIGN KEY REFERENCES academic.Courses(courseID) NOT NULL,
stuID int FOREIGN KEY REFERENCES academic.Students(stuID) NOT NULL,
PRIMARY KEY(courseID,stuID)
)
GO
CREATE TABLE academic.Prof_Degree(
profID int FOREIGN KEY REFERENCES academic.Professors(profID) NOT NULL,
degree varchar (30) NOT NULL,
[date] date NOT NULL,
major varchar(30) NOT NULL,
PRIMARY KEY(profID,degree,[date],major)
)
GO
CREATE TABLE content.Questions(
questionID INT PRIMARY KEY IDENTITY(1,1),
mssg nvarchar(400) NOT NULL,
userID int FOREIGN KEY REFERENCES academic.Users(userID) NOT NULL,
isResolved BIT NOT NULL,
ressourceID int FOREIGN KEY REFERENCES content.RESOURCES(resourceId),
isDeleted BIT NOT NULL,
)
Go
CREATE TABLE content.Comments(
commentID INT PRIMARY KEY IDENTITY(1,1),
mssg nvarchar(4000) NOT NULL,
userID int FOREIGN KEY REFERENCES academic.Users(userID) NOT NULL,
questionID int FOREIGN KEY REFERENCES content.Questions(questionID)  NOT NULL ,--on delete caused problem here
isDeleted BIT NOT NULL
)
GO

CREATE TABLE academic.Announcements(
profID int FOREIGN KEY REFERENCES academic.Professors(profId) NOT NULL,
courseID int FOREIGN KEY REFERENCES academic.Courses(courseID) NOT NULL,
[date] datetime NOT NULL,
[message] varchar (4000) NOT NULL,
isDeleted BIT NOT NULL
PRIMARY KEY (profID,CourseID,[date])
)
GO

CREATE PROCEDURE academic.DoesUserExistAndHisAuthenticationInfoISCorrect(@UserName nvarchar(30),@PassWord nvarchar(200),@Result bit OUTPUT)  
AS
BEGIN
IF EXISTS (SELECT userID FROM academic.Users  where userName=@UserName AND pass=@PassWord )
	BEGIN
		SET @Result=1
	END
ELSE	
	BEGIN
		SET @Result=0
	END
END

GO

CREATE PROCEDURE academic.TypeOFUser(@UserName varchar(30),@Type nvarchar(10)OUT)
AS
BEGIN
IF NOT EXISTS (SELECT userType from academic.Users where userName=@UserName)
	SET @Type='UNKNOWN';
ELSE
	SET @Type=(select userType from academic.Users where userName=@UserName)

END

GO

CREATE PROCEDURE academic.UpdateUserInfo(@UserName nvarchar(30),@Password nvarchar(200),@Picture nvarchar(100),@Email nvarchar(50),@Phone nvarchar(20),@PersonalInfo nvarchar (30))
As
BEGIN
IF EXISTS (select userName from academic.Users where userName=@UserName)
BEGIN
	IF @Password IS NOT NULL AND LEN(@Password) >0
		BEGIN
			UPDATE academic.Users SET pass=@Password WHERE userName=@UserName
		END
	IF @Email IS NOT NULL AND LEN(@Email) >0
		UPDATE academic.Users SET email=@Email where userName=@UserName
	IF @Picture IS NOT NULL AND LEN(@Picture) >0
		UPDATE academic.Users SET picture=@Picture where userName=@UserName
	END
	UPDATE academic.Users SET personalInfo=@PersonalInfo ,phone=@Phone
		where userName=@UserName
END


Go
CREATE PROCEDURE academic.AddNewStudent(@UserName nvarchar(30),@Password nvarchar(200),@Name nvarchar(30),@Surname nvarchar(30),@Picture nvarchar(100),@Email nvarchar(50),@Phone nvarchar(20),@PersonalInf nvarchar(300),@GradDate date)
AS
BEGIN

DECLARE @UID as INT


	BEGIN TRY
		BEGIN TRANSACTION
		INSERT INTO academic.Users VALUES(@Password,@UserName,'STUDENT',@Name,@Surname,@Picture,@Email,@Phone,@PersonalInf,1)
		SET @UID =SCOPE_IDENTITY()
		INSERT INTO academic.Students VALUES(@GradDate,@UID)
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;
	 SELECT 
        @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();

		raiserror(@ErrorMessage,@ErrorSeverity,@ErrorState)
		rollback transaction
	END CATCH
END

Go

CREATE PROCEDURE academic.AddNewProf(@UserName nvarchar(30),@Password nvarchar(200),
										@Name nvarchar(30),@Surname nvarchar(30),@Picture nvarchar(100)
										,@Email nvarchar(50),@Phone nvarchar(20),@PersonalInf nvarchar(300)
										,@WorkingHours int,@Title nvarchar(20))
AS
BEGIN
DECLARE @DpID as int
DECLARE @UID as INT

	BEGIN TRY
		BEGIN TRANSACTION
		INSERT INTO academic.Users VALUES(@Password,@UserName,'PROF',@Name,@Surname,@Picture,@Email,@Phone,@PersonalInf,1)
		SET @UID =SCOPE_IDENTITY()
		INSERT INTO academic.Professors VALUES(@WorkingHours,@Title,@UID)
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;
	 SELECT 
        @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();

		raiserror(@ErrorMessage,@ErrorSeverity,@ErrorState)
		rollback transaction
	END CATCH
END



Go

 CREATE PROCEDURE academic.AddDegree(@ProfUserName nvarchar (30),@Degree nvarchar(30),@Date date,@Major nvarchar(30))
 AS
 BEGIN
	DECLARE @ProfId int
 
	SET @ProfId=(
				select profId
				from academic.Professors as AP join academic.Users as AU on AP.userID=AU.userID
				 WHERE AU.userName=@ProfUserName
				)
	INSERT INTO academic.Prof_Degree VALUES(@ProfId,@Degree,@Date,@Major)
 END
 

 
 GO
 CREATE PROCEDURE academic.AddNewCourse( @CourseName nvarchar(30),@CourseCode nvarchar(15),@Term nvarchar(30),@CourseDescription nvarchar(200),@TidUserName nvarchar(30),@ProfUserName nvarchar(30))
 AS
 BEGIN
 BEGIN TRY
 BEGIN TRANSACTION
	DECLARE @DpID AS INT,@ProfID AS INT,@Tid AS INT ,@StID AS INT

	 SET @ProfID=(
					select profId
					from academic.Professors as AP join academic.Users as AU on AP.userID=AU.userID
					 WHERE AU.userName=@ProfUserName
		)
		IF @TidUserName IS NOT NULL
			BEGIN
				SET @StID=(Select stuID
				from academic.Students as ST join academic.Users AS AU on AU.userID=ST.userID WHERE AU.userName=@TidUserName
				)
				IF @StID IS NOT NULL
				BEGIN 
					INSERT INTO academic.TeachingAssistants VALUES(@StID)
					SET @Tid=SCOPE_IDENTITY()
				END
			END
		INSERT INTO academic.Courses VALUES(@CourseName,@Term,@CourseDescription,@CourseCode,@Tid,@ProfID,1);
		COMMIT TRANSACTION
		END TRY
		BEGIN CATCH
		
			DECLARE @ErrorMessage NVARCHAR(4000);
			DECLARE @ErrorSeverity INT;
			DECLARE @ErrorState INT;
			 SELECT 
				@ErrorMessage = ERROR_MESSAGE(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE();

				raiserror(@ErrorMessage,@ErrorSeverity,@ErrorState)
				rollback transaction
			
		END CATCH
 END



GO
CREATE PROCEDURE content.AddNewResources(@CourseCode nvarchar(15),@ProfUserName nvarchar(30),@Path nvarchar(200))AS
BEGIN
DECLARE @PRofID int;
DECLARE @CourseID int;
SELECT @PRofID=AP.profID,@CourseID=AC.courseID
from academic.Users as AU join academic.Professors AS AP ON AP.userID=AU.userID
JOIN academic.Courses as AC on AC.profID=Ap.profID
	Where AU.userName=@ProfUserName AND AC.code=@CourseCode;
INSERT INTO content.Resources VALUES (@CourseID,@PRofID,@Path,0)
END


Go
CREATE PROCEDURE content.AddNewLecture(@CourseCode nvarchar(15),@ProfUserName nvarchar(30),@YoutubeLink nvarchar(200),@title nvarchar (100),@Number int)AS
BEGIN
DECLARE @PRofID int;
DECLARE @CourseID int;
DECLARE @RID INT;
BEGIN TRY
BEGIN TRANSACTION
	SELECT @PRofID=AP.profID,@CourseID=AC.courseID
	from academic.Users as AU join academic.Professors AS AP ON AP.userID=AU.userID
	JOIN academic.Courses as AC on AC.profID=Ap.profID
		Where AU.userName=@ProfUserName AND AC.code=@CourseCode;
	INSERT INTO content.Resources VALUES (@CourseID,@PRofID,@YoutubeLink,0)
	SET @RID=SCOPE_IDENTITY()
	INSERT INTO content.Lectures VALUES(@title,@RID,@Number)
	commit transaction
	END TRY
	BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000);
			DECLARE @ErrorSeverity INT;
			DECLARE @ErrorState INT;
			 SELECT 
				@ErrorMessage = ERROR_MESSAGE(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE();
	rollback TRANSACTION
	END CATCH
END

GO

CREATE TRIGGER content.OnDeleteQuestionDeleteComment ON content.Questions instead of DELETE
AS
BEGIN
UPDATE content.Comments 
SET isDeleted=1
Where questionID in(select questionID from deleted)
END
UPDATE content.Questions
SET isDeleted=1
where questionID in (select questionID from deleted)
GO

CREATE TRIGGER academic.OnDeleteCourse ON academic.Courses INSTEAD OF DELETE 
AS
BEGIN
UPDATE academic.Courses SET isActive=0 WHERE courseID in (SELECT courseID from deleted)
END
GO
CREATE TRIGGER academic.OnDeleteAnnouncements ON academic.Announcements INSTEAD  OF DELETE 
AS 
BEGIN
UPDATE academic.Announcements  SET isDeleted=1 WHERE EXISTS(SELECT * FROM deleted join academic.Announcements as AA ON AA.profID=deleted.profID AND AA.courseID=deleted.courseID AND AA.[date]=deleted.[date])
END
GO

CREATE TRIGGER content.OnDeleteComment ON content.Comments instead of DELETE 
AS
BEGIN
UPDATE content.Comments 
SET isDeleted=1
Where commentID in(select commentID from deleted)
END
GO

GO
CREATE PROCEDURE content.AddNewQuestion(@Msg nvarchar(400),@UserName nvarchar(30),@IsResolved bit,@ResourceID int)
AS
BEGIN
DECLARE @USerID int;
SET @USerID=(select userID from academic.Users Where userName=@UserName);
INSERT INTO content.Questions VALUES(@Msg,@USerID,0,@ResourceID,0)
END
GO

CREATE PROCEDURE content.SetQuestionResovled(@QuestionID INT,@IsResolved bit)
AS
UPDATE content.Questions SET isResolved=@IsResolved  WHERE content.Questions.questionID=@QuestionID

GO
CREATE PROCEDURE content.AddNewComment(@Msg nvarchar(4000),@UserName nvarchar(30),@QuestionID int)
AS
BEGIN
DECLARE @USerID int;
SET @USerID=(select userID from academic.Users Where userName=@UserName);
INSERT INTO content.Comments VALUES(@Msg,@USerID,@QuestionID,0)
END

GO
CREATE PROCEDURE academic.AddUserToAClass(@UserName nvarchar(30),@CourseCode nvarchar(30))
AS
BEGIN
DECLARE @StudentID int,@CourseID int;
SET @StudentID=(select AST.stuID from academic.Users as AU join academic.Students as AST on AST.userID=AU.userID
 where userName=@UserName)
SET @CourseID =(SELECT courseID from academic.Courses where code=@CourseCode)
INSERT INTO academic.Take VALUES(@CourseID,@StudentID)
END

GO
  
CREATE PROCEDURE academic.AddNewDepartment(@DepartmeantName nvarchar(20),@DepartmentDiscription nvarchar(300))
AS INSERT INTO academic.Departments VALUES (@DepartmeantName,@DepartmentDiscription)
GO

CREATE PROCEDURE academic.AddStudentToCourse(@UserName nvarchar(30),@CourseCode nvarchar(30))
AS
BEGIN
DECLARE @SUID int,@CouserID int
SET @SUID=(select stuID from academic.Students AS AC join  academic.Users AS AU on  AU.userID=AC.userID where AU.userName=@UserName)
SET @CouserID=(select courseID from academic.Courses where Courses.code=@CourseCode)
INSERT INTO academic.[Take] VALUES(@CouserID,@SUID);
END

GO
CREATE FUNCTION academic.StudentsByCourse (@CourseCode nvarchar (30) )
RETURNS @StudentByClass TABLE(
userID  int ,
stuID  int
)
AS
BEGIN 


INSERT INTO @StudentByClass(userID,stuID)
 SELECT AST.userID,AST.stuID
from academic.Courses AS AC join academic.[Take] as AT on AC.courseID=AT.courseID
join academic.Students AS AST on AST.stuID=AT.stuID 
where AC.code=@CourseCode 
RETURN
END

GO
CREATE PROCEDURE academic.MakeNewAnnouncement(@ProfUName nvarchar(30),@CourseCode nvarchar(30),@Date datetime ,@Mesg nvarchar (4000))
AS
BEGIN
DECLARE @ProfId int,@CourseId int;
SET @ProfId=(SELECT profID from academic.Professors join academic.Users on academic.Users.userID= academic.Professors.userID where academic.Users.userName=@ProfUName);
SET @CourseId=(select courseID from academic.Courses where Courses.code=@CourseCode)
INSERT INTO [academic].[Announcements] VALUES(@ProfId,@CourseId,@Date,@Mesg,0)
END

GO
CREATE VIEW academic.StudentsInfo
AS 
SELECT AU.userName,AU.name,AU.surname,Au.email,AD.departmentName,AST.garduationDate
FROM academic.Users AS AU join academic.Students as AST on AST.userID=AU.userID
join academic.Belong_To AS AB on Ab.userID=AU.userID 
join academic.Departments AS AD on Ab.departmentID=AD.departmentID

GO
CREATE VIEW academic.CoursesInfo 
AS
SELECT Ac.code,Ac.courseName,(select AU.name from academic.Professors  AS AP join academic.Users AS AU on AU.userID=AP.userID  where profID=AC.profID) AS [Prof's Name],
(select AU.surname from academic.Professors  AS AP join academic.Users AS AU on AU.userID=AP.userID  where profID=AC.profID) AS [Prof's Surname],
(SELECT AU.name FROM academic.TeachingAssistants AS AT join academic.Students AS ACC ON ACC.stuID=AT.stuID
JOIN academic.Users AS AU ON AU.userID=ACC.userID WHERE AT.tID=AT.tID) AS [TA's Name],
(SELECT AU.surname FROM academic.TeachingAssistants AS AT join academic.Students AS ACC ON ACC.stuID=AT.stuID
JOIN academic.Users AS AU ON AU.userID=ACC.userID WHERE AT.tID=AT.tID) AS [TA's Surname],
(select count(*) from academic.[Take]  AS AT join academic.Students AS AST ON AST.stuID=AT.stuID
join academic.Users AS AUS ON AUS.userID =AST.userID 
WHERE AUS.isActive=1 AND AT.courseID=AC.courseID) AS [Number of current Students],
AC.courseDescription,Ac.term,Ac.isActive
FROM academic.Courses  AS AC
GO
CREATE VIEW academic.DepartmentsInfo AS
SELECT AD.departmentName,
(SELECT COUNT(*) FROM academic.Belong_To AS BT JOIN academic.Users AU ON AU.userID=BT.userID JOIN academic.Professors AS AP ON AP.userID=AU.userID WHERE BT.departmentID=AD.departmentID) AS [Number of Professors in the dep],
(SELECT COUNT (*) FROM academic.Belong_To AS BT JOIN academic.Users AU ON AU.userID=BT.userID JOIN
academic.Students AS AST ON AST.userID=AU.userID where BT.departmentID=AD.departmentID) AS[Number of Students in dep],
(SELECT COUNT (*) FROM academic.Courses AS AC JOIN academic.Have AH ON AH.courseID=AC.courseID WHERE AH.departmentID=AD.departmentID) AS [Number Of Courses in dep]
FROM academic.Departments AS AD

GO

GO
CREATE VIEW academic.ProfInfo --degree gives errors when has multiple values
AS 
SELECT PF.title,AU.name,AU.surname,AU.userName,AU.email,PF.workingHours,AD.departmentName,
(SELECT degree FROM academic.Prof_Degree WHERE profID=PF.profID) AS Degree
,(SELECT COUNT (*) from academic.Courses WHERE academic.Courses.profID=PF.profID) AS [Number Of courses]
FROM academic.Professors AS PF join academic.Users AS AU ON AU.userID=PF.userID 
join academic.Belong_To AS AB on AB.userID=AU.userID 
join academic.Departments AS AD ON AD.departmentID=AB.departmentID;
GO
CREATE VIEW academic.ProfHasNoDepartment
AS 
 SELECT PF.title,AU.name,AU.surname,AU.userName,AU.email,PF.workingHours,
(SELECT degree FROM academic.Prof_Degree WHERE profID=PF.profID) AS Degree
,(SELECT COUNT (*) from academic.Courses WHERE academic.Courses.profID=PF.profID) AS [Number Of courses]
FROM academic.Professors AS PF join academic.Users AS AU ON AU.userID=PF.userID WHERE AU.userID NOT IN(select userID from academic.Belong_To )
GO
select * from academic.ProfHasNoDepartment

SELECT * from academic.ProfInfo
GO
CREATE PROCEDURE academic.AddUserToDepartment(@UserName nvarchar(30),@DepartementName nvarchar(20))
AS
BEGIN
DECLARE @UserId int,@DepId int
SELECT @UserId=AU.userID FROM academic.Users AS AU WHERE AU.userName=@UserName
SELECT @DepId=AD.departmentID FROM academic.Departments AS AD where AD.departmentName=@DepartementName 

INSERT INTO academic.Belong_To VALUES(@UserId,@DepId)
END
GO


CREATE PROCEDURE academic.AddCourseToDepartment(@CourseCode nvarchar(15),@DepartmentName nvarchar(20))
AS
BEGIN 
DECLARE @CourseId int,@DepId int
SELECT @CourseId=courseID FROM academic.Courses WHERE code=@CourseCode
SELECT @DepId=AD.departmentID FROM academic.Departments AS AD where AD.departmentName=@DepartmentName
INSERT INTO academic.Have VALUES(@CourseId,@DepId)
END
GO

CREATE PROCEDURE academic.CoursesByDepartment(@DepartmentName nvarchar(20))
AS
BEGIN
SELECT AC.code,AC.courseDescription,Ac.courseID,AC.courseName,AC.isActive,AC.profID,Ac.term,AC.tID FROM academic.Courses AS AC join academic.Have AH ON AH.courseID=AC.courseID JOIN academic.Departments AS AD  ON AD.departmentID=AH.departmentID WHERE @DepartmentName=AD.departmentName 
END
GO
CREATE PROCEDURE academic.StudentByUserName(@UserName nvarchar(30))AS
SELECT * FROM academic.Users as AU JOIN academic.Students AS AST ON AST.userID=AU.userID WHERE AU.userName=@Username
GO
GO
CREATE PROCEDURE academic.DepartmentByName(@DepartmentName nvarchar(30))AS
SELECT * FROM academic.Departments where departmentName=@DepartmentName;
GO
CREATE PROCEDURE academic.GetDepartments
AS
SELECT * from academic.Departments;
GO
CREATE PROCEDURE academic.UpdateStudentInfo(@Stdid int ,@gradDate date)AS
UPDATE academic.Students set garduationDate=@gradDate WHERE stuID=@Stdid;
Go
CREATE PROCEDURE academic.UserByUserName(@UserName varchar(30))AS
SELECT * from academic.Students AS AC join academic.Users AS AU ON AU.userID=AC.userID WHERE AU.userName=@UserName;
GO
CREATE PROCEDURE academic.StudentById(@StdId varchar(30))AS
SELECT * from academic.Students AS AC join academic.Users AS AU ON AU.userID=AC.userID WHERE Ac.stuID=@StdId;
GO
CREATE PROCEDURE academic.ProfessorByUserName(@ProfUserName nvarchar(30))AS
SELECT * from academic.Professors AS AP JOIN academic.Users AS AU ON AU.userID=AP.userID WHERE  AU.userName=@ProfUserName
GO
CREATE PROCEDURE academic.ProfessorByProfID(@ProfID int)AS
SELECT * from academic.Professors AS AP JOIN academic.Users AS AU ON AU.userID=AP.userID WHERE  AP.profID=@ProfID

exec academic.StudentByUserName 'py'
SELECT * from [academic].[CoursesInfo]
exec academic.AddNewDepartment 'CS','computer eng'
exec academic.AddNewDepartment'EE','Electoringasd asd'
SELECT * from [academic].[DepartmentsInfo]
SELECT * from [academic].ProfHasNoDepartment
select * from academic.StudentsInfo

go

create procedure academic.getstudentcourses(@id as nvarchar(50))
as
select *
from  [academic].[Students]as s inner join [academic].[Take] as t on s.stuID = t.stuID
join [academic].[Courses] as c on c.courseID= t.courseID
where s.stuID=@id 

go
create procedure academic.countstudentcourses(@code as nvarchar(50))
as
select COUNT([stuID]) AS stunum 
from  [academic].[Take] as t inner join [academic].[Courses] as c on t.courseID=c.courseID
where c.[code]=@code
GROUP BY (c.[code])
go
create procedure academic.getdepcourses(@id as nvarchar(50))
as
select *
from [academic].[Courses] as c inner join [academic].[Have] as h on c.courseID = h.courseID
join  [academic].[Departments] as d on d.departmentID= h.departmentID
where d.departmentID=@id
go

create procedure academic.getprofcourses(@id as nvarchar(50))
as
select *
from  [academic].[Professors]as p inner join [academic].[Courses] as c on p.profID = c.profID 
where p.profID=@id
go
create procedure academic.getCourses
as
select *
from [academic].[Courses] 
go
create procedure academic.countcourses
as
select COUNT([courseID]) AS cnum 
from   [academic].[Courses] 
where [isActive]=1
GROUP BY ([isActive])
go
create procedure academic.countresouce
as
select COUNT([resourceID]) AS rnum 
from   [content].[Resources]
where [isDeleted]=0
GROUP BY ([isDeleted])

go
create procedure academic.countlecture(@coursecode as nvarchar(50))
as
select COUNT([lectureID]) AS lecnum 
from   [content].[Resources] as r inner join [content].[Lectures]as l on r.resourceID=l.resourceID
join [academic].[Courses] as c on c.courseID = r.courseID
where c.code=@coursecode
GROUP BY (c.courseID)
go
  create procedure academic.Updatcourseinfo2(@id as nvarchar(50), @name as nvarchar(4000),@term as nvarchar(50), @desc as nvarchar(4000),@code as nvarchar(50),@tname as nvarchar(50),@isactiv as bit)
 as
  BEGIN
 DECLARE @STDID int
  DECLARE @Tid int
 SELECT @STDID=AST.stuID FROM academic.Students as AST join academic.Users AS AU ON AU.userID=AST.userID WHERE AU.userName=@tname
 INSERT INTO academic.TeachingAssistants VALUES(@STDID)
 SET @Tid=SCOPE_IDENTITY()


 update [academic].[Courses]  
 set [courseName]=@name,[term]=@term,[courseDescription]=@desc,[code]=@code,[tID]=@Tid,[isActive]=@isactiv
 where [courseID]=@id
 END
 go


 CREATE PROCEDURE academic.ResourcesByCourse(@CourseCode nvarchar(15))AS
 SELECT * FROM content.Resources AS CR join academic.Courses AS AC ON AC.courseID=CR.courseID where AC.code=@CourseCode

 GO
 CREATE PROCEDURE content.ResourceByID(@ResourceID int)AS
 SELECT *from content.Lectures WHERE resourceID=@ResourceID
 GO

 CREATE PROCEDURE content.LectureById(@LectureID int)AS
 SELECT * from content.Lectures where content.Lectures.lectureID=@LectureID;

 
 CREATE PROCEDURE content.LecturerByCourse(@CourseCode nvarchar(15))AS
 SELECT * from content.Lectures AS CL join  content.Resources as CR  ON CL.resourceID=Cr.resourceID
 join academic.Courses AS AC ON CR.courseID=AC.courseID WHERE AC.code=@CourseCode;

 CREATE PROCEDURE academic.DepartmentsOfUser(@UserName nvarchar(50))AS
 SELECT * from academic.Users AS AU join academic.Belong_To  AS AB ON AB.userID=AU.userID
 JOIN academic.Departments AS AD ON AD.departmentID=AB.departmentID WHERE AU.userName=@UserName 

 CREATE PROCEDURE academic.UpdateProfessorInfo(@ProfID int ,@WorkingHours int,@Title nvarchar(20))AS
 UPDATE academic.Professors SET workingHours=@WorkingHours, title=@Title WHERE profID=@ProfID;

 GO
 CREATE PROCEDURE content.QuestionByID(@questionID int)AS
 SELECT * from  content.Questions where questionID=@questionID;
 GO
 CREATE PROCEDURE content.QuestionByResource(@ResourceID nvarchar(20))AS
 SELECT * FROM content.Questions  WHERE  ressourceID=@ResourceID
 GO

 CREATE PROCEDURE content.CommentByID(@CommentID int)AS
 Select * from content.Comments where commentID=@CommentID;

 CREATE PROCEDURE content.CommentsByQuestion(@QuestionId int)AS
 SELECT * FROM content.Comments WHERE questionID=@QuestionId


 
 CREATE PROCEDURE content.AddCommentToQuestion(@Int
=======
CREATE DATABASE VirtualClass
go
GO

USE [master]
GO

CREATE LOGIN [Admin] WITH PASSWORD='1234', DEFAULT_DATABASE=[VirtualClass], DEFAULT_LANGUAGE=[us_english], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF
GO
GRANT CREATE ANY DATABASE TO [Admin]
ALTER LOGIN [Admin] DISABLE
GO

ALTER SERVER ROLE [sysadmin] ADD MEMBER [Admin]
GO

ALTER SERVER ROLE [securityadmin] ADD MEMBER [Admin]
GO

ALTER SERVER ROLE [serveradmin] ADD MEMBER [Admin]
GO

ALTER SERVER ROLE [setupadmin] ADD MEMBER [Admin]
GO

ALTER SERVER ROLE [processadmin] ADD MEMBER [Admin]
GO

ALTER SERVER ROLE [diskadmin] ADD MEMBER [Admin]
GO

ALTER SERVER ROLE [dbcreator] ADD MEMBER [Admin]
GO

ALTER SERVER ROLE [bulkadmin] ADD MEMBER [Admin]
GO


USE [VirtualClass]
GO

CREATE USER [Ad] FOR LOGIN [Admin] WITH DEFAULT_SCHEMA=[content]
GO
GO
USE VirtualClass
GO
CREATE SCHEMA academic
GO
CREATE SCHEMA content
Go
CREATE TABLE academic.Departments(
departmentID int NOT NULL PRIMARY KEY IDENTITY(1,1),
departmentName nvarchar(20) UNIQUE NOT NULL,
departmentDescription nvarchar(300)
--dont forget to make departmentName unique
)
GO
CREATE TABLE academic.Users(
userID int PRIMARY KEY IDENTITY(1,1),
pass nvarchar(200) NOT NULL,
userName nvarchar(30) UNIQUE  NOT NULL,
userType nvarchar(10) NOT NULL,
name nvarchar (30)Not NULL,
surname nvarchar(30) NOT NULL,
picture nvarchar(100),
email nvarchar(50) NOT NULL,
phone nvarchar(20),
personalInfo nvarchar(300),
isActive bit NOT NULL
)
GO
CREATE TABLE academic.Students(
stuID int IDENTITY(1,1) PRIMARY KEY,
garduationDate date ,
userID int FOREIGN KEY REFERENCES academic.Users(userId)  NOT NULL
)
GO
CREATE TABLE academic.TeachingAssistants(
tID int IDENTITY(1,1) PRIMARY KEY,
stuID int FOREIGN KEY REFERENCES academic.Students(stuID)  NOT NULL
)

GO
CREATE TABLE academic.Professors(
profID int IDENTITY(1,1) PRIMARY KEY,
workingHours int NOT NULL,
title nvarchar(20) NOT NULL,
userID int FOREIGN KEY REFERENCES academic.Users(userId)  NOT NULL
)
GO

CREATE TABLE academic.Courses(
courseID int IDENTITY(1,1) PRIMARY KEY,
courseName nvarchar(30) not NULL,
term nvarchar (15) not null,
courseDescription nvarchar(200),
code nvarchar (15) UNIQUE NOT NULL,
tID int FOREIGN KEY REFERENCES academic.TeachingAssistants(tID) ON DELETE SET NULL,
profID int FOREIGN KEY REFERENCES academic.Professors(profID) NOT NULL,
isActive bit NOT NULL
)
GO
CREATE TABLE content.Resources(
resourceID int IDENTITY(1,1) PRIMARY KEY,
courseID int FOREIGN KEY REFERENCES academic.Courses(courseID) NOT NULL,
profId int FOREIGN KEY REFERENCES academic.Professors(profID) NOT NULL,
[Path] nvarchar(200) UNIQUE NOT NULL,
isDeleted bit NOT NULL
)
GO
CREATE TABLE content.Lectures(
lectureID int PRIMARY KEY IDENTITY(1,1),
title nvarchar(100) NOT NULL,
resourceID int FOREIGN KEY REFERENCES content.Resources(resourceId) NOT NULL,
number int NOT NULL,
)
GO

CREATE TABLE academic.Belong_To(
userID int FOREIGN KEY REFERENCES academic.Users(userID) NOT NULL ,
departmentID int FOREIGN KEY REFERENCES academic.Departments(departmentID) NOT NULL,
CONSTRAINT PK_UserID_DepartmentID PRIMARY KEY (userId,departmentID)
)
GO
CREATE TABLE academic.Have(
courseID int FOREIGN KEY REFERENCES academic.Courses(courseID) NOT NULL,
departmentID int FOrEIGN KEY REFERENCES academic.Departments(departmentID) NOT NULL,
PRIMARY KEY (courseID,departmentID)
)
GO
CREATE TABLE academic.[Take](
courseID int FOREIGN KEY REFERENCES academic.Courses(courseID) NOT NULL,
stuID int FOREIGN KEY REFERENCES academic.Students(stuID) NOT NULL,
PRIMARY KEY(courseID,stuID)
)
GO
CREATE TABLE academic.Prof_Degree(
profID int FOREIGN KEY REFERENCES academic.Professors(profID) NOT NULL,
degree varchar (30) NOT NULL,
[date] date NOT NULL,
major varchar(30) NOT NULL,
PRIMARY KEY(profID,degree,[date],major)
)
GO
CREATE TABLE content.Questions(
questionID INT PRIMARY KEY IDENTITY(1,1),
mssg nvarchar(400) NOT NULL,
userID int FOREIGN KEY REFERENCES academic.Users(userID) NOT NULL,
isResolved BIT NOT NULL,
ressourceID int FOREIGN KEY REFERENCES content.RESOURCES(resourceId),
isDeleted BIT NOT NULL,
)
Go
CREATE TABLE content.Comments(
commentID INT PRIMARY KEY IDENTITY(1,1),
mssg nvarchar(4000) NOT NULL,
userID int FOREIGN KEY REFERENCES academic.Users(userID) NOT NULL,
questionID int FOREIGN KEY REFERENCES content.Questions(questionID)  NOT NULL ,--on delete caused problem here
isDeleted BIT NOT NULL
)
GO

CREATE TABLE academic.Announcements(
profID int FOREIGN KEY REFERENCES academic.Professors(profId) NOT NULL,
courseID int FOREIGN KEY REFERENCES academic.Courses(courseID) NOT NULL,
[date] datetime NOT NULL,
[message] varchar (4000) NOT NULL,
isDeleted BIT NOT NULL
PRIMARY KEY (profID,CourseID,[date])
)
GO

CREATE PROCEDURE academic.DoesUserExistAndHisAuthenticationInfoISCorrect(@UserName nvarchar(30),@PassWord nvarchar(200),@Result bit OUTPUT)  
AS
BEGIN
IF EXISTS (SELECT userID FROM academic.Users  where userName=@UserName AND pass=@PassWord )
	BEGIN
		SET @Result=1
	END
ELSE	
	BEGIN
		SET @Result=0
	END
END

GO

CREATE PROCEDURE academic.TypeOFUser(@UserName varchar(30),@Type nvarchar(10)OUT)
AS
BEGIN
IF NOT EXISTS (SELECT userType from academic.Users where userName=@UserName)
	SET @Type='UNKNOWN';
ELSE
	SET @Type=(select userType from academic.Users where userName=@UserName)

END

GO

CREATE PROCEDURE academic.UpdateUserInfo(@UserName nvarchar(30),@Password nvarchar(200),@Picture nvarchar(100),@Email nvarchar(50),@Phone nvarchar(20),@PersonalInfo nvarchar (30),@Name nvarchar(50),@Surname nvarchar(50))
As
BEGIN
IF EXISTS (select userName from academic.Users where userName=@UserName)
BEGIN
	IF @Password IS NOT NULL AND LEN(@Password) >0
		BEGIN
			UPDATE academic.Users SET pass=@Password WHERE userName=@UserName
		END
	IF @Email IS NOT NULL AND LEN(@Email) >0
		UPDATE academic.Users SET email=@Email where userName=@UserName
	IF @Picture IS NOT NULL AND LEN(@Picture) >0
		UPDATE academic.Users SET picture=@Picture where userName=@UserName
	END
	
	UPDATE academic.Users SET personalInfo=@PersonalInfo ,phone=@Phone ,name=@Name,surname=@Surname
		where userName=@UserName
END


Go
CREATE PROCEDURE academic.AddNewStudent(@UserName nvarchar(30),@Password nvarchar(200),@Name nvarchar(30),@Surname nvarchar(30),@Picture nvarchar(100),@Email nvarchar(50),@Phone nvarchar(20),@PersonalInf nvarchar(300),@GradDate date)
AS
BEGIN

DECLARE @UID as INT


	BEGIN TRY
		BEGIN TRANSACTION
		INSERT INTO academic.Users VALUES(@Password,@UserName,'STUDENT',@Name,@Surname,@Picture,@Email,@Phone,@PersonalInf,1)
		SET @UID =SCOPE_IDENTITY()
		INSERT INTO academic.Students VALUES(@GradDate,@UID)
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;
	 SELECT 
        @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();

		raiserror(@ErrorMessage,@ErrorSeverity,@ErrorState)
		rollback transaction
	END CATCH
END

Go

CREATE PROCEDURE academic.AddNewProf(@UserName nvarchar(30),@Password nvarchar(200),
										@Name nvarchar(30),@Surname nvarchar(30),@Picture nvarchar(100)
										,@Email nvarchar(50),@Phone nvarchar(20),@PersonalInf nvarchar(300)
										,@WorkingHours int,@Title nvarchar(20))
AS
BEGIN
DECLARE @DpID as int
DECLARE @UID as INT

	BEGIN TRY
		BEGIN TRANSACTION
		INSERT INTO academic.Users VALUES(@Password,@UserName,'PROF',@Name,@Surname,@Picture,@Email,@Phone,@PersonalInf,1)
		SET @UID =SCOPE_IDENTITY()
		INSERT INTO academic.Professors VALUES(@WorkingHours,@Title,@UID)
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;
	 SELECT 
        @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();

		raiserror(@ErrorMessage,@ErrorSeverity,@ErrorState)
		rollback transaction
	END CATCH
END



Go

 CREATE PROCEDURE academic.AddDegree(@ProfUserName nvarchar (30),@Degree nvarchar(30),@Date date,@Major nvarchar(30))
 AS
 BEGIN
	DECLARE @ProfId int
 
	SET @ProfId=(
				select profId
				from academic.Professors as AP join academic.Users as AU on AP.userID=AU.userID
				 WHERE AU.userName=@ProfUserName
				)
	INSERT INTO academic.Prof_Degree VALUES(@ProfId,@Degree,@Date,@Major)
 END
 

 
 GO
 CREATE PROCEDURE academic.AddNewCourse( @CourseName nvarchar(30),@CourseCode nvarchar(15),@Term nvarchar(30),@CourseDescription nvarchar(200),@TidUserName nvarchar(30),@ProfUserName nvarchar(30))
 AS
 BEGIN
 BEGIN TRY
 BEGIN TRANSACTION
	DECLARE @DpID AS INT,@ProfID AS INT,@Tid AS INT ,@StID AS INT

	 SET @ProfID=(
					select profId
					from academic.Professors as AP join academic.Users as AU on AP.userID=AU.userID
					 WHERE AU.userName=@ProfUserName
		)
		IF @TidUserName IS NOT NULL
			BEGIN
				SET @StID=(Select stuID
				from academic.Students as ST join academic.Users AS AU on AU.userID=ST.userID WHERE AU.userName=@TidUserName
				)
				IF @StID IS NOT NULL
				BEGIN 
					INSERT INTO academic.TeachingAssistants VALUES(@StID)
					SET @Tid=SCOPE_IDENTITY()
				END
			END
		INSERT INTO academic.Courses VALUES(@CourseName,@Term,@CourseDescription,@CourseCode,@Tid,@ProfID,1);
		COMMIT TRANSACTION
		END TRY
		BEGIN CATCH
		
			DECLARE @ErrorMessage NVARCHAR(4000);
			DECLARE @ErrorSeverity INT;
			DECLARE @ErrorState INT;
			 SELECT 
				@ErrorMessage = ERROR_MESSAGE(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE();

				raiserror(@ErrorMessage,@ErrorSeverity,@ErrorState)
				rollback transaction
			
		END CATCH
 END



GO
CREATE PROCEDURE content.AddNewResources(@CourseCode nvarchar(15),@ProfUserName nvarchar(30),@Path nvarchar(200))AS
BEGIN
DECLARE @PRofID int;
DECLARE @CourseID int;
SELECT @PRofID=AP.profID,@CourseID=AC.courseID
from academic.Users as AU join academic.Professors AS AP ON AP.userID=AU.userID
JOIN academic.Courses as AC on AC.profID=Ap.profID
	Where AU.userName=@ProfUserName AND AC.code=@CourseCode;
INSERT INTO content.Resources VALUES (@CourseID,@PRofID,@Path,0)
END


Go
CREATE PROCEDURE content.AddNewLecture(@CourseCode nvarchar(15),@ProfUserName nvarchar(30),@YoutubeLink nvarchar(200),@title nvarchar (100),@Number int)AS
BEGIN
DECLARE @PRofID int;
DECLARE @CourseID int;
DECLARE @RID INT;
BEGIN TRY
BEGIN TRANSACTION
	SELECT @PRofID=AP.profID,@CourseID=AC.courseID
	from academic.Users as AU join academic.Professors AS AP ON AP.userID=AU.userID
	JOIN academic.Courses as AC on AC.profID=Ap.profID
		Where AU.userName=@ProfUserName AND AC.code=@CourseCode;
	INSERT INTO content.Resources VALUES (@CourseID,@PRofID,@YoutubeLink,0)
	SET @RID=SCOPE_IDENTITY()
	INSERT INTO content.Lectures VALUES(@title,@RID,@Number)
	commit transaction
	END TRY
	BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000);
			DECLARE @ErrorSeverity INT;
			DECLARE @ErrorState INT;
			 SELECT 
				@ErrorMessage = ERROR_MESSAGE(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE();
	rollback TRANSACTION
	END CATCH
END

GO

CREATE TRIGGER content.OnDeleteQuestionDeleteComment ON content.Questions instead of DELETE
AS
BEGIN
UPDATE content.Comments 
SET isDeleted=1
Where questionID in(select questionID from deleted)
END
UPDATE content.Questions
SET isDeleted=1
where questionID in (select questionID from deleted)
GO

CREATE TRIGGER academic.OnDeleteCourse ON academic.Courses INSTEAD OF DELETE 
AS
BEGIN
UPDATE academic.Courses SET isActive=0 WHERE courseID in (SELECT courseID from deleted)
END
GO
CREATE TRIGGER academic.OnDeleteAnnouncements ON academic.Announcements INSTEAD  OF DELETE 
AS 
BEGIN
UPDATE academic.Announcements  SET isDeleted=1 WHERE EXISTS(SELECT * FROM deleted join academic.Announcements as AA ON AA.profID=deleted.profID AND AA.courseID=deleted.courseID AND AA.[date]=deleted.[date])
END
GO

CREATE TRIGGER content.OnDeleteComment ON content.Comments instead of DELETE 
AS
BEGIN
UPDATE content.Comments 
SET isDeleted=1
Where commentID in(select commentID from deleted)
END
GO

GO
CREATE PROCEDURE content.AddNewQuestion(@Msg nvarchar(400),@UserName nvarchar(30),@IsResolved bit,@ResourceID int)
AS
BEGIN
DECLARE @USerID int;
SET @USerID=(select userID from academic.Users Where userName=@UserName);
INSERT INTO content.Questions VALUES(@Msg,@USerID,0,@ResourceID,0)
END
GO

CREATE PROCEDURE content.SetQuestionResovled(@QuestionID INT,@IsResolved bit)
AS
UPDATE content.Questions SET isResolved=@IsResolved  WHERE content.Questions.questionID=@QuestionID

GO
CREATE PROCEDURE content.AddNewComment(@Msg nvarchar(4000),@UserName nvarchar(30),@QuestionID int)
AS
BEGIN
DECLARE @USerID int;
SET @USerID=(select userID from academic.Users Where userName=@UserName);
INSERT INTO content.Comments VALUES(@Msg,@USerID,@QuestionID,0)
END

GO
CREATE PROCEDURE academic.AddUserToAClass(@UserName nvarchar(30),@CourseCode nvarchar(30))
AS
BEGIN
DECLARE @StudentID int,@CourseID int;
SET @StudentID=(select AST.stuID from academic.Users as AU join academic.Students as AST on AST.userID=AU.userID
 where userName=@UserName)
SET @CourseID =(SELECT courseID from academic.Courses where code=@CourseCode)
INSERT INTO academic.Take VALUES(@CourseID,@StudentID)
END

GO
  
CREATE PROCEDURE academic.AddNewDepartment(@DepartmeantName nvarchar(20),@DepartmentDiscription nvarchar(300))
AS INSERT INTO academic.Departments VALUES (@DepartmeantName,@DepartmentDiscription)
GO

CREATE PROCEDURE academic.AddStudentToCourse(@UserName nvarchar(30),@CourseCode nvarchar(30))
AS
BEGIN
DECLARE @SUID int,@CouserID int
SET @SUID=(select stuID from academic.Students AS AC join  academic.Users AS AU on  AU.userID=AC.userID where AU.userName=@UserName)
SET @CouserID=(select courseID from academic.Courses where Courses.code=@CourseCode)
INSERT INTO academic.[Take] VALUES(@CouserID,@SUID);
END

GO
CREATE FUNCTION academic.StudentsByCourse (@CourseCode nvarchar (30) )
RETURNS @StudentByClass TABLE(
userID  int ,
stuID  int
)
AS
BEGIN 


INSERT INTO @StudentByClass(userID,stuID)
 SELECT AST.userID,AST.stuID
from academic.Courses AS AC join academic.[Take] as AT on AC.courseID=AT.courseID
join academic.Students AS AST on AST.stuID=AT.stuID 
where AC.code=@CourseCode 
RETURN
END

GO
CREATE PROCEDURE academic.MakeNewAnnouncement(@ProfUName nvarchar(30),@CourseCode nvarchar(30),@Date datetime ,@Mesg nvarchar (4000))
AS
BEGIN
DECLARE @ProfId int,@CourseId int;
SET @ProfId=(SELECT profID from academic.Professors join academic.Users on academic.Users.userID= academic.Professors.userID where academic.Users.userName=@ProfUName);
SET @CourseId=(select courseID from academic.Courses where Courses.code=@CourseCode)
INSERT INTO [academic].[Announcements] VALUES(@ProfId,@CourseId,@Date,@Mesg,0)
END

GO
CREATE VIEW academic.StudentsInfo
AS 
SELECT AU.userName,AU.name,AU.surname,Au.email,AD.departmentName,AST.garduationDate
FROM academic.Users AS AU join academic.Students as AST on AST.userID=AU.userID
join academic.Belong_To AS AB on Ab.userID=AU.userID 
join academic.Departments AS AD on Ab.departmentID=AD.departmentID

GO
CREATE VIEW academic.CoursesInfo 
AS
SELECT Ac.code,Ac.courseName,(select AU.name from academic.Professors  AS AP join academic.Users AS AU on AU.userID=AP.userID  where profID=AC.profID) AS [Prof's Name],
(select AU.surname from academic.Professors  AS AP join academic.Users AS AU on AU.userID=AP.userID  where profID=AC.profID) AS [Prof's Surname],
(SELECT AU.name FROM academic.TeachingAssistants AS AT join academic.Students AS ACC ON ACC.stuID=AT.stuID
JOIN academic.Users AS AU ON AU.userID=ACC.userID WHERE AT.tID=AT.tID) AS [TA's Name],
(SELECT AU.surname FROM academic.TeachingAssistants AS AT join academic.Students AS ACC ON ACC.stuID=AT.stuID
JOIN academic.Users AS AU ON AU.userID=ACC.userID WHERE AT.tID=AT.tID) AS [TA's Surname],
(select count(*) from academic.[Take]  AS AT join academic.Students AS AST ON AST.stuID=AT.stuID
join academic.Users AS AUS ON AUS.userID =AST.userID 
WHERE AUS.isActive=1 AND AT.courseID=AC.courseID) AS [Number of current Students],
AC.courseDescription,Ac.term,Ac.isActive
FROM academic.Courses  AS AC
GO
CREATE VIEW academic.DepartmentsInfo AS
SELECT AD.departmentName,
(SELECT COUNT(*) FROM academic.Belong_To AS BT JOIN academic.Users AU ON AU.userID=BT.userID JOIN academic.Professors AS AP ON AP.userID=AU.userID WHERE BT.departmentID=AD.departmentID) AS [Number of Professors],
(SELECT COUNT (*) FROM academic.Belong_To AS BT JOIN academic.Users AU ON AU.userID=BT.userID JOIN
academic.Students AS AST ON AST.userID=AU.userID where BT.departmentID=AD.departmentID) AS[Number of Students in dep],
(SELECT COUNT (*) FROM academic.Courses AS AC JOIN academic.Have AH ON AH.courseID=AC.courseID WHERE AH.departmentID=AD.departmentID) AS [Number Of Courses]
FROM academic.Departments AS AD

GO
SELECT * from academic.DepartmentsInfo
GO
CREATE VIEW academic.ProfInfo
AS 
SELECT PF.title,AU.name,AU.surname,AU.userName,AU.email,PF.workingHours,AD.departmentName,
(SELECT degree FROM academic.Prof_Degree WHERE profID=PF.profID) AS Degree
,(SELECT COUNT (*) from academic.Courses WHERE academic.Courses.profID=PF.profID) AS [Number Of courses]
FROM academic.Professors AS PF join academic.Users AS AU ON AU.userID=PF.userID 
join academic.Belong_To AS AB on AB.userID=AU.userID 
join academic.Departments AS AD ON AD.departmentID=AB.departmentID;
GO
CREATE VIEW academic.ProfHasNoDepartment
AS 
 SELECT PF.title,AU.name,AU.surname,AU.userName,AU.email,PF.workingHours,
(SELECT degree FROM academic.Prof_Degree WHERE profID=PF.profID) AS Degree
,(SELECT COUNT (*) from academic.Courses WHERE academic.Courses.profID=PF.profID) AS [Number Of courses]
FROM academic.Professors AS PF join academic.Users AS AU ON AU.userID=PF.userID WHERE AU.userID NOT IN(select userID from academic.Belong_To )
GO
select * from academic.ProfHasNoDepartment

SELECT * from academic.ProfInfo
GO
CREATE PROCEDURE academic.AddUserToDepartment(@UserName nvarchar(30),@DepartementName nvarchar(20))
AS
BEGIN
DECLARE @UserId int,@DepId int
SELECT @UserId=AU.userID FROM academic.Users AS AU WHERE AU.userName=@UserName
SELECT @DepId=AD.departmentID FROM academic.Departments AS AD where AD.departmentName=@DepartementName 

INSERT INTO academic.Belong_To VALUES(@UserId,@DepId)
END
GO


CREATE PROCEDURE academic.AddCourseToDepartment(@CourseCode nvarchar(15),@DepartmentName nvarchar(20))
AS
BEGIN 
DECLARE @CourseId int,@DepId int
SELECT @CourseId=courseID FROM academic.Courses WHERE code=@CourseCode
SELECT @DepId=AD.departmentID FROM academic.Departments AS AD where AD.departmentName=@DepartmentName
INSERT INTO academic.Have VALUES(@CourseId,@DepId)
END
GO

CREATE PROCEDURE academic.CoursesByDepartment(@DepartmentName nvarchar(20))
AS
BEGIN
SELECT AC.code,AC.courseDescription,Ac.courseID,AC.courseName,AC.isActive,AC.profID,Ac.term,AC.tID FROM academic.Courses AS AC join academic.Have AH ON AH.courseID=AC.courseID JOIN academic.Departments AS AD  ON AD.departmentID=AH.departmentID WHERE @DepartmentName=AD.departmentName 
END
GO
CREATE PROCEDURE academic.StudentByUserName(@UserName nvarchar(30))AS
SELECT * FROM academic.Users as AU JOIN academic.Students AS AST ON AST.userID=AU.userID WHERE AU.userName=@Username
GO
GO
CREATE PROCEDURE academic.DepartmentByName(@DepartmentName nvarchar(30))AS
SELECT * FROM academic.Departments where departmentName=@DepartmentName;
GO
CREATE PROCEDURE academic.GetDepartments
AS
SELECT * from academic.Departments;
GO
CREATE PROCEDURE academic.UpdateStudentInfo(@Stdid int ,@gradDate date)AS
UPDATE academic.Students set garduationDate=@gradDate WHERE stuID=@Stdid;
Go
CREATE PROCEDURE academic.UserByUserName(@UserName varchar(30))AS
SELECT * from academic.Students AS AC join academic.Users AS AU ON AU.userID=AC.userID WHERE AU.userName=@UserName;
GO
CREATE PROCEDURE academic.StudentById(@StdId varchar(30))AS
SELECT * from academic.Students AS AC join academic.Users AS AU ON AU.userID=AC.userID WHERE Ac.stuID=@StdId;
GO
CREATE PROCEDURE academic.ProfessorByUserName(@ProfUserName nvarchar(30))AS
SELECT * from academic.Professors AS AP JOIN academic.Users AS AU ON AU.userID=AP.userID WHERE  AU.userName=@ProfUserName
GO
CREATE PROCEDURE academic.ProfessorByProfID(@ProfID int)AS
SELECT * from academic.Professors AS AP JOIN academic.Users AS AU ON AU.userID=AP.userID WHERE  AP.profID=@ProfID

go

create procedure academic.getstudentcourses(@id as nvarchar(50))
as
select *
from  [academic].[Students]as s inner join [academic].[Take] as t on s.stuID = t.stuID
join [academic].[Courses] as c on c.courseID= t.courseID
where s.stuID=@id 

go
create procedure academic.countstudentcourses(@code as nvarchar(50))
as
select COUNT([stuID]) AS stunum 
from  [academic].[Take] as t inner join [academic].[Courses] as c on t.courseID=c.courseID
where c.[code]=@code
GROUP BY (c.[code])
go
create procedure academic.getdepcourses(@id as nvarchar(50))
as
select *
from [academic].[Courses] as c inner join [academic].[Have] as h on c.courseID = h.courseID
join  [academic].[Departments] as d on d.departmentID= h.departmentID
where d.departmentID=@id
go

create procedure academic.getprofcourses(@id as nvarchar(50))
as
select *
from  [academic].[Professors]as p inner join [academic].[Courses] as c on p.profID = c.profID 
where p.profID=@id
go
create procedure academic.getCourses
as
select *
from [academic].[Courses] 
go
create procedure academic.countcourses
as
select COUNT([courseID]) AS cnum 
from   [academic].[Courses] 
where [isActive]=1
GROUP BY ([isActive])
go
create procedure academic.countresouce
as
select COUNT([resourceID]) AS rnum 
from   [content].[Resources]
where [isDeleted]=0
GROUP BY ([isDeleted])
go
create procedure academic.countlecture(@coursecode as nvarchar(50))
as
select COUNT([lectureID]) AS lecnum 
from   [content].[Resources] as r inner join [content].[Lectures]as l on r.resourceID=l.resourceID
join [academic].[Courses] as c on c.courseID = r.courseID
where c.code=@coursecode
GROUP BY (c.courseID)
=======
CREATE DATABASE VirtualClass
go
GO

USE [master]
GO

CREATE LOGIN [Admin] WITH PASSWORD='1234', DEFAULT_DATABASE=[VirtualClass], DEFAULT_LANGUAGE=[us_english], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF
GO
GRANT CREATE ANY DATABASE TO [Admin]
ALTER LOGIN [Admin] DISABLE
GO

ALTER SERVER ROLE [sysadmin] ADD MEMBER [Admin]
GO

ALTER SERVER ROLE [securityadmin] ADD MEMBER [Admin]
GO

ALTER SERVER ROLE [serveradmin] ADD MEMBER [Admin]
GO

ALTER SERVER ROLE [setupadmin] ADD MEMBER [Admin]
GO

ALTER SERVER ROLE [processadmin] ADD MEMBER [Admin]
GO

ALTER SERVER ROLE [diskadmin] ADD MEMBER [Admin]
GO

ALTER SERVER ROLE [dbcreator] ADD MEMBER [Admin]
GO

ALTER SERVER ROLE [bulkadmin] ADD MEMBER [Admin]
GO


USE [VirtualClass]
GO

CREATE USER [Ad] FOR LOGIN [Admin] WITH DEFAULT_SCHEMA=[content]
GO
GO
USE VirtualClass
GO
CREATE SCHEMA academic
GO
CREATE SCHEMA content
Go
CREATE TABLE academic.Departments(
departmentID int NOT NULL PRIMARY KEY IDENTITY(1,1),
departmentName nvarchar(20) UNIQUE NOT NULL,
departmentDescription nvarchar(300)
--dont forget to make departmentName unique
)
GO
CREATE TABLE academic.Users(
userID int PRIMARY KEY IDENTITY(1,1),
pass nvarchar(200) NOT NULL,
userName nvarchar(30) UNIQUE  NOT NULL,
userType nvarchar(10) NOT NULL,
name nvarchar (30)Not NULL,
surname nvarchar(30) NOT NULL,
picture nvarchar(100),
email nvarchar(50) NOT NULL,
phone nvarchar(20),
personalInfo nvarchar(300),
isActive bit NOT NULL
)
GO
CREATE TABLE academic.Students(
stuID int IDENTITY(1,1) PRIMARY KEY,
garduationDate date ,
userID int FOREIGN KEY REFERENCES academic.Users(userId)  NOT NULL
)
GO
CREATE TABLE academic.TeachingAssistants(
tID int IDENTITY(1,1) PRIMARY KEY,
stuID int FOREIGN KEY REFERENCES academic.Students(stuID)  NOT NULL
)

GO
CREATE TABLE academic.Professors(
profID int IDENTITY(1,1) PRIMARY KEY,
workingHours int NOT NULL,
title nvarchar(20) NOT NULL,
userID int FOREIGN KEY REFERENCES academic.Users(userId)  NOT NULL
)
GO

CREATE TABLE academic.Courses(
courseID int IDENTITY(1,1) PRIMARY KEY,
courseName nvarchar(30) not NULL,
term nvarchar (15) not null,
courseDescription nvarchar(200),
code nvarchar (15) UNIQUE NOT NULL,
tID int FOREIGN KEY REFERENCES academic.TeachingAssistants(tID) ON DELETE SET NULL,
profID int FOREIGN KEY REFERENCES academic.Professors(profID) NOT NULL,
isActive bit NOT NULL
)
GO
CREATE TABLE content.Resources(
resourceID int IDENTITY(1,1) PRIMARY KEY,
courseID int FOREIGN KEY REFERENCES academic.Courses(courseID) NOT NULL,
profId int FOREIGN KEY REFERENCES academic.Professors(profID) NOT NULL,
[Path] nvarchar(200) UNIQUE NOT NULL,
isDeleted bit NOT NULL
)
GO
CREATE TABLE content.Lectures(
lectureID int PRIMARY KEY IDENTITY(1,1),
title nvarchar(100) NOT NULL,
resourceID int FOREIGN KEY REFERENCES content.Resources(resourceId) NOT NULL,
number int NOT NULL,
)
GO

CREATE TABLE academic.Belong_To(
userID int FOREIGN KEY REFERENCES academic.Users(userID) NOT NULL ,
departmentID int FOREIGN KEY REFERENCES academic.Departments(departmentID) NOT NULL,
CONSTRAINT PK_UserID_DepartmentID PRIMARY KEY (userId,departmentID)
)
GO
CREATE TABLE academic.Have(
courseID int FOREIGN KEY REFERENCES academic.Courses(courseID) NOT NULL,
departmentID int FOrEIGN KEY REFERENCES academic.Departments(departmentID) NOT NULL,
PRIMARY KEY (courseID,departmentID)
)
GO
CREATE TABLE academic.[Take](
courseID int FOREIGN KEY REFERENCES academic.Courses(courseID) NOT NULL,
stuID int FOREIGN KEY REFERENCES academic.Students(stuID) NOT NULL,
PRIMARY KEY(courseID,stuID)
)
GO
CREATE TABLE academic.Prof_Degree(
profID int FOREIGN KEY REFERENCES academic.Professors(profID) NOT NULL,
degree varchar (30) NOT NULL,
[date] date NOT NULL,
major varchar(30) NOT NULL,
PRIMARY KEY(profID,degree,[date],major)
)
GO
CREATE TABLE content.Questions(
questionID INT PRIMARY KEY IDENTITY(1,1),
mssg nvarchar(400) NOT NULL,
userID int FOREIGN KEY REFERENCES academic.Users(userID) NOT NULL,
isResolved BIT NOT NULL,
ressourceID int FOREIGN KEY REFERENCES content.RESOURCES(resourceId),
isDeleted BIT NOT NULL,
)
Go
CREATE TABLE content.Comments(
commentID INT PRIMARY KEY IDENTITY(1,1),
mssg nvarchar(4000) NOT NULL,
userID int FOREIGN KEY REFERENCES academic.Users(userID) NOT NULL,
questionID int FOREIGN KEY REFERENCES content.Questions(questionID)  NOT NULL ,--on delete caused problem here
isDeleted BIT NOT NULL
)
GO

CREATE TABLE academic.Announcements(
profID int FOREIGN KEY REFERENCES academic.Professors(profId) NOT NULL,
courseID int FOREIGN KEY REFERENCES academic.Courses(courseID) NOT NULL,
[date] datetime NOT NULL,
[message] varchar (4000) NOT NULL,
isDeleted BIT NOT NULL
PRIMARY KEY (profID,CourseID,[date])
)
GO

CREATE PROCEDURE academic.DoesUserExistAndHisAuthenticationInfoISCorrect(@UserName nvarchar(30),@PassWord nvarchar(200),@Result bit OUTPUT)  
AS
BEGIN
IF EXISTS (SELECT userID FROM academic.Users  where userName=@UserName AND pass=@PassWord )
	BEGIN
		SET @Result=1
	END
ELSE	
	BEGIN
		SET @Result=0
	END
END

GO

CREATE PROCEDURE academic.TypeOFUser(@UserName varchar(30),@Type nvarchar(10)OUT)
AS
BEGIN
IF NOT EXISTS (SELECT userType from academic.Users where userName=@UserName)
	SET @Type='UNKNOWN';
ELSE
	SET @Type=(select userType from academic.Users where userName=@UserName)

END

GO

CREATE PROCEDURE academic.UpdateUserInfo(@UserName nvarchar(30),@Password nvarchar(200),@Picture nvarchar(100),@Email nvarchar(50),@Phone nvarchar(20),@PersonalInfo nvarchar (30))
As
BEGIN
IF EXISTS (select userName from academic.Users where userName=@UserName)
BEGIN
	IF @Password IS NOT NULL AND LEN(@Password) >0
		BEGIN
			UPDATE academic.Users SET pass=@Password WHERE userName=@UserName
		END
	IF @Email IS NOT NULL AND LEN(@Email) >0
		UPDATE academic.Users SET email=@Email where userName=@UserName
	IF @Picture IS NOT NULL AND LEN(@Picture) >0
		UPDATE academic.Users SET picture=@Picture where userName=@UserName
	END
	UPDATE academic.Users SET personalInfo=@PersonalInfo ,phone=@Phone
		where userName=@UserName
END


Go
CREATE PROCEDURE academic.AddNewStudent(@UserName nvarchar(30),@Password nvarchar(200),@Name nvarchar(30),@Surname nvarchar(30),@Picture nvarchar(100),@Email nvarchar(50),@Phone nvarchar(20),@PersonalInf nvarchar(300),@GradDate date)
AS
BEGIN

DECLARE @UID as INT


	BEGIN TRY
		BEGIN TRANSACTION
		INSERT INTO academic.Users VALUES(@Password,@UserName,'STUDENT',@Name,@Surname,@Picture,@Email,@Phone,@PersonalInf,1)
		SET @UID =SCOPE_IDENTITY()
		INSERT INTO academic.Students VALUES(@GradDate,@UID)
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;
	 SELECT 
        @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();

		raiserror(@ErrorMessage,@ErrorSeverity,@ErrorState)
		rollback transaction
	END CATCH
END

Go

CREATE PROCEDURE academic.AddNewProf(@UserName nvarchar(30),@Password nvarchar(200),
										@Name nvarchar(30),@Surname nvarchar(30),@Picture nvarchar(100)
										,@Email nvarchar(50),@Phone nvarchar(20),@PersonalInf nvarchar(300)
										,@WorkingHours int,@Title nvarchar(20))
AS
BEGIN
DECLARE @DpID as int
DECLARE @UID as INT

	BEGIN TRY
		BEGIN TRANSACTION
		INSERT INTO academic.Users VALUES(@Password,@UserName,'PROF',@Name,@Surname,@Picture,@Email,@Phone,@PersonalInf,1)
		SET @UID =SCOPE_IDENTITY()
		INSERT INTO academic.Professors VALUES(@WorkingHours,@Title,@UID)
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;
	 SELECT 
        @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();

		raiserror(@ErrorMessage,@ErrorSeverity,@ErrorState)
		rollback transaction
	END CATCH
END



Go

 CREATE PROCEDURE academic.AddDegree(@ProfUserName nvarchar (30),@Degree nvarchar(30),@Date date,@Major nvarchar(30))
 AS
 BEGIN
	DECLARE @ProfId int
 
	SET @ProfId=(
				select profId
				from academic.Professors as AP join academic.Users as AU on AP.userID=AU.userID
				 WHERE AU.userName=@ProfUserName
				)
	INSERT INTO academic.Prof_Degree VALUES(@ProfId,@Degree,@Date,@Major)
 END
 

 
 GO
 CREATE PROCEDURE academic.AddNewCourse( @CourseName nvarchar(30),@CourseCode nvarchar(15),@Term nvarchar(30),@CourseDescription nvarchar(200),@TidUserName nvarchar(30),@ProfUserName nvarchar(30))
 AS
 BEGIN
 BEGIN TRY
 BEGIN TRANSACTION
	DECLARE @DpID AS INT,@ProfID AS INT,@Tid AS INT ,@StID AS INT

	 SET @ProfID=(
					select profId
					from academic.Professors as AP join academic.Users as AU on AP.userID=AU.userID
					 WHERE AU.userName=@ProfUserName
		)
		IF @TidUserName IS NOT NULL
			BEGIN
				SET @StID=(Select stuID
				from academic.Students as ST join academic.Users AS AU on AU.userID=ST.userID WHERE AU.userName=@TidUserName
				)
				IF @StID IS NOT NULL
				BEGIN 
					INSERT INTO academic.TeachingAssistants VALUES(@StID)
					SET @Tid=SCOPE_IDENTITY()
				END
			END
		INSERT INTO academic.Courses VALUES(@CourseName,@Term,@CourseDescription,@CourseCode,@Tid,@ProfID,1);
		COMMIT TRANSACTION
		END TRY
		BEGIN CATCH
		
			DECLARE @ErrorMessage NVARCHAR(4000);
			DECLARE @ErrorSeverity INT;
			DECLARE @ErrorState INT;
			 SELECT 
				@ErrorMessage = ERROR_MESSAGE(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE();

				raiserror(@ErrorMessage,@ErrorSeverity,@ErrorState)
				rollback transaction
			
		END CATCH
 END



GO
CREATE PROCEDURE content.AddNewResources(@CourseCode nvarchar(15),@ProfUserName nvarchar(30),@Path nvarchar(200))AS
BEGIN
DECLARE @PRofID int;
DECLARE @CourseID int;
SELECT @PRofID=AP.profID,@CourseID=AC.courseID
from academic.Users as AU join academic.Professors AS AP ON AP.userID=AU.userID
JOIN academic.Courses as AC on AC.profID=Ap.profID
	Where AU.userName=@ProfUserName AND AC.code=@CourseCode;
INSERT INTO content.Resources VALUES (@CourseID,@PRofID,@Path,0)
END


Go
CREATE PROCEDURE content.AddNewLecture(@CourseCode nvarchar(15),@ProfUserName nvarchar(30),@YoutubeLink nvarchar(200),@title nvarchar (100),@Number int)AS
BEGIN
DECLARE @PRofID int;
DECLARE @CourseID int;
DECLARE @RID INT;
BEGIN TRY
BEGIN TRANSACTION
	SELECT @PRofID=AP.profID,@CourseID=AC.courseID
	from academic.Users as AU join academic.Professors AS AP ON AP.userID=AU.userID
	JOIN academic.Courses as AC on AC.profID=Ap.profID
		Where AU.userName=@ProfUserName AND AC.code=@CourseCode;
	INSERT INTO content.Resources VALUES (@CourseID,@PRofID,@YoutubeLink,0)
	SET @RID=SCOPE_IDENTITY()
	INSERT INTO content.Lectures VALUES(@title,@RID,@Number)
	commit transaction
	END TRY
	BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000);
			DECLARE @ErrorSeverity INT;
			DECLARE @ErrorState INT;
			 SELECT 
				@ErrorMessage = ERROR_MESSAGE(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE();
	rollback TRANSACTION
	END CATCH
END

GO

CREATE TRIGGER content.OnDeleteQuestionDeleteComment ON content.Questions instead of DELETE
AS
BEGIN
UPDATE content.Comments 
SET isDeleted=1
Where questionID in(select questionID from deleted)
END
UPDATE content.Questions
SET isDeleted=1
where questionID in (select questionID from deleted)
GO

CREATE TRIGGER academic.OnDeleteCourse ON academic.Courses INSTEAD OF DELETE 
AS
BEGIN
UPDATE academic.Courses SET isActive=0 WHERE courseID in (SELECT courseID from deleted)
END
GO
CREATE TRIGGER academic.OnDeleteAnnouncements ON academic.Announcements INSTEAD  OF DELETE 
AS 
BEGIN
UPDATE academic.Announcements  SET isDeleted=1 WHERE EXISTS(SELECT * FROM deleted join academic.Announcements as AA ON AA.profID=deleted.profID AND AA.courseID=deleted.courseID AND AA.[date]=deleted.[date])
END
GO

CREATE TRIGGER content.OnDeleteComment ON content.Comments instead of DELETE 
AS
BEGIN
UPDATE content.Comments 
SET isDeleted=1
Where commentID in(select commentID from deleted)
END
GO

GO
CREATE PROCEDURE content.AddNewQuestion(@Msg nvarchar(400),@UserName nvarchar(30),@IsResolved bit,@ResourceID int)
AS
BEGIN
DECLARE @USerID int;
SET @USerID=(select userID from academic.Users Where userName=@UserName);
INSERT INTO content.Questions VALUES(@Msg,@USerID,0,@ResourceID,0)
END
GO

CREATE PROCEDURE content.SetQuestionResovled(@QuestionID INT,@IsResolved bit)
AS
UPDATE content.Questions SET isResolved=@IsResolved  WHERE content.Questions.questionID=@QuestionID

GO
CREATE PROCEDURE content.AddNewComment(@Msg nvarchar(4000),@UserName nvarchar(30),@QuestionID int)
AS
BEGIN
DECLARE @USerID int;
SET @USerID=(select userID from academic.Users Where userName=@UserName);
INSERT INTO content.Comments VALUES(@Msg,@USerID,@QuestionID,0)
END

GO
CREATE PROCEDURE academic.AddUserToAClass(@UserName nvarchar(30),@CourseCode nvarchar(30))
AS
BEGIN
DECLARE @StudentID int,@CourseID int;
SET @StudentID=(select AST.stuID from academic.Users as AU join academic.Students as AST on AST.userID=AU.userID
 where userName=@UserName)
SET @CourseID =(SELECT courseID from academic.Courses where code=@CourseCode)
INSERT INTO academic.Take VALUES(@CourseID,@StudentID)
END

GO
  
CREATE PROCEDURE academic.AddNewDepartment(@DepartmeantName nvarchar(20),@DepartmentDiscription nvarchar(300))
AS INSERT INTO academic.Departments VALUES (@DepartmeantName,@DepartmentDiscription)
GO

CREATE PROCEDURE academic.AddStudentToCourse(@UserName nvarchar(30),@CourseCode nvarchar(30))
AS
BEGIN
DECLARE @SUID int,@CouserID int
SET @SUID=(select stuID from academic.Students AS AC join  academic.Users AS AU on  AU.userID=AC.userID where AU.userName=@UserName)
SET @CouserID=(select courseID from academic.Courses where Courses.code=@CourseCode)
INSERT INTO academic.[Take] VALUES(@CouserID,@SUID);
END

GO
CREATE FUNCTION academic.StudentsByCourse (@CourseCode nvarchar (30) )
RETURNS @StudentByClass TABLE(
userID  int ,
stuID  int
)
AS
BEGIN 


INSERT INTO @StudentByClass(userID,stuID)
 SELECT AST.userID,AST.stuID
from academic.Courses AS AC join academic.[Take] as AT on AC.courseID=AT.courseID
join academic.Students AS AST on AST.stuID=AT.stuID 
where AC.code=@CourseCode 
RETURN
END

GO
CREATE PROCEDURE academic.MakeNewAnnouncement(@ProfUName nvarchar(30),@CourseCode nvarchar(30),@Date datetime ,@Mesg nvarchar (4000))
AS
BEGIN
DECLARE @ProfId int,@CourseId int;
SET @ProfId=(SELECT profID from academic.Professors join academic.Users on academic.Users.userID= academic.Professors.userID where academic.Users.userName=@ProfUName);
SET @CourseId=(select courseID from academic.Courses where Courses.code=@CourseCode)
INSERT INTO [academic].[Announcements] VALUES(@ProfId,@CourseId,@Date,@Mesg,0)
END

GO
CREATE VIEW academic.StudentsInfo
AS 
SELECT AU.userName,AU.name,AU.surname,Au.email,AD.departmentName,AST.garduationDate
FROM academic.Users AS AU join academic.Students as AST on AST.userID=AU.userID
join academic.Belong_To AS AB on Ab.userID=AU.userID 
join academic.Departments AS AD on Ab.departmentID=AD.departmentID

GO
CREATE VIEW academic.CoursesInfo 
AS
SELECT Ac.code,Ac.courseName,(select AU.name from academic.Professors  AS AP join academic.Users AS AU on AU.userID=AP.userID  where profID=AC.profID) AS [Prof's Name],
(select AU.surname from academic.Professors  AS AP join academic.Users AS AU on AU.userID=AP.userID  where profID=AC.profID) AS [Prof's Surname],
(SELECT AU.name FROM academic.TeachingAssistants AS AT join academic.Students AS ACC ON ACC.stuID=AT.stuID
JOIN academic.Users AS AU ON AU.userID=ACC.userID WHERE AT.tID=AT.tID) AS [TA's Name],
(SELECT AU.surname FROM academic.TeachingAssistants AS AT join academic.Students AS ACC ON ACC.stuID=AT.stuID
JOIN academic.Users AS AU ON AU.userID=ACC.userID WHERE AT.tID=AT.tID) AS [TA's Surname],
(select count(*) from academic.[Take]  AS AT join academic.Students AS AST ON AST.stuID=AT.stuID
join academic.Users AS AUS ON AUS.userID =AST.userID 
WHERE AUS.isActive=1 AND AT.courseID=AC.courseID) AS [Number of current Students],
AC.courseDescription,Ac.term,Ac.isActive
FROM academic.Courses  AS AC
GO
CREATE VIEW academic.DepartmentsInfo AS
SELECT AD.departmentName,
(SELECT COUNT(*) FROM academic.Belong_To AS BT JOIN academic.Users AU ON AU.userID=BT.userID JOIN academic.Professors AS AP ON AP.userID=AU.userID WHERE BT.departmentID=AD.departmentID) AS [Number of Professors in the dep],
(SELECT COUNT (*) FROM academic.Belong_To AS BT JOIN academic.Users AU ON AU.userID=BT.userID JOIN
academic.Students AS AST ON AST.userID=AU.userID where BT.departmentID=AD.departmentID) AS[Number of Students in dep],
(SELECT COUNT (*) FROM academic.Courses AS AC JOIN academic.Have AH ON AH.courseID=AC.courseID WHERE AH.departmentID=AD.departmentID) AS [Number Of Courses in dep]
FROM academic.Departments AS AD

GO

GO
CREATE VIEW academic.ProfInfo --degree gives errors when has multiple values
AS 
SELECT PF.title,AU.name,AU.surname,AU.userName,AU.email,PF.workingHours,AD.departmentName,
(SELECT degree FROM academic.Prof_Degree WHERE profID=PF.profID) AS Degree
,(SELECT COUNT (*) from academic.Courses WHERE academic.Courses.profID=PF.profID) AS [Number Of courses]
FROM academic.Professors AS PF join academic.Users AS AU ON AU.userID=PF.userID 
join academic.Belong_To AS AB on AB.userID=AU.userID 
join academic.Departments AS AD ON AD.departmentID=AB.departmentID;
GO
CREATE VIEW academic.ProfHasNoDepartment
AS 
 SELECT PF.title,AU.name,AU.surname,AU.userName,AU.email,PF.workingHours,
(SELECT degree FROM academic.Prof_Degree WHERE profID=PF.profID) AS Degree
,(SELECT COUNT (*) from academic.Courses WHERE academic.Courses.profID=PF.profID) AS [Number Of courses]
FROM academic.Professors AS PF join academic.Users AS AU ON AU.userID=PF.userID WHERE AU.userID NOT IN(select userID from academic.Belong_To )
GO
select * from academic.ProfHasNoDepartment

SELECT * from academic.ProfInfo
GO
CREATE PROCEDURE academic.AddUserToDepartment(@UserName nvarchar(30),@DepartementName nvarchar(20))
AS
BEGIN
DECLARE @UserId int,@DepId int
SELECT @UserId=AU.userID FROM academic.Users AS AU WHERE AU.userName=@UserName
SELECT @DepId=AD.departmentID FROM academic.Departments AS AD where AD.departmentName=@DepartementName 

INSERT INTO academic.Belong_To VALUES(@UserId,@DepId)
END
GO


CREATE PROCEDURE academic.AddCourseToDepartment(@CourseCode nvarchar(15),@DepartmentName nvarchar(20))
AS
BEGIN 
DECLARE @CourseId int,@DepId int
SELECT @CourseId=courseID FROM academic.Courses WHERE code=@CourseCode
SELECT @DepId=AD.departmentID FROM academic.Departments AS AD where AD.departmentName=@DepartmentName
INSERT INTO academic.Have VALUES(@CourseId,@DepId)
END
GO

CREATE PROCEDURE academic.CoursesByDepartment(@DepartmentName nvarchar(20))
AS
BEGIN
SELECT AC.code,AC.courseDescription,Ac.courseID,AC.courseName,AC.isActive,AC.profID,Ac.term,AC.tID FROM academic.Courses AS AC join academic.Have AH ON AH.courseID=AC.courseID JOIN academic.Departments AS AD  ON AD.departmentID=AH.departmentID WHERE @DepartmentName=AD.departmentName 
END
GO
CREATE PROCEDURE academic.StudentByUserName(@UserName nvarchar(30))AS
SELECT * FROM academic.Users as AU JOIN academic.Students AS AST ON AST.userID=AU.userID WHERE AU.userName=@Username
GO
GO
CREATE PROCEDURE academic.DepartmentByName(@DepartmentName nvarchar(30))AS
SELECT * FROM academic.Departments where departmentName=@DepartmentName;
GO
CREATE PROCEDURE academic.GetDepartments
AS
SELECT * from academic.Departments;
GO
CREATE PROCEDURE academic.UpdateStudentInfo(@Stdid int ,@gradDate date)AS
UPDATE academic.Students set garduationDate=@gradDate WHERE stuID=@Stdid;
Go
CREATE PROCEDURE academic.UserByUserName(@UserName varchar(30))AS
SELECT * from academic.Students AS AC join academic.Users AS AU ON AU.userID=AC.userID WHERE AU.userName=@UserName;
GO
CREATE PROCEDURE academic.StudentById(@StdId varchar(30))AS
SELECT * from academic.Students AS AC join academic.Users AS AU ON AU.userID=AC.userID WHERE Ac.stuID=@StdId;
GO
CREATE PROCEDURE academic.ProfessorByUserName(@ProfUserName nvarchar(30))AS
SELECT * from academic.Professors AS AP JOIN academic.Users AS AU ON AU.userID=AP.userID WHERE  AU.userName=@ProfUserName
GO
CREATE PROCEDURE academic.ProfessorByProfID(@ProfID int)AS
SELECT * from academic.Professors AS AP JOIN academic.Users AS AU ON AU.userID=AP.userID WHERE  AP.profID=@ProfID

exec academic.StudentByUserName 'py'
SELECT * from [academic].[CoursesInfo]
exec academic.AddNewDepartment 'CS','computer eng'
exec academic.AddNewDepartment'EE','Electoringasd asd'
SELECT * from [academic].[DepartmentsInfo]
SELECT * from [academic].ProfHasNoDepartment
select * from academic.StudentsInfo

go

create procedure academic.getstudentcourses(@id as nvarchar(50))
as
select *
from  [academic].[Students]as s inner join [academic].[Take] as t on s.stuID = t.stuID
join [academic].[Courses] as c on c.courseID= t.courseID
where s.stuID=@id 

go
create procedure academic.countstudentcourses(@code as nvarchar(50))
as
select COUNT([stuID]) AS stunum 
from  [academic].[Take] as t inner join [academic].[Courses] as c on t.courseID=c.courseID
where c.[code]=@code
GROUP BY (c.[code])
go
create procedure academic.getdepcourses(@id as nvarchar(50))
as
select *
from [academic].[Courses] as c inner join [academic].[Have] as h on c.courseID = h.courseID
join  [academic].[Departments] as d on d.departmentID= h.departmentID
where d.departmentID=@id
go

create procedure academic.getprofcourses(@id as nvarchar(50))
as
select *
from  [academic].[Professors]as p inner join [academic].[Courses] as c on p.profID = c.profID 
where p.profID=@id
go
create procedure academic.getCourses
as
select *
from [academic].[Courses] 
go
create procedure academic.countcourses
as
select COUNT([courseID]) AS cnum 
from   [academic].[Courses] 
where [isActive]=1
GROUP BY ([isActive])
go
create procedure academic.countresouce
as
select COUNT([resourceID]) AS rnum 
from   [content].[Resources]
where [isDeleted]=0
GROUP BY ([isDeleted])

go
create procedure academic.countlecture(@coursecode as nvarchar(50))
as
select COUNT([lectureID]) AS lecnum 
from   [content].[Resources] as r inner join [content].[Lectures]as l on r.resourceID=l.resourceID
join [academic].[Courses] as c on c.courseID = r.courseID
where c.code=@coursecode
GROUP BY (c.courseID)
go
  create procedure academic.Updatcourseinfo2(@id as nvarchar(50), @name as nvarchar(4000),@term as nvarchar(50), @desc as nvarchar(4000),@code as nvarchar(50),@tname as nvarchar(50),@isactiv as bit)
 as
  BEGIN
 DECLARE @STDID int
  DECLARE @Tid int
 SELECT @STDID=AST.stuID FROM academic.Students as AST join academic.Users AS AU ON AU.userID=AST.userID WHERE AU.userName=@tname
 INSERT INTO academic.TeachingAssistants VALUES(@STDID)
 SET @Tid=SCOPE_IDENTITY()


 update [academic].[Courses]  
 set [courseName]=@name,[term]=@term,[courseDescription]=@desc,[code]=@code,[tID]=@Tid,[isActive]=@isactiv
 where [courseID]=@id
 END
 go


 CREATE PROCEDURE academic.ResourcesByCourse(@CourseCode nvarchar(15))AS
 SELECT * FROM content.Resources AS CR join academic.Courses AS AC ON AC.courseID=CR.courseID where AC.code=@CourseCode

 GO
 CREATE PROCEDURE content.ResourceByID(@ResourceID int)AS
 SELECT *from content.Lectures WHERE resourceID=@ResourceID
 GO

 CREATE PROCEDURE content.LectureById(@LectureID int)AS
 SELECT * from content.Lectures where content.Lectures.lectureID=@LectureID;

 
 CREATE PROCEDURE content.LecturerByCourse(@CourseCode nvarchar(15))AS
 SELECT * from content.Lectures AS CL join  content.Resources as CR  ON CL.resourceID=Cr.resourceID
 join academic.Courses AS AC ON CR.courseID=AC.courseID WHERE AC.code=@CourseCode;

 CREATE PROCEDURE academic.DepartmentsOfUser(@UserName nvarchar(50))AS
 SELECT * from academic.Users AS AU join academic.Belong_To  AS AB ON AB.userID=AU.userID
 JOIN academic.Departments AS AD ON AD.departmentID=AB.departmentID WHERE AU.userName=@UserName 

 CREATE PROCEDURE academic.UpdateProfessorInfo(@ProfID int ,@WorkingHours int,@Title nvarchar(20))AS
 UPDATE academic.Professors SET workingHours=@WorkingHours, title=@Title WHERE profID=@ProfID;

 GO
 CREATE PROCEDURE content.QuestionByID(@questionID int)AS
 SELECT * from  content.Questions where questionID=@questionID;
 GO
 CREATE PROCEDURE content.QuestionByResource(@ResourceID nvarchar(20))AS
 SELECT * FROM content.Questions  WHERE  ressourceID=@ResourceID
 GO

 CREATE PROCEDURE content.CommentByID(@CommentID int)AS
 Select * from content.Comments where commentID=@CommentID;

 CREATE PROCEDURE content.CommentsByQuestion(@QuestionId int)AS
 SELECT * FROM content.Comments WHERE questionID=@QuestionId;
>>>>>>> e7f5267ea91165c02d54bbea85b6f17edcb727ba:Stuff/virtualClass.sql

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using virtualclass.modles;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session[Constants.UserName] == null || Session[Constants.UserName].GetType() != typeof(Professor))
        {
            Response.Redirect("/login.aspx");
        }
    }

    protected void btnUpdateInformation_Click1(object sender, EventArgs e)
    {
          Professor proffesor = Session[Constants.UserName] as Professor;
        if (!String.IsNullOrWhiteSpace(txtName.Text))
            proffesor.name = txtName.Text;
        if (!String.IsNullOrWhiteSpace(txtSurname.Text))
            proffesor.surname = txtSurname.Text;
        if (!String.IsNullOrWhiteSpace(txtNewPassword.Text))
            proffesor.password = txtNewPassword.Text;
        if (!String.IsNullOrWhiteSpace(txtPhone.Text))
            proffesor.phone = txtPhone.Text;
        if (!String.IsNullOrWhiteSpace(txtEmail.Text))
            proffesor.email = txtEmail.Text;
        if (!String.IsNullOrWhiteSpace(txtpersonalinfo.Text))
            proffesor.personalInfo = txtpersonalinfo.Text;
        if (!String.IsNullOrWhiteSpace(working.Text))
            proffesor.workingHours = int.Parse(working.Text);
        if (!String.IsNullOrWhiteSpace(txtTitle.Text))
            proffesor.title = txtTitle.Text;


        proffesor.updateInfo();

        mssge.Text = "The record is successfully updated";
    }
    protected void btnUploadPicture_Click(object sender, EventArgs e)
    {
        Professor proffesor = Session[Constants.UserName] as Professor;
        

        if(FileUpload2.HasFiles)
        {
            string str = FileUpload2.FileName;
            FileUpload2.PostedFile.SaveAs(Server.MapPath(".") + "//images//" + str);
            string path = "~//images//" + str.ToString();
            proffesor.picture = path;
            mssge0.Text = "image is updated sucessfully";
            proffesor.updateInfo();
        }
        else
        {
            mssge0.Text = "plz upload ur photo";
        }
    }
}
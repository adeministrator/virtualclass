﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using virtualclass.modles;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session[Constants.UserName] == null || Session[Constants.UserName].GetType() != typeof(Student))
        {
            Response.Redirect("/login.aspx");
        }
    }
    protected void btnUpdateInformation_Click(object sender, EventArgs e)
    {
        Student student = Session[Constants.UserName] as Student;
        if(! String.IsNullOrWhiteSpace(txtName.Text))
          student.name = txtName.Text;
        if (!String.IsNullOrWhiteSpace(txtSurname.Text))
        student.surname = txtSurname.Text;
        if (!String.IsNullOrWhiteSpace(txtPassword.Text))
        student.password = txtPassword.Text;
        if (!String.IsNullOrWhiteSpace(txtPhoneNumber.Text))
        student.phone = txtPhoneNumber.Text;
        if (!String.IsNullOrWhiteSpace(txtEmail.Text))
        student.email = txtEmail.Text;
        if (!String.IsNullOrWhiteSpace(txtpersonalinfo.Text))
        student.personalInfo = txtpersonalinfo.Text;
 

        student.updateInfo();

        lblmssg.Text = "The record is successfully updated";
        
    }

    protected void clGraduationDate_SelectionChanged(object sender, EventArgs e)
    {
        Student student = Session[Constants.UserName] as Student;
        student.graduatingDate = clGraduationDate.SelectedDate;
        
    }
    protected void btnUploadPicture_Click(object sender, EventArgs e)
    {
        Student stu = Session[Constants.UserName] as Student;


        if (FileUpload2.HasFiles)
        {
            string str = FileUpload2.FileName;
            FileUpload2.PostedFile.SaveAs(Server.MapPath(".") + "//images//" + str);
            string path = "~//images//" + str.ToString();
            stu.picture = path;
            mssge0.Text = "image is updated sucessfully";
            stu.updateInfo();
        }
        else
        {
            mssge0.Text = "plz upload ur photo";
        }

    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MiddleMP.master" AutoEventWireup="true" CodeFile="PersonalInfoStu.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .auto-style3 {
            width: 295px;
        }
        .auto-style4 {
            width: 295px;
            height: 20px;
        }
        .auto-style5 {
            height: 20px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="menu2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="middleSide" Runat="Server">
    <table style="width:100%;">
        <tr>
            <td class="auto-style3">
                <asp:Image ID="Image1" runat="server" ImageUrl='<%#"images//"+Eval("picture") %>' Height="130px" Width="154px" />
            </td>
        </tr>
        <tr>
            <td class="auto-style3">
                <asp:Label ID="Label1" runat="server" Text="Name"></asp:Label>
            </td>
            <td class="auto-style5">
                <asp:Label ID="name" runat="server" ></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style4">Surname</td>
            <td class="auto-style5">
                <asp:Label ID="suname" runat="server" ></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style3">
                <asp:Label ID="Label3" runat="server" Text="User Name"></asp:Label>
            </td>
            <td class="auto-style8">
                <asp:Label ID="username" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style4">
                <asp:Label ID="Label6" runat="server" Text="Email Address"></asp:Label>
            </td>
            <td class="auto-style5">
                <asp:Label ID="email" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style3">
                <asp:Label ID="Label7" runat="server" Text="Phone Number"></asp:Label>
            </td>
            <td class="auto-style5">
                <asp:Label ID="phone" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style4">
                <asp:Label ID="Label8" runat="server" Text="Graduation Date"></asp:Label>
            </td>
            <td class="auto-style5">
                <asp:Label ID="date" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style3">
                <asp:Label ID="Label9" runat="server" Text="Department"></asp:Label>
            </td>
            <td class="auto-style5">
                <asp:Label ID="departmment" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style3">
                <asp:Label ID="Label10" runat="server" Text="personal information"></asp:Label>
            </td>
            <td class="auto-style5">
                <asp:Label ID="pi" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style3">&nbsp;</td>
            <td class="auto-style5">
                <asp:Button ID="btnUpdateInformation" runat="server" Text="Update Information" OnClick="btnUpdateInformation_Click" />
            </td>
        </tr>
    </table>
</asp:Content>


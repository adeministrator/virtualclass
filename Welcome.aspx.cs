﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnRedirectSignupStu_Click(object sender, EventArgs e)
    {
        Server.Transfer("signupStu.aspx", true);
    }

    
    protected void btnRedirectSignupProf_Click(object sender, EventArgs e)
    {
        Server.Transfer("signupProf.aspx", true);
    }


    protected void btnLogin_Click(object sender, EventArgs e)
    {
        Server.Transfer("login.aspx", true);
    }
}
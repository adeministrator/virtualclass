﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using virtualclass.modles;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session[Constants.UserName] == null || Session[Constants.UserName].GetType() != typeof(Professor))
        {
            Response.Redirect("/login.aspx");
        }
        Professor prof = Session[Constants.UserName] as Professor;
        List<Course> cours = Course.GetProfCourses(prof.profId);
        foreach (Course s in cours)
        {
            dlstCourseList.Items.Add(s.courseName);
        }
    }
    protected void btnAddLecture_Click(object sender, EventArgs e)
    {
        Professor prof = Session[Constants.UserName] as Professor;
        List<Course> co = Course.GetProfCourses(prof.profId);
        int i = dlstCourseList.SelectedIndex;
        string path = txpath.Text;
        int num  = int.Parse(txtNumber.Text);
        string code= co[i].code;
        string profname= prof.userName;
        string title= txtTitle.Text;
        co[i].AddLecture(code, profname, path, title, num);
    }
}
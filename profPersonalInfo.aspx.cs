﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using virtualclass.modles;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session[Constants.UserName] == null || Session[Constants.UserName].GetType() != typeof(Professor))
        {
            Response.Redirect("/login.aspx");
        }
        if (!IsPostBack)
        {
            bindata();
        }


    }
    public void bindata()
    {
        Professor professor = Session[Constants.UserName] as Professor;
        List<Department> dep =professor.getDepartments();
        lblName.Text = professor.name;
        lblSurname.Text = professor.surname;
        lblUserName.Text = professor.userName;
        lblPhone.Text = professor.phone;
        lblEmail.Text = professor.email;
        lblPersonalInfo.Text = professor.personalInfo;
        lblTitle.Text = professor.title;
        for (int i = 0; i < dep.Count; i++)
        {
            lblDepartment.Text+= dep[i].departmentName;
            if (dep.Count - 1 > i)
                lblDepartment.Text += ",";
        }
        work.Text = professor.workingHours.ToString();
        Image1.ImageUrl = professor.picture;

    }
    protected void btnGoEditPersonalInfoProf_Click(object sender, EventArgs e)
    {
        Response.Redirect("/EditPersonalInfoProf.aspx");
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MiddleMP.master" AutoEventWireup="true" CodeFile="AddLecture.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .auto-style3 {
            width: 405px;
        }
        .auto-style4 {
            width: 393px;
        }
        .auto-style5 {
            width: 191px;
        }
        .auto-style6 {
            width: 393px;
            height: 20px;
        }
        .auto-style7 {
            width: 191px;
            height: 20px;
        }
        .auto-style8 {
            width: 405px;
            height: 20px;
        }
        .auto-style9 {
            height: 20px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="menu2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="middleSide" Runat="Server">
    <table style="width: 100%;">
        <tr>
            <td class="auto-style4">&nbsp;</td>
            <td class="auto-style5">
                <asp:Label ID="Label1" runat="server" Text="Title"></asp:Label>
            </td>
            <td class="auto-style3">
                <asp:TextBox ID="txtTitle" runat="server"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style4">&nbsp;</td>
            <td class="auto-style5">
                <asp:Label ID="Label2" runat="server" Text="Number"></asp:Label>
            </td>
            <td class="auto-style3">
                <asp:TextBox ID="txtNumber" runat="server"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style6"></td>
            <td class="auto-style7">
                <asp:Label ID="Label4" runat="server" Text="path"></asp:Label>
            </td>
            <td class="auto-style8">
                <asp:TextBox ID="txpath" runat="server"></asp:TextBox>
            </td>
            <td class="auto-style9"></td>
            <td class="auto-style9"></td>
        </tr>
        <tr>
            <td class="auto-style4">&nbsp;</td>
            <td class="auto-style5">
                <asp:Label ID="Label3" runat="server" Text="Course Name"></asp:Label>
            </td>
            <td class="auto-style3">
                <asp:DropDownList ID="dlstCourseList" runat="server">
                </asp:DropDownList>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style4">&nbsp;</td>
            <td class="auto-style5">&nbsp;</td>
            <td class="auto-style3">
                <asp:Button ID="btnAddLecture" runat="server" OnClick="btnAddLecture_Click" Text="Add Lecture" />
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style4">&nbsp;</td>
            <td class="auto-style5">&nbsp;</td>
            <td class="auto-style3">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
</asp:Content>


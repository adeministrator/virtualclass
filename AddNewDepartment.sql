USE [VirtualClass]
GO

DECLARE	@return_value int

EXEC	@return_value = [academic].[AddNewDepartment]
		@DepartmeantName = N'Civil Engineering',
		@DepartmentDiscription = N'All lectures about civil engineering and structures will be given'

SELECT	'Return Value' = @return_value

GO
